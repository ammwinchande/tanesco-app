# TANESCO

TANESCO Application is for enabling our customers to reach us directly and with ease while at the same time reducing corruption (VISHOKA) which our customers are usually facing

## Foundation

This project is a starting point for having full fledged TANESCO Application which will carter
for all services that TANESCO as organization has to offer.

## Contact Resources

- Call us: +255768985100
- E-Mail us: customer.service@tanesco.co.tz
- Facebook: https://web.facebook.com/tanesco.yetultd
- Twitter: https//twitter.com/tanescoyetu
- Instagram: https://instagram.com/tanesco_official_page
- Official Website: https://www.tanesco.co.tz/
- YouTube: https://www.youtube.com/user/umemeforum
