import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/dashboard.dart';
import 'package:tanesco/db/dbhelper.dart';
import 'package:tanesco/home.dart';
import 'package:tanesco/landing.dart';
import 'package:tanesco/language.dart';
import 'package:tanesco/login.dart';
import 'package:tanesco/register.dart';
import 'package:tanesco/service_locator.dart';

Future<void> main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();
    await setupLocator();
    runApp(Main());
  } catch (error) {
    Fluttertoast.showToast(msg: 'Locator setup has failed ' + error.toString());
  }
}

class Main extends StatelessWidget {
  // This widget is the root of application.
  @override
  Widget build(BuildContext context) {
    try {
      DBHelper().db;
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }

    return BotToastInit(
      child: MaterialApp(
        title: Config.appNameLabel,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: CustomColors.green,
          accentColor: CustomColors.green,
        ),
        initialRoute: '/',
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case '/':
              return PageTransition(
                child: LandingPage(),
                type: PageTransitionType.rightToLeft,
              );
              break;
            case '/home':
              return PageTransition(
                child: HomePage(),
                type: PageTransitionType.rightToLeft,
                settings: settings,
              );
              break;
            case '/language':
              return PageTransition(
                child: LanguagePage(),
                type: PageTransitionType.rightToLeft,
              );
              break;
            case '/register':
              return PageTransition(
                child: RegistrationPage(),
                type: PageTransitionType.rightToLeft,
                settings: settings,
              );
              break;
            case '/login':
              return PageTransition(
                child: LoginPage(),
                type: PageTransitionType.rightToLeft,
                settings: settings,
              );
              break;
            case '/dashboard':
              return PageTransition(
                child: Dashboard(),
                type: PageTransitionType.rightToLeft,
                settings: settings,
              );
              break;
            default:
              return null;
          }
        },
        navigatorObservers: [BotToastNavigatorObserver()],
      ),
    );
  }
}
