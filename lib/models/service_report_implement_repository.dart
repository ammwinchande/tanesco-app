import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/models/service_rating.dart';
import 'package:tanesco/models/service_report.dart';
import 'package:tanesco/models/service_report_repository.dart';
import 'package:tanesco/models/service_status.dart';

class ServiceReportImplementRepository implements ServiceReportRepository {
  static String urlPost = Config.baseUrl;

  static BaseOptions options = BaseOptions(
    baseUrl: urlPost,
    responseType: ResponseType.plain,
    connectTimeout: 30000,
    receiveTimeout: 30000,
    validateStatus: (code) {
      if (code >= 200) {
        return true;
      }
      return null;
    },
  );
  static Dio dio = Dio(options);

  ServiceReportImplementRepository();

  @override
  Future<String> postData(ServiceReport serviceReport) async {
    try {
      Options options = Options(
        contentType: 'application/json',
      );
      Response response = await dio.post(
        'api/service/report',
        data: this.toJson(serviceReport),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);
        return responseJson;
      } else if (response.statusCode == 401) {
        throw Exception(
          'Invalid data submitted - ${response.statusCode}',
        );
      } else {
        throw Exception(
          'Unknown Device Report Error - ${response.statusCode}',
        );
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
          "Couldn't connect, please ensure you have a stable network.",
        );
      } else {
        return null;
      }
    }
  }

  @override
  Future<String> requestStatus(ServiceStatus serviceStatus) async {
    try {
      Options options = Options(
        contentType: 'application/json',
      );
      Response response = await dio.post(
        "api/service/get-status",
        data: this.statusToJson(serviceStatus),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);
        return responseJson;
      } else if (response.statusCode == 401) {
        throw Exception(
          'Invalid data submitted - ${response.statusCode}',
        );
      } else {
        throw Exception(
          'Unknown Device Report Error - ${response.statusCode}',
        );
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
          "Couldn't connect, please ensure you have a stable network.",
        );
      } else {
        return null;
      }
    }
  }

  @override
  Future<String> addFeedback(ServiceRating serviceRating) async {
    try {
      Options options = Options(
        contentType: 'application/json',
      );
      Response response = await dio.post(
        'api/service/add-feedback',
        data: this.ratingToJson(serviceRating),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);
        return responseJson;
      } else if (response.statusCode == 401) {
        throw Exception(
          'Invalid data submitted - ${response.statusCode}',
        );
      } else {
        throw Exception(
          'Unknown Device Report Error - ${response.statusCode}',
        );
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception('Network Error');
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
          "Couldn't connect, please ensure you have a stable network.",
        );
      } else {
        return null;
      }
    }
  }

  Map<String, dynamic> toJson(ServiceReport serviceReport) {
    return serviceReport.toJson();
  }

  Map<String, dynamic> statusToJson(ServiceStatus serviceStatus) {
    return serviceStatus.toJson();
  }

  Map<String, dynamic> ratingToJson(ServiceRating serviceRating) {
    return serviceRating.toJson();
  }
}
