import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/dao.dart';

class ClientDao implements Dao<Client> {
  final tableName = 'client';
  final columnId = 'id';

  final _columnFirstName = 'first_name';
  final _columnLastName = 'last_name';
  final _columnPhoneNo = 'mobile_no';
  final _columnMeterNo = 'meter_no';
  final _columnLangUsed = 'lang_used';
  final _columnDeviceId = 'device_id';
  final _columnRegionId = 'region_id';
  final _columnDistrictId = 'district_id';
  final _columnAreaId = 'area_id';
  final _columnStreetId = 'street_id';
  final _columnSpecialMark = 'special_mark';

  final _columnCreatedDate = 'created_date';
  final _columnSessionStart = 'session_start_date';
  final _columnSessionEnd = 'session_end_date';

  final getUrl = Config.baseUrl + Client().urlGet;
  final getPost = Config.baseUrl + Client().urlPost;

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY AUTOINCREMENT,"
      " $_columnFirstName TEXT,"
      " $_columnLastName TEXT,"
      " $_columnPhoneNo TEXT,"
      " $_columnMeterNo TEXT,"
      " $_columnLangUsed TEXT,"
      " $_columnDeviceId TEXT,"
      " $_columnRegionId INTEGER,"
      " $_columnDistrictId INTEGER,"
      " $_columnAreaId INTEGER,"
      " $_columnStreetId INTEGER,"
      " $_columnSpecialMark TEXT,"
      " $_columnCreatedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,"
      " $_columnSessionStart TIMESTAMP DEFAULT CURRENT_TIMESTAMP,"
      " $_columnSessionEnd TIMESTAMP  DEFAULT CURRENT_TIMESTAMP"
      ")";

  @override
  Client fromMap(Map<String, dynamic> map) {
    Client client = Client();
    client.id = map[columnId];
    client.firstName = map[_columnFirstName];
    client.lastName = map[_columnLastName];
    client.phone = map[_columnPhoneNo];
    client.meterno = map[_columnMeterNo];
    client.langUsed = map[_columnLangUsed];
    client.deviceId = map[_columnDeviceId];
    client.regionId = map[_columnRegionId];
    client.districtId = map[_columnDistrictId];
    client.areaId = map[_columnAreaId];
    client.streetId = map[_columnStreetId];
    client.specialMark = map[_columnSpecialMark];
    client.createdDate = map[_columnCreatedDate];
    client.sessionStartDate = map[_columnSessionStart];
    client.sessionEndDate = map[_columnSessionEnd];
    return client;
  }

  @override
  Map<String, dynamic> toMap(Client object) {
    return <String, dynamic>{
      _columnFirstName: object.firstName,
      _columnLastName: object.lastName,
      _columnPhoneNo: object.phone,
      _columnMeterNo: object.meterno,
      _columnDeviceId: object.deviceId,
      _columnLangUsed: object.langUsed,
      _columnRegionId: object.regionId,
      _columnDistrictId: object.districtId,
      _columnAreaId: object.areaId,
      _columnStreetId: object.streetId,
      _columnSpecialMark: object.specialMark,
      _columnCreatedDate: object.createdDate,
      _columnSessionStart: object.sessionStartDate,
      _columnSessionEnd: object.sessionEndDate
    };
  }

  Map<String, dynamic> toJson(Client client) {
    return client.toJson();
  }

  @override
  Client fromDb(Map<String, dynamic> map) {
    Client client = Client();
    client.id = map['id'];
    client.firstName = map['first_name'];
    client.lastName = map['last_name'];
    client.phone = map['mobile_number'];
    client.meterno = map['meter_number'];
    client.langUsed = map['lang_used'];
    client.deviceId = map['device_id'];
    client.regionId = map['region_id'];
    client.districtId = map['district_id'];
    client.areaId = map['area_id'];
    client.streetId = map['street_id'];
    client.specialMark = map['special_mark'];
    client.createdDate = map['created_date'];
    client.sessionStartDate = map['session_start_date'];
    client.sessionEndDate = map['session_end_date'];
    return client;
  }

  @override
  List<Client> fromList(List<Map<String, dynamic>> query) {
    List<Client> clients = List<Client>();
    for (Map map in query) {
      clients.add(fromMap(map));
    }
    return clients;
  }
}
