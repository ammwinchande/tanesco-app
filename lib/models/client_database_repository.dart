import 'dart:collection';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:sqflite/sqlite_api.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/db/dbhelper.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/client_dao.dart';
import 'package:tanesco/models/client_language.dart';
import 'package:tanesco/models/client_location.dart';
import 'package:tanesco/models/client_meter.dart';
import 'package:tanesco/models/client_notifications.dart';
import 'package:tanesco/models/client_repository.dart';
import 'package:tanesco/models/client_subscription.dart';
import 'package:tanesco/models/client_unregister.dart';
import 'package:tanesco/models/login_model.dart';

class ClientDatabaseRepository implements ClientRepository {
  final dao = ClientDao();

  static String urlPost = Config.baseUrl;

  static BaseOptions options = BaseOptions(
    baseUrl: urlPost,
    responseType: ResponseType.plain,
    connectTimeout: 30000,
    receiveTimeout: 30000,
    validateStatus: (code) {
      if (code >= 200) {
        return true;
      }
      return null;
    },
  );
  static Dio dio = Dio(options);

  ClientDatabaseRepository();

  @override
  Future<Client> insert(Client client) async {
    var db = await DBHelper().db;
    client.id = await db.insert(
      dao.tableName,
      dao.toMap(client),
    );
    return client;
  }

  @override
  Future<Client> delete(Client client) async {
    var db = await DBHelper().db;
    db.delete(
      dao.tableName,
      where: '${dao.columnId} = ?',
      whereArgs: [client.id],
    );
    return client;
  }

  @override
  Future<Client> update(Client client) async {
    Database db = await DBHelper().db;
    await db.update(
      dao.tableName,
      dao.toMap(client),
      where: '${dao.columnId} = ?',
      whereArgs: [client.id],
    );
    return client;
  }

  @override
  Future<List<Client>> getClients() async {
    var db = await DBHelper().db;
    List<Map> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }

  Future<Client> getClient(int id) async {
    var db = await DBHelper().db;
    List<Map> maps =
        await db.query(dao.tableName, where: 'id = ?', whereArgs: [id]);
    return maps.isNotEmpty ? dao.fromMap(maps.first) : null;
  }

  Future<Client> getClientByColumn(String columnName, String value) async {
    if (columnName == "mobile_no") {
      if (value.startsWith("255")) {
        value = value.replaceFirst("255", "0");
      } else if (value.startsWith("+255")) {
        value = value.replaceFirst("+255", "0");
      }
    }
    var db = await DBHelper().db;
    List<Map> maps = await db.query(
      dao.tableName,
      where: '$columnName = ?',
      whereArgs: [value],
    );
    return maps.isNotEmpty ? dao.fromMap(maps.first) : null;
  }

  @override
  Future<String> getData(String url,
      {String columnName, String columnValue}) async {
    var response;
    Map<String, String> data = new HashMap();
    data['columnName'] = columnName;
    data['columnValue'] = columnValue;
    try {
      url = url + "?$columnName=" + columnValue;
      response = await http.get(url);
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }
    return response.body;
  }

  @override
  Future<String> postData(Client client) async {
    client.fullName = client.firstName + " " + client.lastName;
    try {
      Options options = Options(contentType: 'application/json');
      Response response = await dio.post(
        "/api/client/register",
        data: dao.toJson(client),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        return json.decode(response.data);
      } else if (response.statusCode == 401) {
        throw Exception("Incorrect Client info passed");
      } else {
        throw Exception('Authentication Error');
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
            "Couldn't connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  @override
  Future<String> login(LoginModel loginModel) async {
    try {
      Options options = Options(contentType: 'application/json');
      Response response = await dio.post(
        "/api/client/login",
        data: this.loginModelToJson(loginModel),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        return json.decode(response.data);
      } else if (response.statusCode == 401) {
        throw Exception("Incorrect login info passed");
      } else {
        throw Exception('Authentication Error');
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
            "Couldn't connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  @override
  Future<String> updateLocation(ClientLocation clientLocation) async {
    try {
      Options options = Options(contentType: 'application/json');
      Response response = await dio.post(
        "/api/client/update-profile",
        data: this.clientLocationToJson(clientLocation),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        return json.decode(response.data);
      } else if (response.statusCode == 401) {
        throw Exception("Client location not updated");
      } else {
        throw Exception('Authentication Error');
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
            "Couldn't connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  //---client meter change---
  @override
  Future<String> updateMeter(ClientMeter clientMeter) async {
    try {
      Options options = Options(contentType: 'application/json');
      Response response = await dio.post(
        "/api/client/update-meter-number",
        data: this.clientMeterToJson(clientMeter),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        return json.decode(response.data);
      } else if (response.statusCode == 401) {
        throw Exception("Client meter not updated");
      } else {
        throw Exception('Authentication Error');
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
            "Couldn't connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }
  //---/client meter change---

  @override
  Future<String> updateLanguageUsed(ClientLanguage clientLanguage) async {
    try {
      Options options = Options(contentType: 'application/json');
      Response response = await dio.post(
        "/api/client/update-language",
        data: this.clientLanguageToJson(clientLanguage),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        return json.decode(response.data);
      } else if (response.statusCode == 401) {
        throw Exception("Client language not updated");
      } else {
        throw Exception('Authentication Error');
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
            "Couldn't connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  //---client notification change---
  @override
  Future<String> updateNotification(
      ClientNotification clientNotification) async {
    try {
      Options options = Options(contentType: 'application/json');
      Response response = await dio.post(
        "/api/client/update-notification-status",
        data: this.clientNotificationToJson(clientNotification),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        return json.decode(response.data);
      } else if (response.statusCode == 401) {
        throw Exception("Client notification was not enabled");
      } else {
        throw Exception('Authentication Error');
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
            "Couldn't connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }
  //---/client notification change---

  //---client subscription change---
  @override
  Future<String> updateSubscription(
      ClientSubscription clientSubscription) async {
    try {
      Options options = Options(contentType: 'application/json');
      Response response = await dio.post(
        "/api/client/update-subscription-status",
        data: this.clientSubscriptionToJson(clientSubscription),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        return json.decode(response.data);
      } else if (response.statusCode == 401) {
        throw Exception("Client subscription was not enabled");
      } else {
        throw Exception('Authentication Error');
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
            "Couldn't connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  @override
  Future<String> unregisterClient(ClientUnregister clientUnregister) async {
    if (clientUnregister.mobileNo.startsWith("255")) {
      clientUnregister.mobileNo =
          clientUnregister.mobileNo.replaceFirst("255", "0");
    } else if (clientUnregister.mobileNo.startsWith("+255")) {
      clientUnregister.mobileNo =
          clientUnregister.mobileNo.replaceFirst("+255", "0");
    }
    try {
      Options options = Options(contentType: 'application/json');
      Response response = await dio.post(
        "/api/client/unregister-device",
        data: this.clientUnregisterToJson(clientUnregister),
        options: options,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        return json.decode(response.data);
      } else if (response.statusCode == 401) {
        throw Exception('Client unregister failed');
      } else {
        throw Exception('Authentication Error');
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception("Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
            "Couldn't connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }
  //---/client subscription change---

  @override
  Future<String> getVerificationToken(Client client) async {
    var response;
    try {
      String url = client.urlRVt + "?mobile_no=" + client.phone;
      response = await http.get(url);
    } catch (e) {
      Fluttertoast.showToast(msg: "Verification token error: ${e.toString()}");
    }
    return response.body;
  }

  Map<String, dynamic> loginModelToJson(LoginModel loginModel) {
    return loginModel.toJson();
  }

  Map<String, dynamic> clientLocationToJson(ClientLocation clientLocation) {
    return clientLocation.toJson();
  }

  Map<String, dynamic> clientMeterToJson(ClientMeter clientMeter) {
    return clientMeter.toJson();
  }

  Map<String, dynamic> clientLanguageToJson(ClientLanguage clientLanguage) {
    return clientLanguage.toJson();
  }

  Map<String, dynamic> clientNotificationToJson(
      ClientNotification clientNotification) {
    return clientNotification.toJson();
  }

  Map<String, dynamic> clientSubscriptionToJson(
      ClientSubscription clientSubscription) {
    return clientSubscription.toJson();
  }

  Map<String, dynamic> clientUnregisterToJson(
      ClientUnregister clientUnregister) {
    return clientUnregister.toJson();
  }
}
