abstract class Dao<T> {
  ///
  /// Client table creation String
  ///
  String get createTableQuery;

  ///
  /// Abstracts for mapping client table
  ///
  T fromMap(Map<String, dynamic> query);
  List<T> fromList(List<Map<String, dynamic>> query);
  Map<String, dynamic> toMap(T object);
  Object fromDb(Map<String, dynamic> map);
}
