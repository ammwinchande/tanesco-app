import 'dart:convert';

class ClientUnregister {
  String deviceId;
  String mobileNo;
  bool connStatus;

  ClientUnregister({
    this.deviceId,
    this.mobileNo,
    this.connStatus,
  });

  factory ClientUnregister.fromJson(Map<String, dynamic> json) {
    return ClientUnregister(
        deviceId: json['device_id'],
        mobileNo: json['mobile_no'],
        connStatus: json['conn_status']);
  }

  Map<String, dynamic> toJson() => {
        'device_id': deviceId,
        'mobile_no': mobileNo,
        'conn_status': connStatus,
      };

  ClientUnregister clientFromJson(String jsonData) {
    return ClientUnregister.fromJson(json.decode(jsonData));
  }

  Map toMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.deviceId} ${this.mobileNo} ${this.connStatus} ";
  }
}
