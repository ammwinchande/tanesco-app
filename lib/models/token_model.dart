import 'dart:convert';

import 'package:http/http.dart' as http;

import '../custom_classes/config.dart';

class TokenModel {
  String token;
  String deviceid;
  String mobileno;
  final String url = Config.baseUrl + "/api/client/verify-token";
  TokenModel({this.token, this.deviceid, this.mobileno});

  factory TokenModel.fromJson(Map<String, dynamic> parsedJson) {
    return TokenModel(
      token: parsedJson['token'],
      deviceid: parsedJson['deviceid'],
      mobileno: parsedJson['token'],
    );
  }

  Map<String, dynamic> toJson() => {
        '"token"': '"' + token + '"',
        '"deviceid"': '"' + deviceid + '"',
        '"mobileno"': '"' + mobileno + '"',
      };

  TokenModel userFromJson(String jsonData) {
    return TokenModel.fromJson(json.decode(jsonData));
  }

  Map getMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.token}";
  }

  Future<String> login() async {
    var response;
    try {
      response = await http.get(url +
          "?mobile_no=" +
          mobileno +
          "&&device=" +
          deviceid +
          "&&token=" +
          token);
    } catch (e) {
      print("Error: $e.toString()");
    }
    return response.body;
  }
}
