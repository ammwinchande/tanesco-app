import 'package:tanesco/custom_classes/config.dart';

class ClientsList {
  final List<Client> clients;

  ClientsList({
    this.clients,
  });

  factory ClientsList.fromJson(List<dynamic> parsedJson) {
    List<Client> clients = new List<Client>();
    clients = parsedJson.map((i) => Client.fromJson(i)).toList();

    return new ClientsList(
      clients: clients,
    );
  }
}

class Client {
  int id;
  String firstName;
  String lastName;
  String fullName;
  String phone;
  String meterno;
  String langUsed;
  String deviceId;
  int regionId;
  int districtId;
  int areaId;
  int streetId;
  String specialMark;
  String createdDate;
  String sessionStartDate;
  String sessionEndDate;
  String baseUrl;

  final String urlGet = Config.baseUrl + "/api/client";
  final String urlPost = Config.baseUrl + "/api/client/register";
  final String urlRVt =
      Config.baseUrl + "/api/client/request-verification-token";

  Client({
    this.id,
    this.firstName,
    this.lastName,
    this.phone,
    this.meterno,
    this.langUsed,
    this.deviceId,
    this.regionId,
    this.districtId,
    this.areaId,
    this.streetId,
    this.specialMark,
    this.baseUrl = Config.baseUrl,
  });

  factory Client.fromJson(Map<String, dynamic> parsedJson) {
    List<String> names = parsedJson["name"].split(" ");

    return Client(
      id: parsedJson['id'],
      firstName: names[0],
      lastName: names[1],
      phone: parsedJson['mobile_no'],
      meterno: parsedJson['meter_no'],
      langUsed: parsedJson['lang_used'],
      deviceId: parsedJson['device_id'],
      regionId: parsedJson['region_id'],
      districtId: parsedJson['district_id'],
      areaId: parsedJson['area_id'],
      streetId: parsedJson['street_id'],
      specialMark: parsedJson['special_mark'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': fullName,
        'mobile_no': phone,
        'meter_no': meterno,
        'lang_used': langUsed,
        'device_id': deviceId,
        'region_id': regionId,
        'district_id': districtId,
        'area_id': areaId,
        'street_id': streetId,
        'special_mark': specialMark,
      };

  @override
  String toString() {
    return "${this.id} ${this.firstName} ${this.lastName} ${this.phone} ${this.meterno} ${this.langUsed} "
        "${this.deviceId} ${this.regionId} ${this.districtId} ${this.areaId} ${this.streetId} ${this.specialMark} ${this.createdDate} ${this.sessionStartDate} ${this.sessionEndDate}";
  }
}
