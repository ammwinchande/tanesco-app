import 'dart:convert';

class ClientLanguage {
  String deviceId;
  String mobileNo;
  String langUsed;

  ClientLanguage({
    this.deviceId,
    this.mobileNo,
    this.langUsed,
  });

  factory ClientLanguage.fromJson(Map<String, dynamic> json) {
    return ClientLanguage(
      deviceId: json['device_id'],
      mobileNo: json['mobile_no'],
      langUsed: json['lang_used'],
    );
  }

  Map<String, dynamic> toJson() => {
        'device_id': deviceId,
        'mobile_no': mobileNo,
        'lang_used': langUsed,
      };

  ClientLanguage clientFromJson(String jsonData) {
    return ClientLanguage.fromJson(json.decode(jsonData));
  }

  Map toMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.deviceId} ${this.deviceId} ${this.langUsed} ";
  }
}
