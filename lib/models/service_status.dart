import 'dart:convert';

class ServiceStatus {
  String deviceId;
  String mobileNo;
  String serviceNo;

  ServiceStatus({
    this.mobileNo,
    this.deviceId,
    this.serviceNo,
  });

  factory ServiceStatus.fromJson(Map<String, dynamic> json) {
    return ServiceStatus(
      mobileNo: json['mobile_no'],
      deviceId: json['device_id'],
      serviceNo: json['service_no'],
    );
  }

  Map<String, dynamic> toJson() => {
        'mobile_no': mobileNo,
        'device_id': deviceId,
        'service_no': serviceNo,
      };

  ServiceStatus serviceFromJson(String jsonData) {
    return ServiceStatus.fromJson(json.decode(jsonData));
  }

  Map toMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.mobileNo} ${this.deviceId} ${this.serviceNo}";
  }
}
