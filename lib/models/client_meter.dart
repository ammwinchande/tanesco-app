import 'dart:convert';

class ClientMeter {
  String deviceId;
  String mobileNo;
  String meterNumber;

  ClientMeter({
    this.deviceId,
    this.mobileNo,
    this.meterNumber,
  });

  factory ClientMeter.fromJson(Map<String, dynamic> json) {
    return ClientMeter(
      deviceId: json['device_id'],
      mobileNo: json['mobile_no'],
      meterNumber: json['meter_no'],
    );
  }

  Map<String, dynamic> toJson() => {
        'device_id': deviceId,
        'mobile_no': mobileNo,
        'meter_no': meterNumber,
      };

  ClientMeter clientFromJson(String jsonData) {
    return ClientMeter.fromJson(json.decode(jsonData));
  }

  Map toMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.deviceId} ${this.deviceId} ${this.meterNumber} ";
  }
}
