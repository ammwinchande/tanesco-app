import 'dart:convert';

class ClientNotification {
  String deviceId;
  String mobileNo;
  String isNotified;

  ClientNotification({
    this.deviceId,
    this.mobileNo,
    this.isNotified,
  });

  factory ClientNotification.fromJson(Map<String, dynamic> json) {
    return ClientNotification(
      deviceId: json['device_id'],
      mobileNo: json['mobile_no'],
      isNotified: json['is_notified'],
    );
  }

  Map<String, dynamic> toJson() => {
        'device_id': deviceId,
        'mobile_no': mobileNo,
        'is_notified': isNotified,
      };

  ClientNotification clientFromJson(String jsonData) {
    return ClientNotification.fromJson(json.decode(jsonData));
  }

  Map toMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.deviceId} ${this.mobileNo} ${this.isNotified} ";
  }
}
