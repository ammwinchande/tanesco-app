import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/client_language.dart';
import 'package:tanesco/models/client_location.dart';
import 'package:tanesco/models/client_meter.dart';
import 'package:tanesco/models/client_notifications.dart';
import 'package:tanesco/models/client_subscription.dart';
import 'package:tanesco/models/client_unregister.dart';
import 'package:tanesco/models/login_model.dart';

abstract class ClientRepository {
  /// Store client to localDB
  ///
  /// @param Client client
  /// @return client
  Future<Client> insert(Client client);

  /// Update client details in localDB
  ///
  /// @param Client client
  /// @return client
  Future<Client> update(Client client);

  /// Delete client data in localDB
  ///
  /// @param Client client
  /// @return client
  Future<Client> delete(Client client);

  /// Return list of clients in localDB
  ///
  /// @param Client client
  /// @return client
  Future<List<Client>> getClients();

  /// Post registered client details to API
  ///
  /// @param Client client
  /// @return String message
  Future<String> postData(Client client);

  /// Perform login check from API using login details
  ///
  /// @param LoginModel client
  /// @return String message
  Future<String> login(LoginModel loginModel);

  /// Post client location details to API
  ///
  /// @param ClientLocation clientLocation
  /// @return String message
  Future<String> updateLocation(ClientLocation clientLocation);

  /// Post client meter details to API
  ///
  /// @param ClientMeter clientMeter
  /// @return String message
  Future<String> updateMeter(ClientMeter clientMeter);

  /// Fetch data from API based on params
  ///
  /// @param String url, String variable, String value
  /// @return json data
  Future<String> getData(String url, {String columnName, String columnValue});

  /// Request client verification token
  ///
  /// @param Client client
  /// @return String message
  Future<String> getVerificationToken(Client client);

  /// Post client notification details to API
  ///
  /// @param ClientNotification clientNotification
  /// @return String message
  Future<String> updateNotification(ClientNotification clientNotification);

  /// Post client notification details to API
  ///
  /// @param ClientLanguage clientLanguage
  /// @return String message
  Future<String> updateLanguageUsed(ClientLanguage clientLanguage);

  /// Post client subscription details to API
  ///
  /// @param ClientSubscription clientSubscription
  /// @return String message
  Future<String> updateSubscription(ClientSubscription clientSubscription);

  /// Post client status for unregister to API
  ///
  /// @param ClientSubscription clientSubscription
  /// @return String message
  Future<String> unregisterClient(ClientUnregister clientUnregister);
}
