class Result {
  String status;
  String message;
  Result({
    this.status,
    this.message,
  });
  factory Result.fromJson(Map<String, dynamic> parsedJson) {
    return Result(status: parsedJson['status'], message: parsedJson['message']);
  }
  Map<String, dynamic> toJson() =>
      {'"status"': '"' + status + '"', '"message"': '"' + message + '"'};

  @override
  String toString() {
    return "${this.status} ${this.message}";
  }
}
