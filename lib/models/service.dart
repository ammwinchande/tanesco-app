import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sqflite/sqflite.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/db/dbhelper.dart';

class ServiceList {
  List<Service> serviceList;

  ServiceList({this.serviceList});

  ServiceList.fromJson(List<dynamic> parsedJson) {
    serviceList = new List<Service>();
    parsedJson.forEach((service) {
      serviceList.add(Service.fromJson(service));
    });
  }
}

class Service {
  int id;
  String name;
  String icon;

  Service({
    this.id,
    this.name,
    this.icon,
  });

  factory Service.fromJson(Map<String, dynamic> json) {
    return Service(
      id: json['id'],
      name: json['name'],
      icon: json['service_icon'],
    );
  }

  Map<String, dynamic> toJson() => {
        '"id"': id,
        '"name"': '"' + name + '"',
        '"service_icon"': '"' + icon + '"',
      };

  Service serviceFromJson(String jsonData) {
    return Service.fromJson(json.decode(jsonData));
  }

  Map toMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.id} ${this.name} ${this.icon}";
  }

  Future<List<Service>> getServices() async {
    http.Response response = await http.get(Config.baseUrl + '/api/service');
    return parseServices(response.body);
  }

  List<Service> parseServices(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Service>((json) => Service.fromJson(json)).toList();
  }

  void save() async {
    var dbClient = await DBHelper().db;
    await dbClient.transaction((txn) async {
      return dbClient.insert('service', this.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
    });
  }

  Future<List<Service>> getFromDb() async {
    var dbClient = await DBHelper().db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM service');
    List<Service> services = new List();
    for (int i = 0; i < list.length; i++) {
      services.add(
        new Service(
          id: list[i]["id"],
          name: list[i]["name"],
          icon: list[i]["service_icon"],
        ),
      );
    }
    return services;
  }

  void update() async {
    this.getServices().then(
      (services) {
        services.forEach(
          (e) => print(
            "ID: " +
                e.id.toString() +
                "NAME: " +
                e.name +
                "ICON: " +
                e.icon.toString(),
          ),
        );
      },
    );
  }
}
