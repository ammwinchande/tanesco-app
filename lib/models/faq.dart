import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tanesco/custom_classes/config.dart';

class Faq {
  String question;
  String answer;

  Faq({
    this.question,
    this.answer,
  });
  factory Faq.fromJson(Map<String, dynamic> parsedJson) {
    return Faq(question: parsedJson['question'], answer: parsedJson['answer']);
  }
  Map<String, dynamic> toJson() =>
      {'"question"': '"' + question + '"', '"answer"': '"' + answer + '"'};
  Faq userFromJson(String jsonData) {
    return Faq.fromJson(json.decode(jsonData));
  }

  Map getMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.question} ${this.answer}";
  }

  Future<List<Faq>> get() async {
    var url = Config.baseUrl + 'api/faq';
    http.Response response = await http.get(url);
    return parseFaqs(response.body);
  }

  List<Faq> parseFaqs(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Faq>((json) => Faq.fromJson(json)).toList();
  }
}
