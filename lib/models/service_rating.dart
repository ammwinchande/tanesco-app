import 'dart:convert';

class ServiceRating {
  String mobileNo;
  String deviceId;
  String serviceNo;
  double rating;
  String feedback;

  ServiceRating({
    this.mobileNo,
    this.deviceId,
    this.serviceNo,
    this.rating,
    this.feedback,
  });

  factory ServiceRating.fromJson(Map<String, dynamic> json) {
    return ServiceRating(
      mobileNo: json['mobile_no'],
      deviceId: json['device_id'],
      serviceNo: json['service_no'],
      rating: json['rating'],
      feedback: json['feedback'],
    );
  }

  Map<String, dynamic> toJson() => {
        'mobile_no': mobileNo,
        'device_id': deviceId,
        'service_no': serviceNo,
        'rating': rating,
        'feedback': feedback,
      };

  ServiceRating serviceFromJson(String jsonData) {
    return ServiceRating.fromJson(json.decode(jsonData));
  }

  Map toMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return '${this.mobileNo} ${this.deviceId} ${this.serviceNo} ${this.rating} ${this.feedback}';
  }
}
