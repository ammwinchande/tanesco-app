import 'dart:convert';

class ClientLocation {
  String deviceId;
  String mobileNo;
  int regionId;
  int districtId;
  int areaId;
  int streetId;
  String specialMark;

  ClientLocation({
    this.deviceId,
    this.mobileNo,
    this.regionId,
    this.districtId,
    this.areaId,
    this.streetId,
    this.specialMark,
  });

  factory ClientLocation.fromJson(Map<String, dynamic> json) {
    return ClientLocation(
      deviceId: json['device_id'],
      mobileNo: json['mobile_no'],
      regionId: json['region_id'],
      districtId: json['district_id'],
      areaId: json['area_id'],
      streetId: json['street_id'],
      specialMark: json['special_mark'],
    );
  }

  Map<String, dynamic> toJson() => {
        'device_id': deviceId,
        'mobile_no': mobileNo,
        'region_id': regionId,
        'district_id': districtId,
        'area_id': areaId,
        'street_id': streetId,
        'special_mark': specialMark,
      };

  ClientLocation clientFromJson(String jsonData) {
    return ClientLocation.fromJson(json.decode(jsonData));
  }

  Map toMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.deviceId} ${this.deviceId} ${this.regionId} ${this.districtId} ${this.areaId} ${this.streetId} ${this.specialMark}";
  }
}
