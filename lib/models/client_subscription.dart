import 'dart:convert';

class ClientSubscription {
  String deviceId;
  String mobileNo;
  String isSubscribed;

  ClientSubscription({
    this.deviceId,
    this.mobileNo,
    this.isSubscribed,
  });

  factory ClientSubscription.fromJson(Map<String, dynamic> json) {
    return ClientSubscription(
      deviceId: json['device_id'],
      mobileNo: json['mobile_no'],
      isSubscribed: json['is_subscribed'],
    );
  }

  Map<String, dynamic> toJson() => {
        'device_id': deviceId,
        'mobile_no': mobileNo,
        'is_subscribed': isSubscribed,
      };

  ClientSubscription clientFromJson(String jsonData) {
    return ClientSubscription.fromJson(json.decode(jsonData));
  }

  Map toMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.deviceId} ${this.mobileNo} ${this.isSubscribed} ";
  }
}
