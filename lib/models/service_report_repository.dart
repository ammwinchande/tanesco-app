import 'package:tanesco/models/service_rating.dart';
import 'package:tanesco/models/service_report.dart';
import 'package:tanesco/models/service_status.dart';

abstract class ServiceReportRepository {
  /// Post reported problem/request details to API
  ///
  /// @param ServiceReport serviceReport
  /// @return String message
  Future<String> postData(ServiceReport serviceReport);

  /// Fetch report status from API
  ///
  /// @param ServiceStatus serviceStatus
  /// @return String message
  Future<String> requestStatus(ServiceStatus serviceStatus);

  /// Update report rating from Client
  ///
  /// @param ServiceRating serviceRating
  /// @return String message
  Future<String> addFeedback(ServiceRating serviceRating);
}
