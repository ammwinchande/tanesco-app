import 'dart:convert';

class LoginModel {
  String mobileNo;
  String deviceId;

  LoginModel({this.mobileNo, this.deviceId});

  factory LoginModel.fromJson(Map<String, dynamic> parsedJson) {
    return LoginModel(
      mobileNo: parsedJson['mobile_no'],
      deviceId: parsedJson['device_id'],
    );
  }

  Map<String, dynamic> toJson() => {
        'mobile_no': mobileNo,
        'device_id': deviceId,
      };

  LoginModel userFromJson(String jsonData) {
    return LoginModel.fromJson(json.decode(jsonData));
  }

  Map getMap() {
    Map map = json.decode(this.toJson().toString());
    return map;
  }

  @override
  String toString() {
    return "${this.mobileNo} ${this.deviceId}";
  }

  bool isMobileNo() {
    return mobileNo.startsWith("255") ||
        mobileNo.startsWith("+255") ||
        mobileNo.startsWith("0");
  }
}
