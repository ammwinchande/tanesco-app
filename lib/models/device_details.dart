import 'package:device_info/device_info.dart';
import 'dart:io' show Platform;
import 'package:flutter/services.dart' show PlatformException;

class DeviceDetails {
  static Future<String> getDeviceDetails() async {
    String deviceName;
    String deviceVersion;
    String identifier;
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.model;
        deviceVersion = build.version.release;
        identifier = build.androidId;
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceName = data.name;
        deviceVersion = data.systemVersion;
        identifier = data.identifierForVendor; //UUID for iOS
      }
    } on PlatformException {
      return 'Failed to get platform version';
    }
    return "$deviceName/$deviceVersion/$identifier";
  }
}
