import 'package:tanesco/custom_classes/config.dart';

class ServiceReport {
  int id;
  String region;
  String district;
  String area;
  String street;
  String specialMark;
  String picture;
  String video;
  String serviceNumber;
  int serviceId;
  String subServiceId;
  String remarks;
  String rating;
  String feedback;
  String meterNumber;
  String details;
  String mobileNo;
  String deviceId;
  String baseUrl;
  bool isPrimary;

  final String urlPostReport = Config.baseUrl + "/api/service/report";
  final String urlGetStatus = Config.baseUrl + "/api/service/get-status";
  final String urlReportSummary =
      Config.baseUrl + "/api/service/report-summary";
  ServiceReport({
    this.id,
    this.region,
    this.district,
    this.area,
    this.street,
    this.specialMark,
    this.picture,
    this.video,
    this.serviceNumber,
    this.serviceId,
    this.subServiceId,
    this.remarks,
    this.rating,
    this.feedback,
    this.meterNumber,
    this.details,
    this.mobileNo,
    this.deviceId,
    this.isPrimary,
    this.baseUrl = Config.baseUrl,
  });

  factory ServiceReport.fromJson(Map<String, dynamic> parsedJson) {
    return ServiceReport(
      id: parsedJson['id'],
      region: parsedJson['region'],
      district: parsedJson['district'],
      area: parsedJson['area'],
      street: parsedJson['street'],
      specialMark: parsedJson['special_mark'],
      picture: parsedJson['picture'],
      video: parsedJson['video'],
      serviceNumber: parsedJson['service_no'],
      serviceId: parsedJson['service_id'],
      subServiceId: parsedJson['sub_service_id'],
      remarks: parsedJson['remarks'],
      rating: parsedJson['rating'],
      feedback: parsedJson['feedback'],
      meterNumber: parsedJson['meter_number'],
      details: parsedJson['details'],
      mobileNo: parsedJson['mobile_no'],
      deviceId: parsedJson['device_id'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'region': region,
        'district': district,
        'area': area,
        'street': street,
        'special_mark': specialMark,
        'picture': picture,
        'video': video,
        'service_no': serviceNumber,
        'service_id': serviceId,
        'sub_service_id': subServiceId,
        'remarks': remarks,
        'rating': rating,
        'feedback': feedback,
        'meter_number': meterNumber,
        'details': details,
        'device_id': deviceId,
        'mobile_no': mobileNo,
        'is_primary': isPrimary,
      };

  @override
  String toString() {
    return "${this.id} ${this.region} ${this.district} ${this.area} ${this.street} ${this.specialMark}"
        "${this.picture} ${this.video} ${this.serviceNumber} ${this.serviceId} ${this.subServiceId} ${this.remarks} ${this.feedback} ${this.rating} ${this.meterNumber} ${this.isPrimary} ${this.details} ${this.deviceId} ${this.mobileNo}";
  }
}
