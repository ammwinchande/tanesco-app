import 'package:flutter/material.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/client_notifications.dart';
import 'package:tanesco/models/client_subscription.dart';

class Setting extends StatefulWidget {
  final String title;
  final Client client;

  Setting({Key key, this.title, this.client}) : super(key: key);
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  CustomAppBar customAppBar = CustomAppBar();

  final clientNotification = ClientNotification();
  final clientSubscription = ClientSubscription();

  bool isNotified = true; // switch
  bool isSubscribed = true; // checkbox

//  PERFORM api call for changing notification

  void onChangedSwitch(bool value) {
    setState(() {
      isNotified = value;
    });
  }

  void onChangedCheckbox(bool value) {
    setState(() {
      isSubscribed = value;
    });
  }

  renderBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Column(
          children: <Widget>[
            ListTile(
              title: Text(
                "Notifications",
                style: TextStyle(
                  fontSize: 18.0,
                  fontFamily: 'Raleway',
                ),
              ),

              subtitle: Text(
                "Receive application services notifications",
                style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: 'Raleway',
                  color: Colors.grey,
                  //fontStyle: FontStyle.italic,
                ),
              ),
              trailing: Switch(
                value: isNotified,
                onChanged: (bool value) => onChangedSwitch(value),
              ),
              //leading: Icon(Icons.add_box),
              contentPadding:
                  EdgeInsets.only(top: 10.0, left: 25.0, right: 25.0),

//              isThreeLine: true,
//              onTap: () {
//                print("On Tap is fired");
//              },
            ),
            _divider(),
            ListTile(
              title: Text(
                "Subscribe",
                style: TextStyle(
                  fontSize: 18.0,
                  fontFamily: 'Raleway',
                ),
              ),
              subtitle: Text(
                "Subscribe to receive latest news from TANESCO",
                style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: 'Raleway',
                  color: Colors.grey,
                  //fontStyle: FontStyle.italic,
                ),
              ),
              //trailing: Icon(Icons.home),
              trailing: Switch(
                  value: isSubscribed,
                  onChanged: (bool value) {
                    onChangedCheckbox(value);
                  }),
              //leading: Icon(Icons.add_box),
              contentPadding: EdgeInsets.only(left: 25.0, right: 25.0),

//              isThreeLine: true,
//              onTap: () {
//                print("On Tap is fired");
//              },
            )
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          customAppBar.getAppBar(title: 'Settings'),
          SliverToBoxAdapter(
            child: Container(
              child: renderBody(),
            ),
          )
        ],
      ),
      bottomNavigationBar: customAppBar.getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }

  _divider() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
            child: Divider(
              color: Colors.green.shade900,
            ),
          ),
        )
      ],
    );
  }
}
