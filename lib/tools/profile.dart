import 'dart:convert';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:strings/strings.dart';
import 'package:tanesco/custom_classes/alert.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/dashboard.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/client_database_repository.dart';
import 'package:tanesco/models/client_language.dart';
import 'package:tanesco/models/client_location.dart';
import 'package:http/http.dart' as http;

import 'package:tanesco/models/client_meter.dart';
import 'package:tanesco/service_locator.dart';

class Profile extends StatefulWidget {
  final String title;
  final Client client;
  Profile({Key key, this.title, this.client}) : super(key: key);
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  CustomAppBar customAppBar = CustomAppBar(); // for appBar
  final String baseUrl = Config.baseUrl;
  final _formKey = GlobalKey<FormState>();
  final _client = Client(); // store client info for sending via API
  final _clientLocation = ClientLocation(); // send location details to API
  final _clientMeter = ClientMeter(); // send meter details to API
  final _clientLanguage = ClientLanguage(); // send language used details to API

  TextEditingController _meternoTextEditingController = TextEditingController();
  ClientDatabaseRepository clientDbRepository = ClientDatabaseRepository();

  String _selectedRegion;
  String _selectedDistrict;
  String _selectedArea;
  String _selectedStreet;

  String meter = 'Change meter';
  String location = 'Change location';
  String language = 'Change Language';

  List regions = List();
  List districts = List();
  List areas = List();
  List streets = List();

  List meterVerificationMessage = List(); // store verification message from API

  TextStyle style = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 16.0,
  );

  int _radioValue;

  /// FETCH regions from API
  ///
  /// @param
  /// @return String message
  ///
  Future<String> getRegions() async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/region'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      regions = responseBody;
    });
    return 'Success';
  }

  /// FETCH districts from API based on
  ///
  /// @param int regionId
  /// @return String message
  Future<String> getDistricts({regionId}) async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/district/?region_id=$regionId'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      districts = responseBody;
    });
    return 'Success';
  }

  /// FETCH areas from API based on district_id
  ///
  /// @param int districtId
  /// @return String message
  Future<String> getAreas({districtId}) async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/area/?district_id=$districtId'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      areas = responseBody;
    });
    return 'Success';
  }

  /// FETCH streets from API based on area_id
  ///
  /// @param int areaId
  /// @return String message
  Future<String> getStreets({areaId}) async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/street/?area_id=$areaId'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      streets = responseBody;
    });
    return 'Success';
  }

  /// VERIFY meter number from API
  ///
  /// @param String meterno
  /// @return String message
  Future<String> verifyMeterNumber(String meterno) async {
    var response = await http.get(
      Uri.encodeFull(
        baseUrl + '/api/client/verify-meter/?meter_number=$meterno',
      ),
      headers: {"Accept": "application/json"},
    );
    var responseBody = json.decode(response.body);

    setState(() {
      meterVerificationMessage = responseBody;
    });
    return 'Success';
  }

  @override
  void dispose() {
    super.dispose();
    _meternoTextEditingController.dispose();
  }

  @override
  void initState() {
    super.initState();
    this.getRegions(); // store regions to local variable
    _selectedRegion = widget.client.regionId.toString();
    this.getDistricts(regionId: _selectedRegion);
    _selectedDistrict = widget.client.districtId.toString();
    this.getAreas(districtId: _selectedDistrict);
    _selectedArea = widget.client.areaId.toString();
    this.getStreets(areaId: _selectedArea);
    _selectedStreet = widget.client.streetId.toString();

    widget.client.meterno != null
        ? _meternoTextEditingController.text = widget.client.meterno
        : _meternoTextEditingController.text = '';
    _meternoTextEditingController.addListener(meternoTextEditingListener);

    widget.client.langUsed != null
        ? widget.client.langUsed.toLowerCase() == 'english'
            ? _radioValue = 1
            : _radioValue = 2
        : _radioValue = 1;
  }

  ///
  /// REALTIME typed meter number validation
  /// Test meter: 07041236790
  ///
  meternoTextEditingListener() {
    if (_meternoTextEditingController.text.length >= 5) {
      Future.delayed((Duration(seconds: 1))).then((message) {
        this.verifyMeterNumber(_meternoTextEditingController.text);
      });
    }
  }

  /// RENDER REGIONS
  ///
  /// @param
  /// @return ListTile _renderRegionTile
  ListTile _renderRegionTile() {
    return ListTile(
      title: FormField<String>(
        initialValue: _selectedRegion,
        validator: (value) {
          if (value == null) {
            return "Select Region";
          } else {
            return null;
          }
        },
        onSaved: (value) {
          _client.regionId = int.parse(value);
          _clientLocation.regionId = int.parse(value);
        },
        builder: (
          FormFieldState<String> state,
        ) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InputDecorator(
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.all(0.0),
                  labelText: 'Region *',
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: Text("Select Region"),
                    value: _selectedRegion,
                    onChanged: (selectedRegion) {
                      state.didChange(selectedRegion);
                      setState(() {
                        _selectedRegion = selectedRegion;
                        _selectedDistrict = null;
                        _selectedArea = null;
                        _selectedStreet = null;
                        this.getDistricts(regionId: _selectedRegion);
                      });
                    },
                    items: regions.map((region) {
                      return DropdownMenuItem(
                        child: Text(region['name'].toString().toUpperCase()),
                        value: region['id'].toString(),
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Text(
                state.hasError ? state.errorText : '',
                style:
                    TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
              ),
            ],
          );
        },
      ),
    );
  }

  /// RENDER DISTRICTS
  ///
  /// @param
  /// @return ListTile _renderDistrictTile
  ListTile _renderDistrictTile() {
    return ListTile(
      title: FormField<String>(
        initialValue: _selectedDistrict,
        validator: (value) {
          if (value == null) {
            return "Select District";
          } else {
            return null;
          }
        },
        onSaved: (value) {
          _client.districtId = int.parse(value);
          _clientLocation.districtId = int.parse(value);
        },
        builder: (
          FormFieldState<String> state,
        ) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InputDecorator(
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.all(0.0),
                  labelText: 'District *',
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: Text("Select District"),
                    value: _selectedDistrict,
                    onChanged: (selectedDistrict) {
                      state.didChange(selectedDistrict);
                      setState(() {
                        _selectedRegion = this._selectedRegion;
                        _selectedDistrict = selectedDistrict;
                        _selectedArea = null;
                        _selectedStreet = null;
                        this.getAreas(districtId: _selectedDistrict);
                      });
                    },
                    items: districts.map((district) {
                      return DropdownMenuItem(
                        child: Text(district['name'].toString().toUpperCase()),
                        value: district['id'].toString(),
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Text(
                state.hasError ? state.errorText : '',
                style:
                    TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
              ),
            ],
          );
        },
      ),
    );
  }

  /// RENDER AREAS
  ///
  /// @param
  /// @return ListTile _renderAreaTile
  ListTile _renderAreaTile() {
    return ListTile(
      title: FormField<String>(
        initialValue: _selectedArea,
        validator: (value) {
          if (value == null) {
            return "Select Area";
          } else {
            return null;
          }
        },
        onSaved: (value) {
          _client.areaId = int.parse(value);
          _clientLocation.areaId = int.parse(value);
        },
        builder: (
          FormFieldState<String> state,
        ) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InputDecorator(
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.all(0.0),
                  labelText: 'Area *',
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: Text("Select Area"),
                    value: _selectedArea,
                    onChanged: (selectedArea) {
                      state.didChange(selectedArea);
                      setState(() {
                        _selectedRegion = this._selectedRegion;
                        _selectedDistrict = this._selectedDistrict;
                        _selectedArea = selectedArea;
                        _selectedStreet = null;
                        this.getStreets(areaId: _selectedArea);
                      });
                    },
                    items: areas.map((area) {
                      return DropdownMenuItem(
                        child: Text(area['name'].toString().toUpperCase()),
                        value: area['id'].toString(),
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Text(
                state.hasError ? state.errorText : '',
                style:
                    TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
              ),
            ],
          );
        },
      ),
    );
  }

  /// RENDER STREETS
  ///
  /// @param
  /// @return ListTile _renderStreetTile
  ListTile _renderStreetTile() {
    return ListTile(
      title: FormField<String>(
        initialValue: _selectedStreet,
        validator: (value) {
          if (value == null) {
            return "Select Street";
          } else {
            return null;
          }
        },
        onSaved: (value) {
          _client.streetId = int.parse(value);
          _clientLocation.streetId = int.parse(value);
        },
        builder: (
          FormFieldState<String> state,
        ) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InputDecorator(
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.all(0.0),
                  labelText: 'Street *',
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: Text("Select Street"),
                    value: _selectedStreet,
                    onChanged: (selectedStreet) {
                      state.didChange(selectedStreet);
                      setState(() {
                        _selectedStreet = selectedStreet;

                        _selectedRegion = this._selectedRegion;
                        _selectedDistrict = this._selectedDistrict;
                        _selectedArea = this._selectedArea;
                      });
                    },
                    items: streets.map((street) {
                      return DropdownMenuItem(
                        child: Text(street['name'].toString().toUpperCase()),
                        value: street['id'].toString(),
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Text(
                state.hasError ? state.errorText : '',
                style:
                    TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
              ),
            ],
          );
        },
      ),
    );
  }

  /// RENDER SPECIAL MARK
  ///
  /// @param
  /// @return ListTile _renderSpecialMarkTile
  ListTile _renderSpecialMarkTile() {
    return ListTile(
      title: TextFormField(
        maxLength: 50,
        decoration: InputDecoration(
          icon: Icon(Icons.location_on),
          hintText: 'Your location famous name?',
          labelText: 'Special Mark',
        ),
        onSaved: (value) {
          _client.specialMark = value;
          _clientLocation.specialMark = value;
        },
        validator: (value) {
          if (value.isEmpty) {
            return null;
          } else if (value.length != 0) {
            if (value.length < 3) {
              return 'Please enter special mark';
            } else {
              return null;
            }
          }
          return null;
        },
        initialValue: widget.client.specialMark,
      ),
    );
  }

  /// RENDER meter number form field
  ///
  /// @param String reportedMeter
  /// @return TextFormField _buildMeterNumberFormField
  TextFormField _buildMeterNumberFormField({String reportedMeter}) {
    return TextFormField(
      obscureText: false,
      style: style,
      maxLength: 11,
      autofocus: true,
      controller: _meternoTextEditingController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        icon: Icon(FontAwesomeIcons.tachometerAlt),
        hintText: "Enter Your Meter Number",
        labelText: "Enter Your Meter Number",
      ),
      validator: (String value) {
        String patttern = r'(^[0-9]*$)';
        String limitPattern =
            r'^(\d{5}|\d{7}|\d{9}|\d{11})$'; // enbale this to have starting from 5
        RegExp regExp = new RegExp(patttern);
        RegExp regExpLimit = new RegExp(limitPattern);
        if (value.trim().isEmpty) {
          return 'Meter Number is required';
        }
        if (value.length == 0) {
          return 'Meter Number is Required';
        } else if (!regExp.hasMatch(value)) {
          return 'Enter Correct Meter Number';
        } else if (!regExpLimit.hasMatch(value)) {
          return 'Meter Number is not valid';
        } else if (meterVerificationMessage.isEmpty ||
            meterVerificationMessage[0]['Meter'] == null) {
          return 'Meter Number is not valid';
        }
        return null;
      },
      onSaved: (String value) {
        _clientMeter.meterNumber = value;
      },
      onChanged: (value) {
        value.isEmpty
            ? value = '0'
            : Future.delayed(
                Duration(seconds: 1),
                () {
                  verifyMeterNumber(value);
                },
              );
      },
      // initialValue: widget.client.meterno,
    );
  }

  /// REALTIME meter number validation check
  /// when meet criteria e.g. 07041236790
  ///
  /// @param
  /// @return Text
  Text _realTimeMeterValidation() {
    if (_meternoTextEditingController.text.length < 5) {
      return Text('');
    } else if (meterVerificationMessage == null ||
        meterVerificationMessage.isEmpty) {
      return Text(
        '- Not Found',
        textAlign: TextAlign.left,
        style: style.copyWith(
          fontStyle: FontStyle.italic,
          color: Colors.red.shade600,
        ),
      );
    } else {
      return Text(
        '- ${camelize(meterVerificationMessage[0]['Meter'].toString())}',
        textAlign: TextAlign.start,
        style: style.copyWith(
          color: Colors.green.shade600,
          fontStyle: FontStyle.italic,
        ),
      );
    }
  }

  /// BUILD meter number form fields
  ///
  /// @return Container _renderMeterNumberFormFields
  Container _renderMeterNumberFormFields() {
    return Container(
      child: Column(
        children: <Widget>[
          _buildMeterNumberFormField(),
          _realTimeMeterValidation(),
        ],
      ),
    );
  }

  ExpansionTile _meterExpansionTile() {
    return ExpansionTile(
      title: Text(
        this.meter.toUpperCase(),
        //style: style.copyWith(fontWeight: FontWeight.w500),
      ),
      children: <Widget>[
        Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 36.0),
              child: _renderMeterNumberFormFields(),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 16.0),
              child: _updateMeterButton(),
            ),
          ],
        ),
      ],
    );
  }

  /// DISPLAY error on updating client info
  ///
  /// @param Client client
  /// @return Fluttertoast _clientUpdateFailure
  _clientUpdateFailure({@required Client client}) {
    return Fluttertoast.showToast(
      msg: 'Failed to update local client DB $client',
      backgroundColor: Colors.redAccent,
      textColor: Colors.white,
    );
  }

  ///
  /// PERFORM meter number update
  ///
  _performMeterNumberUpdate() {
    /// DIALOG loader
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );

    if (_formKey.currentState.validate()) {
      _client.deviceId = widget.client.deviceId;
      _clientMeter.deviceId = widget.client.deviceId;
      _clientMeter.mobileNo = widget.client.phone;
      _formKey.currentState.save();

      pr.style(
        message: 'Updating meter number...',
        borderRadius: 5.0,
        backgroundColor: Colors.white,
        progressWidget: LinearProgressIndicator(
          backgroundColor: CustomColors.green,
          valueColor: AlwaysStoppedAnimation(CustomColors.yellow),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.fastLinearToSlowEaseIn,
        progressTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 13.0,
          fontWeight: FontWeight.w400,
        ),
        messageTextStyle: style.copyWith(color: Colors.grey.shade700),
      );
      pr.show();

      Future.delayed((Duration(seconds: 1))).then((value) {
        /// Update client location details on API
        clientDbRepository.updateMeter(_clientMeter).then(
          (jsonData) {
            if (jsonData.isNotEmpty) {
              var responseJson = json.decode(jsonData)["data"];
              var responseStatus = json.decode(jsonData)["status"];

              /// Update local client location details
              Client client = Client();
              if (responseStatus == 'success') {
                Future.delayed((Duration(seconds: 1))).then((value) {
                  clientDbRepository
                      .getClient(widget.client.id)
                      .then((currClient) {
                    client = currClient;
                    client.meterno = _clientMeter.meterNumber;

                    clientDbRepository.update(client).then((updatedClient) {
                      if (updatedClient != null) {
                        pr.update(
                          message: 'Almost done...',
                          progressWidget: LinearProgressIndicator(
                            backgroundColor: CustomColors.green,
                            valueColor:
                                AlwaysStoppedAnimation(CustomColors.yellow),
                          ),
                        );

                        Future.delayed(Duration(seconds: 1)).then(
                          (value) {
                            // send to verification token
                            pr.hide().whenComplete(() {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => Dashboard(
                                    title: Config.homeLabel,
                                    client: client,
                                  ),
                                ),
                              );

                              _displayNotification(message: responseJson);
                            });
                          },
                        );
                      } else {
                        Alert().showErrorAlert(
                            context: context,
                            message: 'Meter Number Update Failed');
                      }
                    });
                  });
                });
              } else {
                _clientUpdateFailure(client: client);
              }
            }
          },
        );
      });
    }
  }

  /// RENDER meter number button
  ///
  /// @return Material updateMeterButton
  Material _updateMeterButton() {
    return Material(
      elevation: 16.0,
      borderRadius: BorderRadius.circular(50.0),
      color: CustomColors.green,
      child: MaterialButton(
        padding: const EdgeInsets.all(16.0),
        onPressed: () => _performMeterNumberUpdate(),
        child: Text(
          "update".toUpperCase(),
          textAlign: TextAlign.center,
          style: style.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  ExpansionTile _locationExpansionTile() {
    return ExpansionTile(
      title: Text(
        this.location.toUpperCase(),
        style: style.copyWith(fontWeight: FontWeight.w500),
      ),
      backgroundColor: Theme.of(context).accentColor.withOpacity(0.025),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              _renderRegionTile(),
              _renderDistrictTile(),
              _renderAreaTile(),
              _renderStreetTile(),
              _renderSpecialMarkTile(),
              _updateLocationButton(),
            ],
          ),
        ),
      ],
    );
  }

  ///
  /// PERFORM client location details
  ///
  _performLocationUpdate() {
    /// DIALOG loader
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );

    if (_formKey.currentState.validate()) {
      _client.deviceId = widget.client.deviceId;
      _clientLocation.deviceId = widget.client.deviceId;
      _clientLocation.mobileNo = widget.client.phone;
      _formKey.currentState.save();

      pr.style(
        message: 'Updating Location...',
        borderRadius: 5.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.fastLinearToSlowEaseIn,
        progressTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 13.0,
          fontWeight: FontWeight.w400,
        ),
        messageTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 19.0,
          fontWeight: FontWeight.w600,
        ),
      );
      pr.show();

      Future.delayed((Duration(seconds: 1))).then((value) {
        /// Update client location details on API
        clientDbRepository.updateLocation(_clientLocation).then(
          (jsonData) {
            if (jsonData.isNotEmpty) {
              var responseJson = json.decode(jsonData)["data"];
              var responseStatus = json.decode(jsonData)["status"];

              /// Update local client location details
              Client client = Client();
              if (responseStatus == 'success') {
                Future.delayed((Duration(seconds: 1))).then((value) {
                  clientDbRepository
                      .getClient(widget.client.id)
                      .then((currClient) {
                    client = currClient;
                    client.regionId = _client.regionId;
                    client.districtId = _client.districtId;
                    client.areaId = _client.areaId;
                    client.streetId = _client.streetId;
                    client.specialMark = _client.specialMark;

                    clientDbRepository.update(client).then((updatedClient) {
                      if (updatedClient != null) {
                        pr.update(
                          message: 'Almost done...',
                          progressWidget: LinearProgressIndicator(
                            backgroundColor: CustomColors.green,
                            valueColor:
                                AlwaysStoppedAnimation(CustomColors.yellow),
                          ),
                        );

                        Future.delayed(Duration(seconds: 1)).then(
                          (value) {
                            // send to verification token
                            pr.hide().whenComplete(() {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => Dashboard(
                                    title: Config.homeLabel,
                                    client: client,
                                  ),
                                ),
                              );

                              _displayNotification(message: responseJson);
                            });
                          },
                        );
                      } else {
                        Alert().showErrorAlert(
                          context: context,
                          message: 'Location Update Failed',
                        );
                      }
                    });
                  });
                });
              } else {
                _clientUpdateFailure(client: client);
              }
            }
          },
        );
      });
    }
  }

  /// RENDER location update button
  ///
  /// @return Material updateLocationButton
  Material _updateLocationButton() {
    return Material(
      elevation: 16.0,
      borderRadius: BorderRadius.circular(50.0),
      color: CustomColors.green,
      child: MaterialButton(
        padding: const EdgeInsets.all(16.0),
        onPressed: () => _performLocationUpdate(),
        child: Text(
          "update".toUpperCase(),
          textAlign: TextAlign.center,
          style: style.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
      switch (_radioValue) {
        case 1:
          _setLanguageSelected(selectedValue: value);
          break;
        case 2:
          _setLanguageSelected(selectedValue: value);
          break;
      }
    });
  }

  _setLanguageSelected({@required int selectedValue}) {
    if (selectedValue == 1) {
      _clientLanguage.langUsed = 'english';
    }
    if (selectedValue == 2) {
      _clientLanguage.langUsed = 'swahili';
    }
  }

  _setLangCode({@required String language}) {
    var storageService = sl.get("LocalStorageService");
    storageService.langCode = language;
  }

  /// RENDER language selection radios
  ///
  /// @return Column
  _renderLanguageRadios() {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Radio(
                value: 1,
                groupValue: _radioValue,
                onChanged: _handleRadioValueChange,
              ),
              Text(
                Config.labelLangEn,
                style: TextStyle(fontSize: 16.0),
              ),
            ],
          ),
        ),
        Container(
          child: Row(
            children: <Widget>[
              Radio(
                value: 2,
                groupValue: _radioValue,
                onChanged: _handleRadioValueChange,
              ),
              Text(
                Config.labelLangSw,
                style: TextStyle(fontSize: 16.0),
              ),
            ],
          ),
        ),
      ],
    );
  }

  ///
  /// PERFORM client location details
  ///
  _performChangeLanguageUpdate() {
    /// DIALOG loader
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );

    if (_formKey.currentState.validate()) {
      _client.deviceId = widget.client.deviceId;
      _clientLanguage.deviceId = widget.client.deviceId;
      _clientLanguage.mobileNo = widget.client.phone;
      _formKey.currentState.save();

      pr.style(
        message: 'Updating language preference...',
        borderRadius: 5.0,
        backgroundColor: Colors.white,
        progressWidget: LinearProgressIndicator(
          backgroundColor: CustomColors.green,
          valueColor: AlwaysStoppedAnimation(CustomColors.yellow),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.fastLinearToSlowEaseIn,
        progressTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 13.0,
          fontWeight: FontWeight.w400,
        ),
        messageTextStyle: style.copyWith(color: Colors.grey.shade700),
      );
      pr.show();

      _setLangCode(language: _clientLanguage.langUsed);

      Future.delayed((Duration(seconds: 1))).then((value) {
        /// Update client location details on API
        clientDbRepository.updateLanguageUsed(_clientLanguage).then(
          (jsonData) {
            if (jsonData.isNotEmpty) {
              var responseJson = json.decode(jsonData)["data"];
              var responseStatus = json.decode(jsonData)["status"];

              /// Update local client location details
              Client client = Client();
              if (responseStatus == 'success') {
                Future.delayed((Duration(seconds: 1))).then((value) {
                  clientDbRepository
                      .getClient(widget.client.id)
                      .then((currClient) {
                    client = currClient;
                    client.langUsed = _clientLanguage.langUsed;

                    clientDbRepository.update(client).then((updatedClient) {
                      if (updatedClient != null) {
                        pr.update(
                          message: 'Almost done...',
                          progressWidget: LinearProgressIndicator(
                            backgroundColor: CustomColors.green,
                            valueColor:
                                AlwaysStoppedAnimation(CustomColors.yellow),
                          ),
                        );

                        Future.delayed(Duration(seconds: 1)).then(
                          (value) {
                            // send to verification token
                            pr.hide().whenComplete(() {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => Dashboard(
                                    title: Config.homeLabel,
                                    client: client,
                                  ),
                                ),
                              );

                              _displayNotification(message: responseJson);
                            });
                          },
                        );
                      } else {
                        Alert().showErrorAlert(
                          context: context,
                          message: 'Language Update Failed',
                        );
                      }
                    });
                  });
                });
              } else {
                _clientUpdateFailure(client: client);
              }
            }
          },
        );
      });
    }
  }

  /// CHANGE default language by client
  ///
  /// return ExpansionTile
  ExpansionTile _clientLanguageExpansionTile() {
    return ExpansionTile(
      title: Text(
        this.language.toUpperCase(),
        //style: style.copyWith(fontWeight: FontWeight.w500),
      ),
      children: <Widget>[
        Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 36.0),
              child: _renderLanguageRadios(),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 16.0),
              child: _updateLanguageButton(),
            ),
          ],
        ),
      ],
    );
  }

  /// RENDER location update button
  ///
  /// @return Material updateLocationButton
  _updateLanguageButton() {
    return Material(
      elevation: 16.0,
      borderRadius: BorderRadius.circular(50.0),
      color: CustomColors.green,
      child: MaterialButton(
        padding: const EdgeInsets.all(16.0),
        onPressed: () => _performChangeLanguageUpdate(),
        child: Text(
          "update".toUpperCase(),
          textAlign: TextAlign.center,
          style: style.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  /// DISPLAY notification for the update
  ///
  /// @param String message
  /// @return BotToast _displayNotification
  _displayNotification({@required String message}) {
    return BotToast.showCustomText(
      duration: Duration(seconds: 15),
      animationDuration: Duration(milliseconds: 700),
      animationReverseDuration: Duration(milliseconds: 700),
      onlyOne: true,
      clickClose: true,
      crossPage: true,
      ignoreContentClick: true,
      backgroundColor: Colors.transparent,
      toastBuilder: (_) => Align(
        alignment: Alignment(-1.0, -0.92),
        child: Card(
          color: Colors.green.shade100,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
            side: BorderSide(color: Colors.green.shade400, width: 2.0),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Your $message',
                    style: style.copyWith(
                      color: CustomColors.grey,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          customAppBar.getAppBar(title: 'Manage Profile'),
          SliverToBoxAdapter(
            child: Form(
              key: _formKey,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 24.0),
                      child: _meterExpansionTile(),
                    ),
                    _locationExpansionTile(),
                    _clientLanguageExpansionTile(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: customAppBar.getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }
}
