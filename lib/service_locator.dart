import 'package:get_it/get_it.dart';
import 'package:tanesco/services/navigations_service.dart';
import 'package:tanesco/services/permissions_service.dart';
import 'package:tanesco/services/screensize_service.dart';
import 'package:tanesco/services/local_storage_service.dart';

GetIt sl = GetIt.I; /// Service Locator Instance

Future setupLocator() async {
  var instance = await LocalStorageService.getInstance();
  sl.registerSingleton<LocalStorageService>(instance, instanceName: "LocalStorageService");
  sl.registerSingleton<PermissionsService>(PermissionsService(), instanceName: "PermissionsService");
  sl.registerLazySingleton<NavigationService>(() => NavigationService(), instanceName: "NavigationService");
  sl.registerSingleton<ScreenSizeReducersService>(ScreenSizeReducersService(), instanceName: "ScreenSizeReducersService");
}