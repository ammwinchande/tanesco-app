import 'package:flutter/material.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/login.dart';
import 'package:tanesco/service_locator.dart';

class LanguagePage extends StatefulWidget {
  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  CustomAppBar _customAppBar = CustomAppBar();
  // int _radioValue1 = 1;

  TextStyle style = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 20.0,
  );
  int _radioValue = 1;
  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
      switch (_radioValue) {
        case 1:
          showAlertDialog(context, value);
          break;
        case 2:
          showAlertDialog(context, value);
          break;
      }
    });
  }

  _setLangCode() {
    var storageService = sl.get("LocalStorageService");
    var langCode = storageService.langCode;

    /// Has App Language already set.?
    if ([null, ""].contains(langCode))
      storageService.langCode = (_radioValue == 1) ? "english" : "swahili";
  }

  showAlertDialog(BuildContext context, int value) {
    String message, title = "INFO";
    if (value == 1) {
      message = "Language Selected is English";
    }
    if (value == 2) {
      message = "Language Selected is Swahili";
    }

    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {},
    );

    // set up the AlertDialog
    AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        okButton,
      ],
    );
  }

  Material langSaveButtonLabel() {
    return Material(
      elevation: 16.0,
      borderRadius: BorderRadius.circular(50.0),
      color: CustomColors.green,
      child: MaterialButton(
        padding: const EdgeInsets.all(16.0),
        onPressed: () {
          _setLangCode();
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => LoginPage(title: Config.loginLabel),
            ),
          );
        },
        child: Text(
          Config.langButtonLabel,
          textAlign: TextAlign.center,
          style: style.copyWith(
            fontSize: 16.0,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

  ///
  /// Render screen body
  ///
  renderBody() {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 30.0,
        ),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Text(
            Config.selectLangEn,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Raleway",
              fontWeight: FontWeight.normal,
              color: CustomColors.black,
              fontSize: 16.0,
            ),
          ),
        ),
        SizedBox(
          height: 30.0,
        ),
        Container(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Radio(
                value: 1,
                groupValue: _radioValue,
                onChanged: _handleRadioValueChange,
              ),
              Text(
                Config.labelLangEn,
                style: TextStyle(fontSize: 16.0),
              ),
            ],
          ),
        ),
        Container(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Radio(
                value: 2,
                groupValue: _radioValue,
                onChanged: _handleRadioValueChange,
              ),
              Text(
                Config.labelLangSw,
                style: TextStyle(fontSize: 16.0),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 40.0,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 40.0),
          child: langSaveButtonLabel(),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _customAppBar.getAppBar(
              title: Config.languageSelectionLabel, removeLeading: true),
          SliverPadding(
            padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 24.0),
            sliver: SliverToBoxAdapter(
              child: _customAppBar.renderLogo(),
            ),
          ),
          SliverToBoxAdapter(
            child: Column(
              children: <Widget>[
                renderBody(),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: _customAppBar.getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }
}
