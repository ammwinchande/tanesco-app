import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tanesco/models/client_dao.dart';

class DBHelper {
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'tanesco.db');
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  void _onCreate(Database db, int version) async {
    await db.transaction((txn) async {
      var batch = txn.batch();
      batch.execute(ClientDao().createTableQuery);
      await batch.commit(noResult: true);
    });
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
