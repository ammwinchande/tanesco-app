import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:strings/strings.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/models/client.dart';

class Faqs extends StatefulWidget {
  final String title;
  final Client client;

  Faqs({Key key, this.title, this.client}) : super(key: key);
  _FaqsState createState() => _FaqsState();
}

class _FaqsState extends State<Faqs> {
  List faqs = List();
  final String baseUrl = Config.baseUrl;

  CustomAppBar customAppBar = CustomAppBar();

  TextStyle style = TextStyle(
    fontFamily: 'Raleway',
    color: CustomColors.black,
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
  );

  ///
  /// Fetch faqs from API
  ///
  Future<String> getFaqs() async {
    var response = await http.get(Uri.encodeFull(baseUrl + '/api/faq'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      faqs = responseBody;
    });
    return 'Success';
  }

  @override
  void initState() {
    super.initState();
    this.getFaqs();
  }

  ListView _renderFaqs() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      physics: ScrollPhysics(),
      padding: const EdgeInsets.all(8),
      itemCount: faqs.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 16.0),
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                ExpansionTile(
                  title: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      faqs[index]['question'].toString().toUpperCase(),
                      style: style,
                    ),
                  ),
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        camelize(faqs[index]['answer']),
                        style: style.copyWith(
                          color: CustomColors.black,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          customAppBar.getAppBarWithAction(
            title: 'Frequently asked questions',
            context: context,
            action: true,
            button: IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                showSearch(
                  context: context,
                  delegate: DataSearch(),
                );
              },
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              child: Column(
                children: <Widget>[
                  faqs.isEmpty
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : _renderFaqs(),
                ],
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: customAppBar.getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }
}

class DataSearch extends SearchDelegate<String> {
  final faq = [
    "Cost of a single  rotten pole - The cost is 50,000/ pole",
    "What is the cost of meter separation"
  ];
  final recentFaq = [
    "How to connect electricity service in new house      Please visit near by TANESCO office",
    "How to buy TANESCO tender - Please visit our website for details"
  ];
  @override
  List<Widget> buildActions(BuildContext context) {
    // action for app bar
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    //leading icon on the left of app bar
    return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // show result based on selection
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    //show suggestion when user searching
    final suggestionList = query.isEmpty
        ? recentFaq
        : faq.where((p) => p.contains(query)).toList();
    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
        //leading: Icon(Icons.location_city),
        title: RichText(
          text: TextSpan(
            text: suggestionList[index].substring(0, query.length),
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            children: [
              TextSpan(
                  text: suggestionList[index].substring(query.length),
                  style: TextStyle(color: Colors.grey))
            ],
          ),
        ),
      ),
      itemCount: suggestionList.length,
    );
  }
}
