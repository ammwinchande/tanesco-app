import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:strings/strings.dart';
import 'package:tanesco/custom_classes/alert.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/login.dart';
import 'package:tanesco/login_verify.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/client_database_repository.dart';
import 'package:tanesco/models/device_details.dart';
import 'package:tanesco/models/result.dart';
import 'package:tanesco/service_locator.dart';
import 'package:tanesco/services/generic_service_form.dart';
import 'package:tanesco/services/location.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class RegistrationPage extends StatefulWidget {
  final String title;
  RegistrationPage({Key key, this.title}) : super(key: key);
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final String baseUrl = Config.baseUrl;
  CustomAppBar customAppBar = CustomAppBar(); // for appBar
  var txtFirstName = TextEditingController();
  var txtLastName = TextEditingController();
  var txtPhoneNo = TextEditingController();

  ClientDatabaseRepository _clientDbRepository = ClientDatabaseRepository();
  TextEditingController _meternoTextEditingController = TextEditingController();

  bool renderMeterNoField = false; // for drawing meter number field

  TextStyle style = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 24.0,
  );

  TextStyle _stepperTitleStyle = TextStyle(
    fontSize: 16.0,
    fontFamily: "Raleway",
    fontWeight: FontWeight.w600,
  );

  String deviceDetails;

  final _formKey = GlobalKey<FormState>();
  final _client = Client();
  int currStep = 0;

  bool isClientExistsInDb = false;
  bool isClientExistsInServer = false;
  Client clientFromRemoteServer;
  Client clientFromLocalDb;

  // Form builder
  GenericServiceForm genericServiceForm = new GenericServiceForm();

  /// Location details
  Location location = Location();

  String _selectedRegion;
  String _selectedDistrict;
  String _selectedArea;
  String _selectedStreet;

  List regions = List();
  List districts = List();
  List areas = List();
  List streets = List();

  List meterVerificationMessage = List(); // store verification message from API

  /// Return Text form field label
  ///
  /// @required String fieldName, bool isRequired
  /// @return Text
  Widget fieldLabel({String fieldName, bool isRequired = true}) {
    return Text(
      isRequired ? fieldName.toUpperCase() + ' *' : fieldName.toUpperCase(),
      textAlign: TextAlign.left,
      style: TextStyle(
        color: CustomColors.grey,
        fontSize: 16.0,
      ),
    );
  }

  /// Fetch regions from API
  ///
  /// @param
  /// @return String message
  ///
  Future<String> getRegions() async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/region'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      regions = responseBody;
    });
    return 'Success';
  }

  /// Fetch districts from API based on
  ///
  /// @param int regionId
  /// @return String message
  Future<String> getDistricts({regionId}) async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/district/?region_id=$regionId'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      districts = responseBody;
    });
    return 'Success';
  }

  /// Fetch areas from API based on district_id
  ///
  /// @param int districtId
  /// @return String message
  Future<String> getAreas({districtId}) async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/area/?district_id=$districtId'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      areas = responseBody;
    });
    return 'Success';
  }

  /// Fetch streets from API based on area_id
  ///
  /// @param int areaId
  /// @return String message
  Future<String> getStreets({areaId}) async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/street/?area_id=$areaId'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      streets = responseBody;
    });
    return 'Success';
  }

  /// Verify meter number from API
  ///
  /// @param String meterno
  /// @return String message
  Future<String> verifyMeterNumber(String meterno) async {
    var response = await http.get(
      Uri.encodeFull(
        baseUrl + '/api/client/verify-meter/?meter_number=$meterno',
      ),
      headers: {"Accept": "application/json"},
    );
    var responseBody = json.decode(response.body);

    setState(() {
      meterVerificationMessage = responseBody;
    });
    return 'Success';
  }

  ///
  /// Realtime typed meter number checking
  /// Test meter: 07041236790
  ///
  meternoTextEditingListener() {
    if (_meternoTextEditingController.text.length >= 5) {
      Future.delayed((Duration(seconds: 1))).then((message) {
        this.verifyMeterNumber(_meternoTextEditingController.text);
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    _meternoTextEditingController.dispose();
  }

  @override
  void initState() {
    super.initState();
    this.getRegions(); // store regions to local variable
    _meternoTextEditingController.addListener(meternoTextEditingListener);
  }

  /// Form guide label
  ///
  /// @return Row
  Row _formGuide() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
            child: Text(
              "Fill the form below to start enjoying TANESCO Services:",
              textDirection: TextDirection.ltr,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontFamily: "Raleway",
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Row _doHaveAccount() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Have an Account?",
            textAlign: TextAlign.left,
            style: style.copyWith(fontSize: 16.0),
          ),
        ),
        SizedBox(
          width: 10.0,
        ),
        _loginLabel(),
      ],
    );
  }

  GestureDetector _loginLabel() {
    return GestureDetector(
      child: Text(
        "Login".toUpperCase(),
        style: TextStyle(
          color: CustomColors.green,
          fontSize: 20.0,
          decoration: TextDecoration.underline,
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => LoginPage(title: "Login"),
          ),
        );
      },
    );
  }

  ///
  /// Terms of service combined
  ///
  Container _termsOfService() {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _termOfServiceStartLabel,
          _termOfServiceLabel,
        ],
      ),
    );
  }

  final _termOfServiceStartLabel = Text(
    "By using this app, You agree to our ",
    textAlign: TextAlign.left,
    style: TextStyle(
      color: CustomColors.grey,
      fontSize: 12.0,
    ),
  );

  static final String termsOfServiceUrl =
      'http://www.tanesco.co.tz/index.php/terms-conditions-tanesco-app';

  /// IMPLEMENT launch terms of service URL
  static void _launchTermsOfServiceUrl() async {
    if (await canLaunch(termsOfServiceUrl)) {
      await launch(termsOfServiceUrl);
    } else {
      throw 'Could not launch $termsOfServiceUrl';
    }
  }

  final _termOfServiceLabel = GestureDetector(
    child: Text(
      "Terms of Services",
      style: TextStyle(
        color: CustomColors.green,
        fontSize: 14.0,
        decoration: TextDecoration.underline,
      ),
    ),
    onTap: () => _launchTermsOfServiceUrl(),
  );

  /// Add Column spacing
  ///
  /// @return SizedBox
  Widget _separator() {
    return SizedBox(
      height: 10.0,
    );
  }

  /// Return Stepper state
  ///
  /// @param int step
  /// @return StepState
  StepState _getState(int i) {
    if (currStep >= i)
      return StepState.complete;
    else
      return StepState.indexed;
  }

  FormField _buildRegionDropdown() {
    return FormField<String>(
      autovalidate: true,
      validator: (value) {
        if (value == null) {
          return "Select Region";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        this._client.regionId = int.parse(value);
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new InputDecorator(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'Region *',
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: new Text("Select Region"),
                  value: _selectedRegion,
                  onChanged: (selectedRegion) {
                    state.didChange(selectedRegion);
                    setState(() {
                      _selectedRegion = selectedRegion;
                      _selectedDistrict = null;
                      this.getDistricts(regionId: _selectedRegion);
                    });
                  },
                  items: regions.map((region) {
                    return DropdownMenuItem(
                      child: Text(region['name'].toString().toUpperCase()),
                      value: region['id'].toString(),
                    );
                  }).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
      },
    );
  }

  FormField _buildDistrictDropdown() {
    return FormField<String>(
      autovalidate: true,
      validator: (value) {
        if (value == null) {
          return "Select District";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        this._client.districtId = int.parse(value);
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new InputDecorator(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'District *',
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: new Text("Select District"),
                  value: _selectedDistrict,
                  onChanged: (selectedDistrict) {
                    state.didChange(selectedDistrict);
                    setState(() {
                      _selectedDistrict = selectedDistrict;
                      _selectedArea = null;
                      this.getAreas(districtId: _selectedDistrict);
                    });
                  },
                  items: districts.map((district) {
                    return DropdownMenuItem(
                      child: Text(district['name'].toString().toUpperCase()),
                      value: district['id'].toString(),
                    );
                  }).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
      },
    );
  }

  FormField _buildAreaDropdown() {
    return FormField<String>(
      autovalidate: true,
      validator: (value) {
        if (value == null) {
          return "Select Area";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        this._client.areaId = int.parse(value);
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new InputDecorator(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'Area *',
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: new Text("Select Area"),
                  value: _selectedArea,
                  onChanged: (selectedArea) {
                    state.didChange(selectedArea);
                    setState(() {
                      _selectedArea = selectedArea;
                      _selectedStreet = null;
                      this.getStreets(areaId: _selectedArea);
                    });
                  },
                  items: areas.map((area) {
                    return DropdownMenuItem(
                      child: Text(area['name'].toString().toUpperCase()),
                      value: area['id'].toString(),
                    );
                  }).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
      },
    );
  }

  FormField _buildStreetDropdown() {
    return FormField<String>(
      autovalidate: true,
      validator: (value) {
        if (value == null) {
          return "Select Street";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        this._client.streetId = int.parse(value);
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new InputDecorator(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'Street *',
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: new Text("Select Street"),
                  value: _selectedStreet,
                  onChanged: (selectedStreet) {
                    state.didChange(selectedStreet);
                    setState(() {
                      _selectedStreet = selectedStreet;
                    });
                  },
                  items: streets.map((street) {
                    return DropdownMenuItem(
                      child: Text(street['name'].toString().toUpperCase()),
                      value: street['id'].toString(),
                    );
                  }).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
      },
    );
  }

  TextFormField _buildSpecialMarkFormField() {
    return TextFormField(
      autovalidate: true,
      maxLength: 50,
      decoration: InputDecoration(
        icon: Icon(Icons.location_on),
        hintText: 'Your location famous name?',
        labelText: 'Special Mark',
      ),
      onSaved: (value) {
        this._client.specialMark = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          return null;
        } else if (value.length != 0) {
          if (value.length < 3) {
            return 'Please enter special mark';
          } else {
            return null;
          }
        }
        return null;
      },
    );
  }

  ///
  /// Input decoration for form fields
  ///
  InputDecoration _textFormFieldDecoration(
      {fieldLabel, bool isRequired = true}) {
    return InputDecoration(
      hintText: "Enter Your " + camelize(fieldLabel),
      labelText:
          isRequired ? camelize(fieldLabel) + ' *' : camelize(fieldLabel),
    );
  }

  ///
  /// First name form field
  ///
  TextFormField _firstNameField() {
    return TextFormField(
      autofocus: true,
      autovalidate: true,
      obscureText: false,
      style: style.copyWith(fontSize: 16.0),
      controller: txtFirstName,
      keyboardType: TextInputType.text,
      maxLength: 32,
      decoration: _textFormFieldDecoration(fieldLabel: 'first name'),
      validator: (String value) {
        if (value.trim().isEmpty) {
          return 'First Name is required';
        }
        String patttern = r'(^[a-zA-Z ]*$)';
        RegExp regExp = new RegExp(patttern);
        if (value.length == 0) {
          return "First Name is Required";
        } else if (!regExp.hasMatch(value)) {
          return "First Name must be a-z and A-Z";
        }
        return null;
      },
      onSaved: (String value) {
        this._client.firstName = value;
      },
    );
  }

  ///
  /// Last name form field
  ///
  TextFormField _lastNameField() {
    return TextFormField(
      autovalidate: true,
      obscureText: false,
      style: style.copyWith(fontSize: 16.0),
      maxLength: 32,
      controller: txtLastName,
      keyboardType: TextInputType.text,
      decoration: _textFormFieldDecoration(fieldLabel: 'last name'),
      validator: (String value) {
        if (value.trim().isEmpty) {
          return 'Last Name is required';
        }
        String patttern = r'(^[a-zA-Z ]*$)';
        RegExp regExp = new RegExp(patttern);
        if (value.length == 0) {
          return "Last Name is Required";
        } else if (!regExp.hasMatch(value)) {
          return "Last Name must be a-z and A-Z";
        }
        return null;
      },
      onSaved: (String value) {
        this._client.lastName = value;
      },
    );
  }

  ///
  /// Phone form field
  ///
  TextFormField _phoneField() {
    return TextFormField(
      autovalidate: true,
      obscureText: false,
      style: style.copyWith(fontSize: 16.0),
      maxLength: 12,
      controller: txtPhoneNo,
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        hintText: "(255 or 0)744536457",
        labelText: "Enter Your Phone Number *",
      ),
      validator: (String value) {
        String patttern = r'(^[0-9]*$)';
        String limitPattern = r'^(\d{10}|\d{12})$';
        RegExp regExp = new RegExp(patttern);
        RegExp regExpLimit = new RegExp(limitPattern);
        if (value.length == 0) {
          return "Phone Number is Required";
        } else if (!regExpLimit.hasMatch(value)) {
          return "Enter Correct Phone Number";
        } else if (!regExp.hasMatch(value)) {
          return "Phone Number must be digits";
        }
        return null;
      },
      onSaved: (String value) {
        if (value.startsWith('255') || value.startsWith('+255'))
          value = value.replaceFirst("255", "0");
        this._client.phone = value;
      },
    );
  }

  ///
  /// Meter number form field
  ///
  TextFormField _buildMeterNumberFormField() {
    return TextFormField(
      autovalidate: true,
      obscureText: false,
      style: style.copyWith(fontSize: 16.0),
      maxLength: 11,
      autofocus: false,
      controller: _meternoTextEditingController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        icon: Icon(FontAwesomeIcons.tachometerAlt),
        hintText: "Enter Your Meter Number",
        labelText: "Enter Your Meter Number",
      ),
      validator: (String value) {
        String patttern = r'(^[0-9]*$)';
        String limitPattern =
            r'^(\d{5}|\d{7}|\d{9}|\d{11})$'; // enbale this to have starting from 5
        RegExp regExp = new RegExp(patttern);
        RegExp regExpLimit = new RegExp(limitPattern);

        if (value != "") {
          return null;
        } else if (value.length != 0) {
          if (!regExpLimit.hasMatch(value)) {
            return "Enter Correct Meter Number";
          } else if (!regExp.hasMatch(value)) {
            return "Meter Number must be digits";
          } else if (meterVerificationMessage.isEmpty ||
              meterVerificationMessage[0]['Meter'] == null) {
            return 'Meter Number is not valid';
          }
        }
        return null;
      },
      onSaved: (String value) {
        _client.meterno = value;
      },
      onChanged: (value) {
        value.isEmpty
            ? value = '0'
            : Future.delayed(
                Duration(seconds: 1),
                () {
                  verifyMeterNumber(value);
                },
              );
      },
    );
  }

  /// Build meter number form fields
  ///
  /// @param
  /// @return Container
  Container _renderMeterNumberFormFields() {
    return Container(
      child: Column(
        children: <Widget>[
          _buildMeterNumberFormField(),
          // REALTIME meter number validation when meet criteria e.g. 07041236790
          _meternoTextEditingController.text.length < 5
              ? Text('')
              : meterVerificationMessage == null ||
                      meterVerificationMessage.isEmpty
                  ? Text(
                      '- Not Found',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 16.0,
                        color: Colors.red.shade500,
                        fontStyle: FontStyle.italic,
                      ),
                    )
                  : Text(
                      '- ' +
                          camelize(
                              meterVerificationMessage[0]['Meter'].toString()),
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 16.0,
                        color: CustomColors.green,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
        ],
      ),
    );
  }

  /// Build form steps
  ///
  /// @param
  /// @return List<Step> steps
  List<Step> _formSteps() {
    List<Step> steps = [
      Step(
        title: Text(
          "Personal Details",
          style: _stepperTitleStyle,
        ),
        isActive: true,
        state: _getState(1),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _firstNameField(),
            _lastNameField(),
            _phoneField(),
            // meternoField(),
            _renderMeterNumberFormFields(),
          ],
        ),
      ),
      Step(
        title: Text(
          "Location Details",
          style: _stepperTitleStyle,
        ),
        isActive: true,
        state: _getState(2),
        content: Column(
          children: <Widget>[
            _buildRegionDropdown(),
            _buildDistrictDropdown(),
            _buildAreaDropdown(),
            _buildStreetDropdown(),
            _buildSpecialMarkFormField()
          ],
        ),
      ),
    ];
    return steps;
  }

  ///
  /// Render form
  ///
  MediaQuery renderForm() {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      removeBottom: true,
      child: Stepper(
        physics: ClampingScrollPhysics(), // enable scrolling for stepper
        steps: _formSteps(),
        type: StepperType.vertical,
        currentStep: this.currStep,
        onStepContinue: () {
          setState(
            () {
              if (currStep < _formSteps().length - 1) {
                currStep = currStep + 1;
              } else {
                currStep = 0;
              }
            },
          );
        },
        onStepCancel: () {
          setState(
            () {
              if (currStep > 0) {
                currStep = currStep - 1;
              } else {
                currStep = 0;
              }
            },
          );
        },
        onStepTapped: (step) {
          setState(
            () {
              currStep = step;
            },
          );
        },
        controlsBuilder: (BuildContext context,
            {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
          if (currStep == _formSteps().length - 1) {
            return Row(
              children: <Widget>[
                // submit button
                FlatButton(
                  color: CustomColors.green,
                  child: Text(
                    'PREVIOUS',
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: onStepCancel,
                ),
                // cancel button
              ],
            );
          } else {
            return Row(
              children: <Widget>[
                FlatButton(
                  color: CustomColors.green,
                  child: Text(
                    'NEXT',
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: onStepContinue,
                ),
              ],
            );
          }
        },
      ),
    );
  }

  var storageService = sl.get("LocalStorageService");

  ///
  /// Store/Update LocalStorageServices Catche
  ///
  _updateLocalStorageServices({
    bool fromRemote = false,
    @required String mobNo,
    String firstN,
    String lastN,
    String language,
  }) {
    var storageService = sl.get("LocalStorageService");
    bool hasLoggedIn = storageService.hasLoggedIn;
    String mobileNo = storageService.mobileNo;

    /// Catch Logged in User (Creating User Session on  Disk)
    bool hasSignedUp = storageService.hasSignedUp;
    String firstName = storageService.firstName;
    String lastName = storageService.lastName;

    /// Catch Logged in User (Creating User Session on  Disk).?
    if (fromRemote) {
      if ([null, false].contains(hasSignedUp))
        storageService.hasSignedUp = true;
      if ([null, ""].contains(firstName)) storageService.firstName = firstN;
      if ([null, ""].contains(lastName)) storageService.lastName = lastN;
    } else {
      if ([null, false].contains(hasLoggedIn))
        storageService.hasLoggedIn = true;
    }
    storageService.hasCompletedTour = true;
    storageService.langCode = language;

    /// Store Mobile Number on Disk
    if ([null, ""].contains(mobileNo)) {
      storageService.mobileNo = (mobNo.startsWith("255"))
          ? mobNo.replaceFirst("255", "0")
          : (mobNo.startsWith("+255"))
              ? mobNo.replaceFirst("+255", "0")
              : mobNo;
    }
  }

  ///
  /// RESETTING App Data
  ///
  _resetPreferences() {
    storageService.resetAllPrefs();
  }

  /// Perform registration process on pressing register button
  ///
  /// @param
  /// @return
  void _performRegistration() {
    /// DIALOG loader
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _clientDbRepository.getClients().then(
        (clients) {
          if (clients.isEmpty) {
            /// no client in server
            if (isClientExistsInServer) {
              _clientDbRepository.insert(clientFromRemoteServer).then(
                (clientFromDb) {
                  if (clientFromDb != null) {
                    /// Invoking LocalStorageServices Func
                    this._resetPreferences();
                    _updateLocalStorageServices(
                      mobNo: clientFromDb.phone,
                      firstN: clientFromDb.firstName,
                      lastN: clientFromDb.lastName,
                      fromRemote: false,
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginVerify(
                          title: "VerifyToken",
                          client: clientFromDb,
                        ),
                      ),
                    );
                  } else {
                    Alert().showErrorAlert(
                        context: context, message: 'Registration Process Fail');
                  }
                },
              );
            } else {
              //do post to server
              _clientDbRepository.postData(_client).then(
                (jsonData) {
                  pr.style(
                    message: 'Registering New Client...',
                    borderRadius: 5.0,
                    backgroundColor: Colors.white,
                    progressWidget: CircularProgressIndicator(),
                    elevation: 10.0,
                    insetAnimCurve: Curves.fastLinearToSlowEaseIn,
                    progressTextStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 13.0,
                      fontWeight: FontWeight.w400,
                    ),
                    messageTextStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 19.0,
                      fontWeight: FontWeight.w600,
                    ),
                  );
                  pr.show();
                  if (jsonData.isNotEmpty) {
                    var responseJson = json.decode(jsonData)["data"];
                    Client clientFromRemote = Client.fromJson(
                      responseJson,
                    );
                    _clientDbRepository.insert(clientFromRemote).then(
                      (clientFromDb) {
                        if (clientFromDb != null) {
                          this._resetPreferences();

                          /// Invoking LocalStorageServices Func
                          _updateLocalStorageServices(
                            mobNo: clientFromDb.phone,
                            firstN: clientFromDb.firstName,
                            lastN: clientFromDb.lastName,
                            fromRemote: true,
                          );
                          // _updateLocalStorageServices(clientFromDb.phone,
                          //     clientFromDb.firstName, clientFromDb.lastName);
                          Future.delayed(Duration(seconds: 1)).then(
                            (value) {
                              pr.update(
                                message: "Almost done...",
                                progressWidget: LinearProgressIndicator(
                                  backgroundColor: CustomColors.green,
                                  valueColor: AlwaysStoppedAnimation(
                                      CustomColors.yellow),
                                ),
                              );

                              Future.delayed(Duration(seconds: 1)).then(
                                (value) {
                                  pr.update(
                                    message:
                                        'Verification token has been sent to your phone',
                                    progressWidget: Icon(
                                      Icons.check,
                                      color: CustomColors.green,
                                      size: 35.0,
                                    ),
                                  );

                                  Future.delayed(Duration(seconds: 1)).then(
                                    (value) {
                                      // send to verification token
                                      pr.hide().whenComplete(
                                        () {
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) => LoginVerify(
                                                title: "Verify Token",
                                                client: clientFromDb,
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                  );
                                },
                              );
                            },
                          );
                        } else {
                          Alert().showErrorAlert(
                            context: context,
                            message: 'Registration Process Fail',
                          );
                        }
                      },
                    );
                  }
                },
              );
            }
          } else {
            if (_client.phone == clientFromLocalDb.phone) {
              pr.style(
                message: 'Client already registered...',
                borderRadius: 5.0,
                backgroundColor: Colors.white,
                progressWidget: CircularProgressIndicator(),
                elevation: 10.0,
                insetAnimCurve: Curves.fastLinearToSlowEaseIn,
                progressTextStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 13.0,
                  fontWeight: FontWeight.w400,
                ),
                messageTextStyle: TextStyle(
                  fontFamily: 'Raleway',
                  color: Colors.black,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w400,
                ),
              );
              pr.show();
              _clientDbRepository
                  .getClientByColumn("mobile_no", _client.phone)
                  .then(
                (clientFromDb) {
                  if (clientFromDb != null) {
                    _clientDbRepository.getVerificationToken(clientFromDb).then(
                      (verificationToken) {
                        Result result =
                            Result.fromJson(json.decode(verificationToken));
                        Future.delayed(Duration(seconds: 1)).then(
                          (value) {
                            pr.update(
                              message: "Retrieving logging in  info...",
                              progressWidget: LinearProgressIndicator(
                                backgroundColor: CustomColors.green,
                                valueColor:
                                    AlwaysStoppedAnimation(CustomColors.yellow),
                              ),
                            );

                            Future.delayed(Duration(seconds: 1)).then(
                              (value) {
                                pr.update(
                                  message: result.message,
                                  progressWidget: Icon(
                                    Icons.check,
                                    color: CustomColors.green,
                                    size: 35.0,
                                  ),
                                );

                                Future.delayed(Duration(seconds: 1)).then(
                                  (value) {
                                    // send to verification token
                                    pr.hide().whenComplete(() {
                                      this._resetPreferences();

                                      /// Invoking LocalStorageServices Func
                                      _updateLocalStorageServices(
                                        mobNo: clientFromDb.phone,
                                        firstN: clientFromDb.firstName,
                                        lastN: clientFromDb.lastName,
                                        fromRemote: true,
                                      );
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) => LoginVerify(
                                            title: "VerifyToken",
                                            client: clientFromDb,
                                          ),
                                        ),
                                      );
                                    });
                                  },
                                );
                              },
                            );
                          },
                        );
                      },
                    );
                  }
                },
              );
            } else {
              _clientDbRepository.postData(_client).then(
                (jsonData) {
                  pr.style(
                    message: 'Registering...',
                    borderRadius: 5.0,
                    backgroundColor: Colors.white,
                    progressWidget: CircularProgressIndicator(),
                    elevation: 10.0,
                    insetAnimCurve: Curves.fastLinearToSlowEaseIn,
                    progressTextStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 13.0,
                      fontWeight: FontWeight.w400,
                    ),
                    messageTextStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 19.0,
                      fontWeight: FontWeight.w600,
                    ),
                  );
                  pr.show();
                  if (jsonData.isNotEmpty) {
                    var responseJson = json.decode(jsonData)["data"];
                    Client clientFromRemote = Client.fromJson(
                      responseJson,
                    );
                    //do insert
                    _clientDbRepository.insert(clientFromRemote).then(
                      (clientFromDb) {
                        if (clientFromDb != null) {
                          Future.delayed(Duration(seconds: 1)).then(
                            (value) {
                              pr.update(
                                message: "Almost done...",
                                progressWidget: LinearProgressIndicator(
                                  backgroundColor: CustomColors.green,
                                  valueColor: AlwaysStoppedAnimation(
                                      CustomColors.yellow),
                                ),
                              );

                              _updateLocalStorageServices(
                                mobNo: clientFromDb.phone,
                                firstN: clientFromDb.firstName,
                                lastN: clientFromDb.lastName,
                                fromRemote: true,
                              );

                              Future.delayed(Duration(seconds: 1)).then(
                                (value) {
                                  pr.update(
                                    message:
                                        'Verification token has been sent to your phone',
                                    progressWidget: Icon(
                                      Icons.check,
                                      color: CustomColors.green,
                                      size: 35.0,
                                    ),
                                  );

                                  Future.delayed(Duration(seconds: 1)).then(
                                    (value) {
                                      // send to verification token
                                      pr.hide().whenComplete(
                                        () {
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) => LoginVerify(
                                                title: "Verify Token",
                                                client: clientFromDb,
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                  );
                                },
                              );
                            },
                          );
                        } else {
                          Alert().showErrorAlert(
                            context: context,
                            message: 'Registration Process Fail',
                          );
                        }
                      },
                    );
                  }
                },
              );
            }
          }
        },
      );
    }
  }

  ///
  /// Create registration button
  ///
  Material _registerButton() {
    return Material(
      elevation: 16.0,
      borderRadius: BorderRadius.circular(50.0),
      color: CustomColors.green,
      child: MaterialButton(
        padding: const EdgeInsets.all(16.0),
        onPressed: () => _performRegistration(),
        child: Text(
          "register".toUpperCase(),
          textAlign: TextAlign.center,
          style: style.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: 16.0,
          ),
        ),
      ),
    );
  }

  /// Using deviceId check if client exists
  /// Store client to localDB
  ///
  /// @param deviceId
  /// @return
  void _getDeviceInfo() {
    Future.delayed(Duration(seconds: 1)).then((value) {
      DeviceDetails.getDeviceDetails().then(
        (val) {
          deviceDetails = val.replaceAll(new RegExp(r"\s\b|\b\s"), "");
          _client.deviceId = deviceDetails;
          _clientDbRepository
              .getData(
            _client.urlGet,
            columnName: "device_id",
            columnValue: _client.deviceId,
          )
              .then(
            (jsonResp) {
              if (jsonResp.isNotEmpty) {
                clientFromRemoteServer = Client.fromJson(json.decode(jsonResp));
                isClientExistsInServer = true;
              }
            },
          );
          _clientDbRepository
              .getClientByColumn("device_id", deviceDetails)
              .then(
            (clientFromDb) {
              if (clientFromDb != null) {
                isClientExistsInDb = true;
                clientFromLocalDb = clientFromDb;
              }
            },
          );
        },
      );
    });
  }

  ///
  /// Render body
  ///
  Form _renderForm() {
    return Form(
      key: _formKey,
      child: Container(
        child: Column(
          children: <Widget>[
            _formGuide(),
            renderForm(),
            _termsOfService(),
            _separator(),
            _registerButton(),
            _separator(),
            _doHaveAccount(),
            _separator(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    this._getDeviceInfo(); // get client based on device details
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          customAppBar.getAppBar(title: 'Register'),
          SliverPadding(
            padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 24.0),
            sliver: SliverToBoxAdapter(
              child: customAppBar.renderLogo(),
            ),
          ),
          SliverToBoxAdapter(
            child: _renderForm(),
          ),
        ],
      ),
      bottomNavigationBar: customAppBar.getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }
}
