import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:tanesco/custom_classes/alert.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/models/client_database_repository.dart';
import 'package:tanesco/dashboard.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/login_model.dart';
import 'package:tanesco/models/result.dart';
import 'package:tanesco/models/token_model.dart';
import 'package:tanesco/service_locator.dart';

class LoginVerify extends StatefulWidget {
  final String title;
  final Client client;
  final Map map;
  LoginVerify({this.title, this.client, this.map, Key key}) : super(key: key);
  @override
  _LoginVerifyState createState() => _LoginVerifyState();
}

class _LoginVerifyState extends State<LoginVerify> {
  final _formKey = GlobalKey<FormState>();
  final _tokenModel = TokenModel();
  final _loginModel = LoginModel();

  CustomAppBar customAppBar = CustomAppBar();

  TextStyle style = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 16.0,
  );

  ///
  /// Store/Update LocalStorageServices Catche
  ///
  _updateLocalStorageServices(String _mobileNo, String _fName, String _lName) {
    var storageService = sl.get("LocalStorageService");
    bool hasLoggedIn = storageService.hasLoggedIn;
    String mobileNo = storageService.mobileNo;

    /// Catch Logged in User (Creating User Session on  Disk).?
    if ([null, false].contains(hasLoggedIn)) storageService.hasLoggedIn = true;

    /// Store Mobile Number on Disk
    if ([null, ""].contains(mobileNo)) {
      storageService.mobileNo = (_mobileNo.startsWith("255"))
          ? _mobileNo.replaceFirst("255", "0")
          : (_mobileNo.startsWith("+255"))
              ? _mobileNo.replaceFirst("+255", "0")
              : _mobileNo;
    }
  }

  void _resendVerificationToken() {
    _loginModel.mobileNo = widget.client.phone;
    if (_loginModel.isMobileNo()) {
      ClientDatabaseRepository()
          .getClientByColumn(
        "mobile_no",
        _loginModel.mobileNo,
      )
          .then(
        (clientFromDb) {
          if (clientFromDb != null) {
            ClientDatabaseRepository().getVerificationToken(clientFromDb).then(
              (verificationToken) {
                Alert().showSuccessAlert(
                  context: context,
                  message: 'Verification token sent successfully',
                );
              },
            );
          } else {
            Alert().showErrorAlert(
              context: context,
              message: 'Click REGISTER to create an account',
            );
          }
        },
      );
    }
  }

  Text _tokenVerifyLabel() {
    return Text(
      Config.tokenVerifyLabel,
      textAlign: TextAlign.left,
      style: style.copyWith(color: CustomColors.grey),
    );
  }

  TextFormField _tokenFormField() {
    return TextFormField(
      obscureText: false,
      style: style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter Token",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(1.0),
          borderSide: const BorderSide(
            color: CustomColors.grey,
            width: 1.0,
          ),
        ),
      ),
      validator: (String value) {
        if (value.trim().isEmpty) {
          return 'Token is required';
        } else {
          return null;
        }
      },
      onSaved: (String value) {
        this._tokenModel.token = value.toUpperCase();
      },
    );
  }

  ///
  /// Perform token verification
  ///
  void _verifyToken() {
    /// DIALOG loader
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _tokenModel.mobileno = widget.client.phone;
      _tokenModel.deviceid = widget.client.deviceId;

      _tokenModel.login().then(
        (jsonResult) {
          Result result = Result.fromJson(json.decode(jsonResult));
          var formatterDateTime = new DateFormat('yyyy-MM-dd HH:mm:ss');
          if (result.status == "success") {
            pr.style(
              message: 'Verifying token...',
              borderRadius: 5.0,
              backgroundColor: Colors.white,
              progressWidget: CircularProgressIndicator(),
              elevation: 10.0,
              insetAnimCurve: Curves.fastLinearToSlowEaseIn,
              progressTextStyle: TextStyle(
                color: Colors.black,
                fontSize: 13.0,
                fontWeight: FontWeight.w400,
              ),
              messageTextStyle: TextStyle(
                color: Colors.black,
                fontSize: 19.0,
                fontWeight: FontWeight.w600,
              ),
            );
            pr.show();
            var now = new DateTime.now();
            Client client = widget.client;
            client.createdDate = formatterDateTime.format(now);
            client.sessionStartDate = formatterDateTime.format(now);
            var sessionEndDate = DateTime.parse(client.sessionStartDate)
                .add(new Duration(days: 35));
            client.sessionEndDate = formatterDateTime.format(sessionEndDate);

            Future.delayed(Duration(milliseconds: 500)).then((value) {
              pr.update(
                message: 'Token verified...',
                progressWidget: Icon(
                  Icons.check,
                  color: Colors.green,
                  size: 35.0,
                ),
              );

              ClientDatabaseRepository().update(client).then((updatedClient) {
                if (updatedClient != null) {
                  Future.delayed(Duration(milliseconds: 500)).then((value) {
                    pr.hide().whenComplete(
                      () {
                        /// Invoking LocalStorageServices Func
                        _updateLocalStorageServices(updatedClient.phone,
                            updatedClient.firstName, updatedClient.lastName);
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => Dashboard(
                              title: "TANESCO",
                              client: widget.client,
                            ),
                          ),
                        );
                      },
                    );
                  });
                } else {
                  Alert().showErrorAlert(
                    context: context,
                    message:
                        Config.capitalize('failed to start session, try again'),
                  );
                }
              });
            });
          } else {
            Alert().showErrorAlert(
              context: context,
              message: Config.capitalize(result.message),
            );
          }
        },
      );
    }
  }

  Material _tokenVerifyButton() {
    return Material(
      elevation: 16.0,
      borderRadius: BorderRadius.circular(50.0),
      color: CustomColors.green,
      child: MaterialButton(
        padding: const EdgeInsets.all(16.0),
        onPressed: () => _verifyToken(),
        child: Text(
          "VERIFY TOKEN",
          textAlign: TextAlign.center,
          style: style.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

  dontHaveAccLabel() {
    return Text(
      "Having token problem?",
      textAlign: TextAlign.left,
      style: style.copyWith(color: CustomColors.grey),
    );
  }

  GestureDetector _resendLabel() {
    return GestureDetector(
      child: Text(
        "Resend".toUpperCase(),
        style: style.copyWith(
          color: Colors.green.shade900,
          decoration: TextDecoration.underline,
        ),
      ),
      onTap: () {
        _resendVerificationToken();
      },
    );
  }

  Form _renderForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: _tokenVerifyLabel(),
          ),
          SizedBox(height: 15.0),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: _tokenFormField(),
          ),
          SizedBox(
            height: 35.0,
          ),
          Container(
            child: _tokenVerifyButton(),
          ),
          SizedBox(
            height: 35.0,
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              dontHaveAccLabel(),
              SizedBox(
                width: 10.0,
              ),
              _resendLabel(),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          customAppBar.getAppBar(
            title: Config.verificationTokenLabel,
            removeLeading: true,
          ),
          SliverPadding(
            padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 24.0),
            sliver: SliverToBoxAdapter(
              child: customAppBar.renderLogo(),
            ),
          ),
          SliverToBoxAdapter(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                _renderForm(),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: customAppBar.getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }
}
