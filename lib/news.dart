import 'package:flutter/material.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/models/client.dart';

class News extends StatefulWidget {
  final String title;
  final Client client;

  News({Key key, this.title, this.client}) : super(key: key);
  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {
  CustomAppBar customAppBar = CustomAppBar();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          customAppBar.getAppBar(title: widget.title),
          SliverToBoxAdapter(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[],
            ),
          )
        ],
      ),
      bottomNavigationBar: customAppBar.getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }
}
