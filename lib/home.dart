import "package:flutter/material.dart";
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/language.dart';
import 'package:tanesco/service_locator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:page_transition/page_transition.dart';

///
/// Render TextStyle For Text<Widget>
/// @return TextStyle
///
TextStyle _style = TextStyle(
  fontFamily: 'Raleway',
  color: CustomColors.green,
  fontSize: 20.0,
);

///
/// Make a list of screenShots Paths
/// @return List<String>
///
final List<String> _screenShotPaths = <String>[
  "assets/images/language-screen.png",
  "assets/images/login-screen.png",
  "assets/images/registration_screen_part_1.png",
  "assets/images/registration_screen_part_2.png",
  "assets/images/token_verification_screen.png",
  "assets/images/dashboard-screen.png",
];

///
/// Render Image List
/// @return Text<Widget>
///
final List _screenShots = map<Widget>(_screenShotPaths, (index, value) {
  String heading = index == 0
      ? "LANGUAGE SCREEN"
      : index == 1 ? "LOGIN SCREEN" : "DASHBOARD SCREEN";
  return Container(
    margin: EdgeInsets.all(5.0),
    child: ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(5.0)),
      child: Stack(children: <Widget>[
        Center(
          child: Text(
            '$heading',
            style: _style,
          ),
        ),
        Image(
            image: AssetImage(
          "$value",
        )),
      ]),
    ),
  );
}).toList();

///
/// Populate a list of CarouselSlider items
/// @return List<T>
///
List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }
  return result;
}

class HomePage extends StatefulWidget {
  final String title;
  HomePage({Key key, this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  CustomAppBar customAppBar = CustomAppBar();
  int _currentstep = 0;
  var storageService = sl.get("LocalStorageService");
  var screenSizeReducers = sl.get("ScreenSizeReducersService");

  ///
  /// Render Future Builder
  /// @return Future<Builder>
  ///
  Future<Builder> navigator() {
    bool hasCompletedTour = storageService.hasCompletedTour;

    /// Running App for the first time After Installation.?
    /// Catch Logged in User (Creating User Session).?
    if ([null, false].contains(hasCompletedTour))
      storageService.hasCompletedTour = true;

    return Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            // duration: Duration(seconds: 1),
            child: LanguagePage()));
  }

  ///
  /// Render CarouselSlider
  /// @return CarouselSlider<Widget>
  ///
  renderBody() {
    double height = screenSizeReducers.screenHeightExcludingToolbar(context,
        dividedBy: 1.4, reducedBy: 10.0);
    final CarouselSlider pageSlider = CarouselSlider(
      height: height,
      reverse: false,
      initialPage: 0,
      aspectRatio: 16 / 9,
      viewportFraction: 0.7,
      enlargeCenterPage: true,
      enableInfiniteScroll: false,
      scrollDirection: Axis.horizontal,
      onPageChanged: (index) => setState(() {
        _currentstep = index;
      }),
      items: _screenShots,
    );
    return Column(
      children: <Widget>[
        pageSlider,
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: map<Widget>(
            _screenShotPaths,
            (index, url) {
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _currentstep == index
                        ? Color.fromRGBO(0, 0, 0, 0.9)
                        : Color.fromRGBO(0, 0, 0, 0.4)),
              );
            },
          ),
        ),
        Row(children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: (_currentstep == 0)
                  ? SizedBox()
                  : FlatButton(
                      textColor: Colors.black,
                      onPressed: () => pageSlider.previousPage(
                          duration: Duration(milliseconds: 300),
                          curve: Curves.linear),
                      child: Text(
                        'BACK',
                        style: _style,
                      ),
                    ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: (_currentstep == 5)
                  ? SizedBox()
                  : FlatButton(
                      textColor: Colors.black,
                      onPressed: () => pageSlider.nextPage(
                          duration: Duration(milliseconds: 300),
                          curve: Curves.linear),
                      child: Text(
                        'NEXT',
                        style: _style,
                      ),
                    ),
            ),
          ),
        ]),
      ],
    );
  }

  ///
  /// Render Home Page
  ///
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          customAppBar.getAppBar(
              title: Config.userGuideLabel, removeLeading: true),
          SliverToBoxAdapter(
            child: renderBody(),
          ),
        ],
      ),
      floatingActionButton: _currentstep == 5
          ? FloatingActionButton.extended(
              elevation: 16.0,
              label: Text('FINISH'),
              icon: Icon(Icons.thumb_up),
              onPressed: () => navigator(),
              backgroundColor: Colors.green.shade800,
            )
          : FloatingActionButton.extended(
              elevation: 16.0,
              label: Text('SKIP'),
              icon: Icon(Icons.skip_next),
              onPressed: () => navigator(),
              backgroundColor: Colors.amber.shade800,
            ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      bottomNavigationBar: customAppBar.getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }
}
