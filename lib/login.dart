import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rich_alert/rich_alert.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:tanesco/dashboard.dart';
import 'package:tanesco/login_verify.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/client_database_repository.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/models/device_details.dart';
import 'package:tanesco/models/login_model.dart';
import 'package:tanesco/service_locator.dart';
import 'package:tanesco/register.dart';

class LoginPage extends StatefulWidget {
  final String title;
  LoginPage({Key key, this.title}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _loginModel = LoginModel();

  String _deviceId;

  CustomAppBar customAppBar = CustomAppBar();
  ClientDatabaseRepository _clientDbRepository = ClientDatabaseRepository();

  var storageService = sl.get("LocalStorageService");

  TextStyle style = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 16.0,
    color: CustomColors.grey,
  );

  final mobileLabel = Text(
    Config.mobileLabel.toUpperCase(),
    textAlign: TextAlign.left,
    style: TextStyle(
      color: CustomColors.grey,
      fontSize: 16.0,
    ),
  );

  final dontHaveAccLabel = Text(
    "Don’t have an account?",
    textAlign: TextAlign.left,
    style: TextStyle(
      color: CustomColors.grey,
      fontSize: 16.0,
    ),
  );

  /// Using deviceId check if client exists
  /// Store client to localDB
  ///
  /// @param deviceId
  /// @return
  void _getDeviceInfo() {
    Future.delayed(Duration(seconds: 1)).then((value) {
      DeviceDetails.getDeviceDetails().then(
        (val) {
          _deviceId = val.replaceAll(new RegExp(r"\s\b|\b\s"), "");
        },
      );
    });
  }

  @override
  void initState() {
    super.initState();
    this._getDeviceInfo();
  }

  GestureDetector registerLabel() {
    return GestureDetector(
      child: Text(
        "Register".toUpperCase(),
        style: TextStyle(
          color: CustomColors.green,
          fontSize: 20.0,
          decoration: TextDecoration.underline,
        ),
      ),
      onTap: () {
        //Implement Launch Registration Screen functionality
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => RegistrationPage(),
          ),
        );
      },
    );
  }

  TextFormField phoneNumberField() {
    return TextFormField(
      obscureText: false,
      style: style,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "(255 or 0)754452105",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0.0),
          borderSide: const BorderSide(
            color: CustomColors.grey,
            width: 1.0,
          ),
        ),
      ),
      validator: (value) {
        if (value.trim().isEmpty) {
          return 'Mobile Number is required';
        } else {
          return null;
        }
      },
      onSaved: (value) {
        this._loginModel.mobileNo = value;
      },
    );
  }

  ///
  /// Store/Update LocalStorageServices Catche
  ///
  _updateLocalStorageServices({
    bool fromRemote = false,
    @required String mobNo,
    String firstN,
    String lastN,
    String language,
  }) {
    var storageService = sl.get("LocalStorageService");
    bool hasLoggedIn = storageService.hasLoggedIn;
    String mobileNo = storageService.mobileNo;

    /// Catch Logged in User (Creating User Session on  Disk)
    bool hasSignedUp = storageService.hasSignedUp;
    String firstName = storageService.firstName;
    String lastName = storageService.lastName;

    /// Catch Logged in User (Creating User Session on  Disk).?
    if (fromRemote) {
      if ([null, false].contains(hasSignedUp))
        storageService.hasSignedUp = true;
      if ([null, ""].contains(firstName)) storageService.firstName = firstN;
      if ([null, ""].contains(lastName)) storageService.lastName = lastN;
    } else {
      if ([null, false].contains(hasLoggedIn))
        storageService.hasLoggedIn = true;
    }
    storageService.hasCompletedTour = true;
    storageService.langCode = language;

    /// Store Mobile Number on Disk
    if ([null, ""].contains(mobileNo)) {
      storageService.mobileNo = (mobNo.startsWith("255"))
          ? mobNo.replaceFirst("255", "0")
          : (mobNo.startsWith("+255"))
              ? mobNo.replaceFirst("+255", "0")
              : mobNo;
    }
  }

  ///
  /// RESETTING App Data
  ///
  _resetPreferences() {
    storageService.resetAllPrefs();
  }

  /// RENDER create account dialog
  ///
  /// @return RichAlertDialog
  _createAccountDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 3), () {
          Navigator.of(context).pop(true);
        });
        return RichAlertDialog(
          alertTitle: richTitle('Fail'.toUpperCase()),
          alertSubtitle: richSubtitle('Click REGISTER to create an account'),
          alertType: RichAlertType.ERROR,
          actions: <Widget>[
            FlatButton(
              color: Colors.red,
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Raleway',
                  fontSize: 16.0,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  /// PERFORM login using mobile number
  ///
  /// @param
  /// @return
  _performLogin() {
    // DIALOG loder
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );

    if (_formKey.currentState.validate()) {
      _loginModel.deviceId = this._deviceId;
      _formKey.currentState.save();
      _clientDbRepository
          .getClientByColumn(
        "mobile_no",
        _loginModel.mobileNo,
      )
          .then(
        (clientFromDb) {
          if (clientFromDb != null) {
            pr.style(
              message: 'Please wait...',
              borderRadius: 5.0,
              backgroundColor: Colors.white,
              progressWidget: CircularProgressIndicator(),
              elevation: 10.0,
              insetAnimCurve: Curves.fastLinearToSlowEaseIn,
              progressTextStyle: style.copyWith(
                fontWeight: FontWeight.w400,
                fontSize: 13.0,
              ),
              messageTextStyle: style.copyWith(fontWeight: FontWeight.w400),
            );
            pr.show();
            Future.delayed(Duration(seconds: 1)).then(
              (value) {
                pr.update(
                  message: "Logging in...",
                  progressWidget: LinearProgressIndicator(
                    backgroundColor: CustomColors.green,
                    valueColor: AlwaysStoppedAnimation(CustomColors.yellow),
                  ),
                );

                Future.delayed(Duration(seconds: 1)).then(
                  (value) {
                    pr.update(
                      message: "Successfully Logged in...",
                      progressWidget: Icon(
                        Icons.check,
                        color: CustomColors.green,
                        size: 35.0,
                      ),
                    );

                    Future.delayed(Duration(seconds: 1)).then(
                      (value) {
                        // send to Dashboard
                        pr.hide().whenComplete(
                          () {
                            _resetPreferences();

                            /// Invoking LocalStorageServices Func
                            _updateLocalStorageServices(
                              mobNo: clientFromDb.phone,
                              firstN: clientFromDb.firstName,
                              lastN: clientFromDb.lastName,
                              language: clientFromDb.langUsed,
                            );
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => Dashboard(
                                  title: Config.homeLabel,
                                  client: clientFromDb,
                                ),
                              ),
                            );
                          },
                        );
                      },
                    );
                  },
                );
              },
            );
          } else {
            // check from API if account exist
            _clientDbRepository.login(_loginModel).then(
              (jsonData) {
                pr.style(
                  message: 'Please wait...',
                  borderRadius: 5.0,
                  backgroundColor: Colors.white,
                  progressWidget: CircularProgressIndicator(),
                  elevation: 10.0,
                  insetAnimCurve: Curves.fastLinearToSlowEaseIn,
                  progressTextStyle: style.copyWith(
                    fontWeight: FontWeight.w400,
                    fontSize: 13.0,
                  ),
                  messageTextStyle: style.copyWith(fontWeight: FontWeight.w400),
                );
                if (json.decode(jsonData)["data"] != null) {
                  pr.show();
                  var responseJson = json.decode(jsonData)["data"];
                  Client clientFromRemote = Client.fromJson(
                    responseJson,
                  );
                  // wait till passed verification token then insert
                  _clientDbRepository.insert(clientFromRemote).then(
                    (clientFromDb) {
                      if (clientFromDb != null) {
                        /// Invoking LocalStorageServices Func
                        _updateLocalStorageServices(
                          fromRemote: true,
                          mobNo: clientFromDb.phone,
                          firstN: clientFromDb.firstName,
                          lastN: clientFromDb.lastName,
                          language: clientFromDb.langUsed,
                        );
                        Future.delayed(Duration(seconds: 1)).then(
                          (value) {
                            pr.update(
                              message: "Logging in...",
                              progressWidget: LinearProgressIndicator(
                                backgroundColor: CustomColors.green,
                                valueColor:
                                    AlwaysStoppedAnimation(CustomColors.yellow),
                              ),
                            );

                            Future.delayed(Duration(seconds: 1)).then(
                              (value) {
                                pr.update(
                                  message:
                                      'Verification token has been sent to your phone',
                                  progressWidget: Icon(
                                    Icons.check,
                                    color: CustomColors.green,
                                    size: 35.0,
                                  ),
                                );

                                Future.delayed(Duration(seconds: 1)).then(
                                  (value) {
                                    // send to verification token
                                    pr.hide().whenComplete(
                                      () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) => LoginVerify(
                                              title: "Verify Token",
                                              client: clientFromDb,
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  },
                                );
                              },
                            );
                          },
                        );
                      } else {
                        _createAccountDialog();
                      }
                    },
                  );
                } else {
                  pr.hide().whenComplete(() => _createAccountDialog());
                  // _createAccountDialog();
                }
              },
            );
          }
        },
      );
    }
  }

  ///
  /// Create login button
  ///
  Material loginButon() {
    return Material(
      elevation: 16.0,
      borderRadius: BorderRadius.circular(50.0),
      color: CustomColors.green,
      child: MaterialButton(
        padding: const EdgeInsets.all(8.0),
        onPressed: () => _performLogin(),
        child: Text(
          Config.loginLabel.toUpperCase(),
          textAlign: TextAlign.center,
          style: style.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

  ///
  /// Render form
  ///
  renderForm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 60.0,
        ),
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: mobileLabel,
        ),
        SizedBox(height: 15.0),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: phoneNumberField(),
        ),
        SizedBox(
          height: 35.0,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: loginButon(),
        ),
        SizedBox(
          height: 35.0,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 50.0),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: dontHaveAccLabel,
              ),
              registerLabel(),
            ],
          ),
        ),
      ],
    );
  }

  DateTime currentBackPressTime;
  static const String exit_warning = 'Press back 2 times to exit application';
  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 1)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: exit_warning);
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      return Future.value(false);
    } else if (storageService.langCode != null) {
      SystemChannels.platform.invokeMapMethod('SystemNavigator.pop');
    }
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      // onWillPop: () async => false,
      onWillPop: onWillPop,
      child: Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            customAppBar.getAppBar(
                title: Config.loginLabel, removeLeading: true),
            SliverPadding(
              padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 24.0),
              sliver: SliverToBoxAdapter(
                child: customAppBar.renderLogo(),
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                child: Form(
                  key: _formKey,
                  child: renderForm(),
                ),
              ),
            )
          ],
        ),
        bottomNavigationBar: customAppBar.getBottomAppBar(
          copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
        ),
      ),
    );
  }
}
