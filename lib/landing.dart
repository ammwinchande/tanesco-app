import "package:flutter/material.dart";
import 'package:permission_handler/permission_handler.dart';
import 'package:tanesco/service_locator.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var storageService = sl("LocalStorageService");
    bool hasCompletedTour = storageService.hasCompletedTour;
    var langCode = storageService.langCode;
    bool hasLoggedIn = storageService.hasLoggedIn;
    bool hasSignedUp = storageService.hasSignedUp;

    void _getPermissions() async {
      await PermissionHandler().requestPermissions([
        PermissionGroup.storage,
        PermissionGroup.phone,
        PermissionGroup.sms,
        PermissionGroup.location,
        PermissionGroup.locationAlways,
        PermissionGroup.locationWhenInUse,
        PermissionGroup.camera,
        PermissionGroup.photos,
        PermissionGroup.notification,
      ]);
    }

    void _checkPermissions() async {
      await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);
      await PermissionHandler().checkPermissionStatus(PermissionGroup.phone);
      await PermissionHandler().checkPermissionStatus(PermissionGroup.sms);
      await PermissionHandler().checkPermissionStatus(PermissionGroup.location);
      await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.locationWhenInUse);
      await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.locationAlways);
      await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);
      await PermissionHandler().checkPermissionStatus(PermissionGroup.photos);
      await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.notification);
      _getPermissions();
    }

    _checkPermissions();

    ///
    /// Switch to Next Page
    /// @return Navigator
    ///
    _switchPage() {
      Future.delayed(Duration(seconds: 1)).then((value) {
        if (![null, false].contains(hasLoggedIn)) Navigator.pushNamed(context, '/dashboard');
        else if ([null, false].contains(hasCompletedTour)) Navigator.pushNamed(context, '/home');
        else if ([null, ""].contains(langCode)) Navigator.pushNamed(context, '/language');
        else if ([null, false].contains(hasLoggedIn)) Navigator.pushNamed(context, '/login');
        else if ([null, false].contains(hasSignedUp)) Navigator.pushNamed(context, '/register');
      });
    }

    _switchPage();

    return Scaffold(
      body: SafeArea(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
