import 'dart:convert';
import 'dart:io' show Platform;

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:page_transition/page_transition.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:strings/strings.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/faqs.dart';
import 'package:tanesco/home.dart';
import 'package:tanesco/login.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/client_database_repository.dart';
import 'package:tanesco/models/client_unregister.dart';
import 'package:tanesco/models/service.dart';
import 'package:tanesco/service_locator.dart';
import 'package:tanesco/services/generic_service_entry_point.dart';
import 'package:tanesco/services/services_summary.dart';
import 'package:tanesco/tools/profile.dart';
import 'package:tanesco/tools/setting.dart';
import 'package:url_launcher/url_launcher.dart';

class Dashboard extends StatefulWidget {
  final String title;
  final Client client;
  final String notification;
  Dashboard({this.title, this.client, this.notification, Key key})
      : super(key: key);
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  Client _authenticClient;
  final String baseUrl = Config.baseUrl;
  CustomAppBar _customAppBar = CustomAppBar();
  ClientDatabaseRepository _clientDatabaseRepository =
      ClientDatabaseRepository(); // client database repository
  var storageService = sl.get("LocalStorageService");
  final _clientUnregister = ClientUnregister();
  final _formKey = GlobalKey<FormState>();

  Future<ServiceList> serviceListFuture; // fetch all servicesList
  List _services = List();
  List _faqs = List();

  TextStyle _textStyle = TextStyle(
    color: Colors.black,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w300,
    fontSize: 16.0,
  );

  TextStyle otherServiceStyle = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w900,
    fontSize: 16.0,
  );

  TextStyle accountNameStyle = TextStyle(
    color: CustomColors.white,
  );

  TextStyle drawerItemStyle = TextStyle(
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w600,
    fontSize: 16.0,
  );

  TextStyle questionStyle = TextStyle(
    fontFamily: 'Raleway',
    fontStyle: FontStyle.normal,
    color: CustomColors.grey,
    fontSize: 16.0,
    fontWeight: FontWeight.bold,
  );

  TextStyle answerStyle = TextStyle(
    fontFamily: 'Raleway',
    color: CustomColors.black,
    fontSize: 16.0,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
  );

  TextStyle labelStyle = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 16.0,
  );

  double topTextHeight = 50;

  /// Method defined to check internet connectivity
  Future<bool> isConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  /// Method to fetch all services from API
  Future<ServiceList> getServiceListData() async {
    final response = await http.get(
      Config.baseUrl + '/api/service',
    );
    return ServiceList.fromJson(json.decode(response.body));
  }

  Future<String> getServices() async {
    var response = await http.get(Uri.encodeFull(baseUrl + '/api/service'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      _services = responseBody;
    });
    return 'Success';
  }

  ///
  /// Fetch faqs from API
  ///
  Future<String> getFaqs({int limit}) async {
    var response = await http.get(
        Uri.encodeFull(baseUrl + '/api/faq/?limit=$limit'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      _faqs = responseBody;
    });
    return 'Success';
  }

  ///
  /// Restricting Access to un-authorized Client
  ///
  _isClientAuthentic() {
    var storageService = sl.get("LocalStorageService");

    /// Has User Confirmed SignedUp/LoggedIn Verification Code.?
    bool hasLoggedIn = storageService.hasLoggedIn;
    if ([null, false].contains(hasLoggedIn))
      Navigator.push(
        context,
        PageTransition(
          type: PageTransitionType.leftToRightWithFade,
          child: LoginPage(),
        ),
      );
  }

  @override
  void initState() {
    _isClientAuthentic();
    _loggedInClient();
    isConnected().then((internet) {
      if (internet) {
        setState(() {
          serviceListFuture = getServiceListData();
        });
      } else {
        /*Display dialog with no internet connection message*/
        SnackBar(
          content: Text(
            'No internet connection',
            style: _textStyle,
          ),
        );
      }
    });
    _loggedInClient();
    this.getServices();
    this.getFaqs(limit: 5);
    super.initState();
  }

  /// Redirect to correct service based on name
  ///
  /// @param
  /// @return
  void selectedRoute({title, serviceId}) {
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => GenericServiceEntryPoint(
          title: title.toUpperCase(),
          client: widget.client ?? _authenticClient,
          serviceId: serviceId,
        ),
      ),
    );
  }

  Expanded _greeting() {
    return Expanded(
      flex: 1,
      child: Container(
        padding: EdgeInsets.fromLTRB(20, 10, 8, 0),
        height: topTextHeight,
        alignment: Alignment.centerLeft,
        color: Color(0xFFFFFF),
        child: Text(
          widget.client == null
              ? _authenticClient == null
                  ? 'Hi,'
                  : 'Hi, ' + camelize(_authenticClient.firstName) + '!'
              : 'Hi, ' + camelize(widget.client.firstName) + '!',
          style: _textStyle,
        ),
      ),
    );
  }

  /// RENDER logged in user
  ///
  /// @return Row
  Row _renderTopBanner() {
    return Row(
      children: <Widget>[
        _greeting(),
      ],
    );
  }

  /// RENDER logged in user
  ///
  /// @return Row
  Row _renderReportedIssues() {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            alignment: Alignment.centerRight,
            padding: const EdgeInsets.fromLTRB(0, 0, 8.0, 0),
            child: GestureDetector(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Card(
                    elevation: 16.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    color: CustomColors.green,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            FontAwesomeIcons.history,
                            color: CustomColors.yellow,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'reported\nissues'.toUpperCase(),
                            style: _textStyle.copyWith(
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ServicesSummary(
                      client: widget.client ?? _authenticClient,
                      title: Config.reportedServicesLabel,
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  /// Render services label
  ///
  /// @param String label
  /// @return Row
  Row _renderTitleLabel({@required String label}) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
              right: 16.0,
              bottom: 0.0,
              left: 16.0,
            ),
            child: Text(
              label.toUpperCase(),
              style: TextStyle(
                fontFamily: 'Raleway',
                fontSize: 24.0,
                color: CustomColors.green,
              ),
            ),
          ),
        )
      ],
    );
  }

  /// Render contacts Information
  ///
  /// @return Container
  Container _renderContacts() {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      height: 280,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Text(
                    "CONTACT US",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontFamily: 'Raleway',
                      color: CustomColors.green,
                    ),
                  ),
                ),
              )
            ],
          ),
          Expanded(child: Divider()),
          Container(
            height: 200.0,
            child: ContactUsPart(),
          ),
        ],
      ),
    );
  }

  ///
  /// Resetting App Data
  ///
  _resetPreferences() {
    storageService.resetAllPrefs();
  }

  ///
  /// Client Logout (Ristricting Client from Accessing App)
  ///
  _logOut() {
    storageService.hasLoggedIn = false;
  }

  ///
  /// render unregister buttons
  ///
  _renderUnregisterButtons({bool submit}) {
    return Material(
      borderRadius: BorderRadius.circular(0.0),
      color: submit ? CustomColors.green : Colors.grey.shade300,
      child: FlatButton(
        padding: const EdgeInsets.all(16.0),
        onPressed: () =>
            submit ? _removeClientDetails() : Navigator.of(context).pop(),
        child: Text(
          !submit ? 'cancel'.toUpperCase() : 'accept'.toUpperCase(),
          textAlign: TextAlign.center,
          style: labelStyle.copyWith(
            color: submit ? Colors.white : Colors.grey.shade900,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  /// Remove client details from Device and Portal
  ///
  /// @param
  /// @return
  void _removeClientDetails() {
    /// DIALOG loader
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );

    if (_formKey.currentState.validate()) {
      _clientUnregister.deviceId = widget.client == null
          ? _authenticClient.deviceId
          : widget.client.deviceId;
      _clientUnregister.mobileNo =
          widget.client == null ? _authenticClient.phone : widget.client.phone;
      _clientUnregister.connStatus = false;

      _formKey.currentState.save();

      pr.style(
        message: 'Un-registering your device...',
        borderRadius: 5.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 13.0,
          fontWeight: FontWeight.w400,
        ),
        messageTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 19.0,
          fontWeight: FontWeight.w600,
        ),
      );

      pr.show();

      Future.delayed((Duration(seconds: 2))).then(
        (value) {
          _clientDatabaseRepository.unregisterClient(_clientUnregister).then(
            (jsonData) {
              if (jsonData.isNotEmpty) {
                var responseStatus = json.decode(jsonData)['status'].toString();
                String responseMessage =
                    json.decode(jsonData)['data'].toString();
                if (responseStatus == 'success') {
                  _resetPreferences();
                  pr.update(message: 'almost done...');
                  Future.delayed((Duration(seconds: 1))).then((value) {
                    _clientDatabaseRepository
                        .delete(widget.client ?? _authenticClient)
                        .then((value) {
                      pr.update(
                        message: responseMessage,
                        progressWidget: Icon(
                          Icons.check,
                          color: CustomColors.green,
                          size: 35.0,
                        ),
                      );
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => HomePage(),
                        ),
                      );
                    });
                  });
                }
              }
            },
          );
        },
      );
    }
  }

  /// Remove client details from Device and Portal
  ///
  /// @param
  /// @return
  void _performLogout() {
    /// DIALOG loader
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );
    pr.style(
      message: 'Logging you out...',
      borderRadius: 5.0,
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut,
      progressTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 13.0,
        fontWeight: FontWeight.w400,
      ),
      messageTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 19.0,
        fontWeight: FontWeight.w600,
      ),
    );

    pr.show();

    Future.delayed((Duration(seconds: 1))).then(
      (value) {
        _logOut();
        pr.hide().whenComplete(() {
          Navigator.of(context).pop();
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (context) => LoginPage(),
            ),
          );
        });
      },
    );
  }

  /// Render alert dialog for un register device
  ///
  /// @param BuildContext context, bool isLogout
  /// @return ShowDialog
  _showAlertDialog({@required BuildContext context}) {
    String title = "Are you sure you want to UN-REGISTER?";
    String content =
        "By Un-registering device, TANESCO Application will start from fresh.";

    // set up the AlertDialog
    AlertDialog androidAlert = AlertDialog(
      title: Text(
        title,
      ),
      content: Text(
        content,
        style: labelStyle.copyWith(
          fontSize: 12.0,
          letterSpacing: 1,
        ),
      ),
      actions: [
        _renderUnregisterButtons(submit: false),
        _renderUnregisterButtons(submit: true),
      ],
    );
    CupertinoAlertDialog iOSAlert = CupertinoAlertDialog(
      title: Text(
        title,
      ),
      content: Padding(
        padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 0),
        child: Text(
          content,
          style: labelStyle.copyWith(
            letterSpacing: 1,
          ),
        ),
      ),
      actions: <Widget>[
        _renderUnregisterButtons(submit: false),
        _renderUnregisterButtons(submit: true),
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Platform.isAndroid
            ? Form(key: _formKey, child: androidAlert)
            : Platform.isIOS
                ? Form(key: _formKey, child: iOSAlert)
                : Form(key: _formKey, child: androidAlert);
      },
    );
  }

  /// Render coming soon dialog
  ///
  /// @param BuildContext context
  /// @return
  void _renderComingSoonDialog({BuildContext context}) {
    String title = Config.commingSoonLabel;

    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () => Navigator.of(context).pop(),
    );

    AlertDialog androidAlert = AlertDialog(
      title: Text(title),
      content: Text("Coming soon, still under development"),
      actions: [
        okButton,
      ],
    );

    CupertinoAlertDialog iOSAlert = CupertinoAlertDialog(
      title: Text(title),
      actions: <Widget>[
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Platform.isAndroid
            ? Form(key: _formKey, child: androidAlert)
            : Platform.isIOS
                ? Form(key: _formKey, child: iOSAlert)
                : Form(key: _formKey, child: androidAlert);
      },
    );
  }

  test() {
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Text('Test Widget'),
        )
      ],
    );
  }

  /// change logic for drawer items
  /// create a list of all static items
  /// sum the length of api _servicesLength and _otherServicesLength
  ///
  List _appMenuItems = [];

  /// ListTile Drawer
  ///
  /// @param IconData, String, bool
  /// @return ListTile
  ListTile _tileDrawer(
      {@required IconData icon, @required String title, bool route = true}) {
    return ListTile(
      leading: Icon(
        icon,
        color: CustomColors.green,
      ),
      title: Text(
        title,
        style: drawerItemStyle,
      ),
      onTap: () {
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ServicesSummary(
              title: Config.reportedServicesLabel,
              client: widget.client ?? _authenticClient,
            ),
          ),
        );
      },
    );
  }

  Container _apiMenuItems() {
    return Container(
      padding: EdgeInsets.zero,
      height: double.maxFinite,
      child: ListView.builder(
        padding: EdgeInsets.zero,
        itemCount: _services == null ? 0 : _services.length,
        itemBuilder: (BuildContext context, int index) {
          String langUsed =
              this.storageService.langCode.toString().toLowerCase();
          String serviceName;
          if (langUsed == 'english') {
            serviceName = _services[index]['name'];
          } else if (langUsed == 'swahili') {
            serviceName = _services[index]['name_sw'];
          }
          return ListTile(
            leading: Image.network(
              Config.baseUrl +
                  'uploads/services/' +
                  _services[index]['service_icon'].toString(),
              width: 30,
              height: 30,
              color: CustomColors.green,
            ),
            title: Text(
              capitalize(serviceName),
              style: drawerItemStyle,
            ),
            onTap: () => selectedRoute(
              title: serviceName,
              serviceId: _services[index]['id'],
            ),
          );
        },
      ),
    );
  }

  Container _staticMenuItems() {
    return Container(
      height: double.maxFinite,
      margin: EdgeInsets.only(top: 220),
      padding: EdgeInsets.zero,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          ListTile(
            leading: Icon(
              FontAwesomeIcons.history,
              color: CustomColors.green,
            ),
            title: Text(
              Config.reportedServicesLabel,
              style: drawerItemStyle,
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ServicesSummary(
                    title: Config.reportedServicesLabel,
                    client: widget.client ?? _authenticClient,
                  ),
                ),
              );
            },
          ),
          Divider(),
          ListTile(
            title: Text(
              Config.toolsLabel,
              style: drawerItemStyle,
            ),
          ),
          ListTile(
            leading: Icon(
              FontAwesomeIcons.cog,
              color: CustomColors.green,
            ),
            title: Text(
              Config.faqsLabel,
              style: drawerItemStyle,
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (context) => Faqs(
                    title: Config.faqsLabel,
                    client: widget.client ?? _authenticClient,
                  ),
                ),
              );
            },
          ),
          ListTile(
            leading: Icon(
              FontAwesomeIcons.cog,
              color: CustomColors.green,
            ),
            title: Text(
              Config.settingsLabel,
              style: drawerItemStyle,
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => new Setting(
                    client: widget.client ?? _authenticClient,
                    title: Config.settingsLabel.toUpperCase(),
                  ),
                ),
              );
            },
          ),
          ListTile(
            leading: Icon(
              FontAwesomeIcons.solidUserCircle,
              color: CustomColors.green,
            ),
            title: Text(
              Config.profileLabel,
              style: drawerItemStyle,
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => new Profile(
                    title: Config.profileLabel.toUpperCase(),
                    client: widget.client ?? _authenticClient,
                  ),
                ),
              );
            },
          ),
          ListTile(
            leading: Icon(
              Icons.exit_to_app,
              color: CustomColors.green,
            ),
            title: Text(
              Config.unregisterLabel,
              style: drawerItemStyle,
            ),
            onTap: () {
              _showAlertDialog(context: context);
            },
          ),
          Divider(),
          ListTile(
            title: Text(
              Config.otherLinksLabel,
              style: drawerItemStyle,
            ),
          ),
          ListTile(
            leading: Icon(
              FontAwesomeIcons.newspaper,
              color: CustomColors.green,
            ),
            title: Text(
              Config.newsLabel,
              style: drawerItemStyle,
            ),
            onTap: () => _renderComingSoonDialog(context: context),
          ),
          ListTile(
            leading: Icon(
              FontAwesomeIcons.powerOff,
              color: CustomColors.green,
            ),
            title: Text(
              Config.logoutLabel,
              style: drawerItemStyle,
            ),
            onTap: () {
              _performLogout();
            },
          ),
        ],
      ),
    );
  }

  Expanded _drawerMenus() {
    return Expanded(
      flex: 1,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _apiMenuItems(),
        ],
      ),
    );
  }

  /// RENDER userAccountDrawer
  ///
  /// @return UserAccountsDrawerHeader
  UserAccountsDrawerHeader _userAccountDrawer() {
    return UserAccountsDrawerHeader(
      accountName: Text(
        widget.client == null
            ? _authenticClient == null
                ? ''
                : camelize(_authenticClient.firstName) +
                    ' ' +
                    camelize(_authenticClient.lastName)
            : camelize(widget.client.firstName) +
                ' ' +
                camelize(widget.client.lastName),
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      accountEmail: Text(
        widget.client == null
            ? _authenticClient == null ? '' : _authenticClient.phone
            : widget.client.phone,
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      currentAccountPicture: CircleAvatar(
        backgroundColor: CustomColors.yellow,
        child: Text(
          widget.client == null
              ? _authenticClient == null
                  ? ''
                  : _authenticClient.firstName[0].toUpperCase() +
                      _authenticClient.lastName[0].toUpperCase()
              : widget.client.firstName[0].toUpperCase() +
                  widget.client.lastName[0].toUpperCase(),
        ),
      ),
      decoration: BoxDecoration(color: CustomColors.green),
    );
  }

  /// CREATE Drawer
  ///
  /// @param BuilContext context
  /// @return Drawer
  Drawer _buildDrawer({context}) {
    return Drawer(
      child: Column(
        children: <Widget>[
          _userAccountDrawer(),
          _drawerMenus(),
        ],
      ),
    );
  }

  /// SHOW progress indicator
  ///
  /// @return CircularProgressIndicator
  circularProgress() {
    return Center(child: CircularProgressIndicator());
  }

  /// Services builder from API
  ///
  /// @param
  /// @return SliverGrid
  SliverGrid _buildServicesGrid() {
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 1.2,
        mainAxisSpacing: 1.0,
        crossAxisSpacing: 1.0,
      ),
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        String langUsed = this.storageService.langCode.toString().toLowerCase();
        String serviceName;
        if (langUsed == 'english') {
          serviceName = _services[index]['name'];
        } else if (langUsed == 'swahili') {
          serviceName = _services[index]['name_sw'];
        }
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            child: Card(
              elevation: 8.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              color: Colors.grey[30],
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  alignment: Alignment.center,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Center(
                        child: Image.network(
                          Config.baseUrl +
                              'uploads/services/' +
                              _services[index]['service_icon'].toString(),
                          width: 60,
                          height: 60,
                          color: CustomColors.green,
                        ),
                      ),
                      Expanded(
                        child: Center(
                          child: Text(
                            serviceName.toUpperCase(),
                            style: TextStyle(
                              fontFamily: 'Raleway',
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            onTap: () => selectedRoute(
              title: _services[index]['name'].toString().toLowerCase(),
              serviceId: _services[index]['id'],
            ),
          ),
        );
      }, childCount: _services.length),
    );
  }

  /// FAQs builder from API
  ///
  /// @param
  /// @return SliverList
  SliverList _buildFaqsList() {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 2.0,
              horizontal: 6.0,
            ),
            child: Card(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
                padding: EdgeInsets.all(2.0),
                child: Column(
                  children: <Widget>[
                    ExpansionTile(
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            FontAwesomeIcons.questionCircle,
                            size: 16.0,
                            color: CustomColors.yellow,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            child: Text(
                              _faqs[index]['question'].toString().toUpperCase(),
                              style: TextStyle(
                                fontFamily: 'Raleway',
                                color: CustomColors.black,
                                fontSize: 16.0,
                                fontWeight: FontWeight.w500,
                              ),
                              softWrap: true,
                              overflow: TextOverflow.clip,
                            ),
                          ),
                        ],
                      ),
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(
                            camelize(_faqs[index]['answer'].toString()),
                            style: answerStyle.copyWith(letterSpacing: .5),
                            softWrap: true,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        },
        childCount: _faqs.length,
      ),
    );
  }

  ///
  /// Fetch Client Object if null
  /// @return Navigator
  ///
  _loggedInClient() async {
    var storageService = sl.get("LocalStorageService");
    String mobileNo = storageService.mobileNo;
    Future.delayed(Duration(milliseconds: 1000)).then((value) {
      ClientDatabaseRepository()
          .getClientByColumn("mobile_no", mobileNo)
          .then((_object) {
        setState(() {
          _authenticClient = _object;
        });
      });
    });
  }

  DateTime currentBackPressTime;
  static const String exit_warning = 'Press back 2 times to exit application';
  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: exit_warning);
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      return Future.value(false);
    }
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            _customAppBar.getAppBar(title: Config.tanescoLabel),
            SliverToBoxAdapter(
              child: Column(
                children: <Widget>[
                  _renderTopBanner(),
                  _renderReportedIssues(),
                  _renderTitleLabel(label: Config.servicesLabel),
                ],
              ),
            ),
            _services.isEmpty
                ? SliverToBoxAdapter(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : _buildServicesGrid(),
            SliverToBoxAdapter(
              child: Column(
                children: <Widget>[
                  _renderTitleLabel(label: Config.faqsLabel),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            ),
            _faqs.isEmpty
                ? SliverToBoxAdapter(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : _buildFaqsList(),
            SliverToBoxAdapter(
              child: _renderContacts(),
            ),
          ],
        ),
        drawer: _buildDrawer(context: context),
        bottomNavigationBar: _customAppBar.getBottomAppBar(
          copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
        ),
      ),
    );
  }
}

class ContactUsPart extends StatelessWidget {
  final String callCenterNo = '+255768985100';
  final String callCenterEmail = 'customer.service@tanesco.co.tz';
  final String facebookUrl = 'https://web.facebook.com/tanesco.yetultd';
  final String twitterUrl = 'https://twitter.com/tanescoyetu';
  final String instagramUrl = 'https://instagram.com/tanesco_official_page';
  final String officialUrl = 'https://www.tanesco.co.tz/';
  final String youtubeUrl = 'https://www.youtube.com/user/umemeforum';

  final TextStyle textStyle = TextStyle(
    fontFamily: 'Raleway',
    color: CustomColors.black,
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
  );

  /// LAUNCH specific app based on socialSpec passed
  void _launchSocial({String socialItem, String socialSpec}) async {
    if (socialSpec == 'tel') {
      if (await canLaunch('tel:$socialItem')) {
        await launch('tel:$socialItem');
      } else {
        throw 'Could not launch $socialItem';
      }
    } else if (socialSpec == 'site') {
      if (await canLaunch(socialItem)) {
        await launch(socialItem);
      } else {
        throw 'Could not launch $socialItem';
      }
    } else if (socialSpec == 'mail') {
      if (await canLaunch('mailto:$socialItem')) {
        await launch('mailto:$socialItem');
      } else {
        throw 'Could not launch $socialItem';
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final String callText = "Need Further Assistance? Call us any time 24/7";
    final String mailText = "Impressed or Disappointed? Send us your feedback";
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    //width: 20.0,
                    alignment: Alignment.centerLeft,
                    child: FlatButton(
                      child: Icon(
                        Icons.phone_in_talk,
                        size: 35.0,
                        color: CustomColors.green,
                      ),
                      onPressed: () => _launchSocial(
                          socialItem: callCenterNo, socialSpec: 'tel'),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: VerticalDivider(color: CustomColors.green),
                ),
                Expanded(
                  flex: 14,
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      callText,
                      style: textStyle,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: FlatButton(
                      child: Icon(
                        Icons.mail_outline,
                        size: 35.0,
                        color: CustomColors.green,
                      ),
                      onPressed: () => _launchSocial(
                        socialItem: callCenterEmail,
                        socialSpec: 'mail',
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: VerticalDivider(
                    color: CustomColors.green,
                  ),
                ),
                Expanded(
                  flex: 14,
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      mailText,
                      style: textStyle,
                    ),
                  ),
                ),
              ],
            ),
            Expanded(child: Divider()),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    child: FlatButton(
                      child: Icon(
                        FontAwesomeIcons.facebook,
                        size: 35.0,
                        color: CustomColors.green,
                      ),
                      onPressed: () => _launchSocial(
                        socialItem: facebookUrl,
                        socialSpec: 'site',
                      ),
                    ),
                  ),
                ),
                VerticalDivider(
                  color: CustomColors.green,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    child: FlatButton(
                      child: Icon(
                        FontAwesomeIcons.twitter,
                        size: 35.0,
                        color: CustomColors.green,
                      ),
                      onPressed: () => _launchSocial(
                        socialItem: twitterUrl,
                        socialSpec: 'site',
                      ),
                    ),
                  ),
                ),
                VerticalDivider(
                  color: CustomColors.green,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    child: FlatButton(
                      child: Icon(
                        FontAwesomeIcons.instagram,
                        size: 35.0,
                        color: CustomColors.green,
                      ),
                      onPressed: () => _launchSocial(
                        socialItem: instagramUrl,
                        socialSpec: 'site',
                      ),
                    ),
                  ),
                ),
                VerticalDivider(
                  color: CustomColors.green,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    child: FlatButton(
                      child: Icon(
                        FontAwesomeIcons.youtube,
                        size: 35.0,
                        color: CustomColors.green,
                      ),
                      onPressed: () => _launchSocial(
                        socialItem: youtubeUrl,
                        socialSpec: 'site',
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        )
      ],
    );
  }
}
