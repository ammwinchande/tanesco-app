import 'package:flutter/material.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';

class CustomAppBar {
  /// Build Sliver Appbar
  ///
  /// @param String title
  /// @return SliverAppBar
  SliverAppBar getAppBar(
      {String title, bool pinned = true, bool removeLeading = false}) {
    return SliverAppBar(
      title: Text(
        title.toUpperCase(),
        style: TextStyle(
          fontFamily: 'Raleway',
          color: CustomColors.yellow,
          fontWeight: FontWeight.w700,
          fontSize: 20.0,
        ),
      ),
      pinned: pinned,
      iconTheme: IconThemeData(color: CustomColors.yellow),
      centerTitle: true,
      leading: removeLeading ? Container() : null,
      backgroundColor: CustomColors.green,
      actions: [],
    );
  }

  /// Build Sliver Appbar
  ///
  /// @param String title
  /// @return SliverAppBar
  SliverAppBar getAppBarWithAction({
    String title,
    bool pinned = true,
    action = false,
    context,
    IconButton button,
  }) {
    return SliverAppBar(
      title: Text(
        title.toUpperCase(),
        style: TextStyle(
          fontFamily: 'Raleway',
          color: CustomColors.yellow,
          fontWeight: FontWeight.w700,
          fontSize: 16.0,
        ),
      ),
      pinned: pinned,
      iconTheme: IconThemeData(color: CustomColors.yellow),
      centerTitle: true,
      actions: <Widget>[
        button,
      ],
    );
  }

  /// Build Copyright Bottom bar
  ///
  /// @param String copyRightLabel
  /// @return BottomAppBar
  BottomAppBar getBottomAppBar(
      {String copyRightLabel = Config.copyRightLabel}) {
    return BottomAppBar(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(height: 10.0),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              copyRightLabel,
              style: TextStyle(
                fontFamily: 'Raleway',
                fontWeight: FontWeight.w700,
                fontSize: 16.0,
              ),
            ),
          ),
          SizedBox(height: 10.0),
        ],
      ),
    );
  }

  /// Render logo
  ///
  /// @param
  /// @return Container with logo
  Container renderLogo() {
    return Container(
      height: 80.0,
      alignment: Alignment.center,
      child: Image.asset(
        "assets/images/logo.png",
        fit: BoxFit.contain,
      ),
    );
  }
}
