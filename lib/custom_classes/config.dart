class Config {
  // static const String baseUrl = "http://localhost:8080/";
  // static const String baseUrl = "http://192.168.12.93/tanesco-app-portal/web/";
  static const String baseUrl = "http://196.45.42.90/tanesco-app-portal/web/";

  static const String copyRightLabel = "\u00a9 2019 TANESCO";
  static const String registerLabel = "Register";
  static const String userGuideLabel = "User Guide (Tour)";
  static const String newsLabel = "News";
  static const String faqsLabel = "FAQS";
  static const String toolsLabel = "Tools";
  static const String loginLabel = "Login";
  static const String settingsLabel = "Settings";
  static const String otherLinksLabel = "Other Links";
  static const String commingSoonLabel = "Coming Soon";
  static const String emergencyServiceLabel = "Emergency";
  static const String requestServiceLabel = "Request";
  static const String enquiryServiceLabel = "Enquiry";
  static const String complaintsServiceLabel = "Complaint";
  static const String mobileLabel = "Mobile Number";
  static const String tokenVerifyLabel = "VERIFICATION TOKEN";
  static const String langButtonLabel = "SAVE";
  static const String verificationTokenLabel = "Token Verification";
  static const String tanescoLabel = "Tanesco";
  static const String appNameLabel = "TanescoApp";
  static const String appNameDescLabel = "Tanesco Mobile Application";
  static const String homeLabel = "TANESCO";
  // static const String homeLabel = "Dashboard";
  static const String servicesLabel = "Services";
  static const String logoutLabel = "Logout";
  static const String profileLabel = "Manage Profile";
  static const String unregisterLabel = "Unregister Device";
  static const String reportedServicesLabel = "Reported Services";
  static const String languageSelectionLabel = "Language Selection";
  static const int notificationSeconds = 30;

  static const String selectLangEn =
      "Select Application Default Language \n This can be changed later in your profile";
  static const String labelLangEn = "ENGLISH";
  static const String labelLangSw = "SWAHILI";

  static String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
}
