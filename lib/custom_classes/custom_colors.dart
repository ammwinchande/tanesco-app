import 'package:flutter/material.dart';

class CustomColors {
  static const Color green = Color(0xFF005900);
  static const Color yellow = Color(0xFFFFBF00);
  static const Color black = Color(0xEF000000);
  static const Color grey = Color(0xEF3D3D3D);
  static const Color white = Color(0xEFFFFFFF);
}
