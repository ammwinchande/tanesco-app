import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rich_alert/rich_alert.dart';

class Alert {
  showErrorAlert({context, String message}) {
    return showDialog(
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 3), () {
          Navigator.of(context).pop(true);
        });
        return RichAlertDialog(
          alertTitle: richTitle('Fail'.toUpperCase()),
          alertSubtitle: richSubtitle(message),
          alertType: RichAlertType.ERROR,
          actions: <Widget>[
            FlatButton(
              color: Colors.red.shade700,
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Raleway',
                  fontSize: 16.0,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  showSuccessAlert({context, String message}) {
    return showDialog(
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 3), () {
          Navigator.of(context).pop(true);
        });
        return RichAlertDialog(
          alertTitle: richTitle('Success'),
          alertSubtitle: richSubtitle(message),
          alertType: RichAlertType.SUCCESS,
          actions: <Widget>[
            FlatButton(
              color: Colors.green.shade700,
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Raleway',
                  fontSize: 16.0,
                ),
              ),
            )
          ],
        );
      },
    );
  }

  showProgressDialog({context}) {
    ProgressDialog pr =
        new ProgressDialog(context, type: ProgressDialogType.Normal);

    pr.style(
      message: 'Submitting your report...',
      borderRadius: 5.0,
      backgroundColor: Colors.white,
      progressWidget: LinearProgressIndicator(),
      elevation: 10.0,
      insetAnimCurve: Curves.fastLinearToSlowEaseIn,
      progressTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 13.0,
        fontWeight: FontWeight.w400,
      ),
      messageTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 19.0,
        fontWeight: FontWeight.w600,
      ),
    );

    pr.show();

    return Future.delayed(Duration(seconds: 1)).then(
      (value) {
        pr.update(message: "Almost done...");

        Future.delayed(Duration(seconds: 1)).then((value) {
          pr.update(
            message: "Successfully reported...",
            progressWidget: Icon(
              Icons.check,
              color: Colors.green,
              size: 35.0,
            ),
          );

          Future.delayed(Duration(seconds: 1)).then((value) {
            pr.hide();
          });
        });
      },
    );
  }
}
