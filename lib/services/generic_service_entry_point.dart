import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/login.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/service_locator.dart';
import 'package:tanesco/services/generic_service_form.dart';

class GenericServiceEntryPoint extends StatefulWidget {
  final String title;
  final Client client;
  final int serviceId;
  final String reportedForComplaint;
  GenericServiceEntryPoint(
      {this.title,
      this.client,
      this.serviceId,
      this.reportedForComplaint,
      Key key})
      : super(key: key);
  @override
  _GenericServiceEntryPointState createState() =>
      _GenericServiceEntryPointState();
}

class _GenericServiceEntryPointState extends State<GenericServiceEntryPoint> {
  ///
  /// Restricting Access to un-authorized Client
  ///
  _isClientAuthentic() {
    var storageService = sl.get("LocalStorageService");

    /// Has User Confirmed SignedUp/LoggedIn Verification Code.?
    bool hasLoggedIn = storageService.hasLoggedIn;
    if ([null, false].contains(hasLoggedIn))
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.leftToRightWithFade,
              child: LoginPage(
                title: Config.loginLabel,
              )));
  }

  @override
  initState() {
    super.initState();
    _isClientAuthentic();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GenericServiceForm(
        client: widget.client,
        serviceId: widget.serviceId,
        title: widget.title,
        reportedForComplaint: widget.reportedForComplaint,
      ),
      bottomNavigationBar: CustomAppBar().getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }
}
