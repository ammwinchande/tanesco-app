import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:tanesco/custom_classes/config.dart';

class Location {
  static final String baseUrl = Config.baseUrl;

  /// Fetch regions from API
  ///
  /// @param
  /// @return List message
  Future<List> getRegions() async {
    var response = await http.get(Uri.encodeFull(baseUrl + '/api/region'),
        headers: {"Accept": "application/json"});
    return json.decode(response.body);
  }

  /// Fetch districts from API based on
  ///
  /// @param int regionId
  /// @return List message
  Future<List> getDistricts({int regionId}) async {
    var response = await http.get(
        Uri.encodeFull(baseUrl + '/api/district/?region_id=$regionId'),
        headers: {"Accept": "application/json"});
    return json.decode(response.body);
  }

  /// Fetch areas from API based on district_id
  ///
  /// @param int districtId
  /// @return List message
  Future<List> getAreas({int districtId}) async {
    var response = await http.get(
        Uri.encodeFull(baseUrl + '/api/area/?district_id=$districtId'),
        headers: {"Accept": "application/json"});
    return json.decode(response.body);
  }

  /// Fetch streets from API based on area_id
  ///
  /// @param int areaId
  /// @return List message
  Future<List> getStreets({int areaId}) async {
    var response = await http.get(
        Uri.encodeFull(baseUrl + '/api/street/?area_id=$areaId'),
        headers: {"Accept": "application/json"});
    return json.decode(response.body);
  }
}
