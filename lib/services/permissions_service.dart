import 'package:permission_handler/permission_handler.dart';

class PermissionsService {
  final PermissionHandler _permissionHandler = PermissionHandler();
  Map<PermissionGroup, PermissionStatus> permissions;

  /****************************** Permissions CallBacks Definitions *****************************/

  ///
  /// Check All App Permission
  /// @return Future for requesting permission if not granted
  ///
  void getPermissions() async {
    // permissions = await PermissionHandler().requestPermissions([
    permissions = await _permissionHandler.requestPermissions([
      PermissionGroup.storage,
      PermissionGroup.phone,
      PermissionGroup.sms,
      PermissionGroup.location,
      PermissionGroup.locationAlways,
      PermissionGroup.locationWhenInUse,
      PermissionGroup.camera,
      PermissionGroup.photos,
      PermissionGroup.notification,
    ]);
  }

  /// [Requests] any users permission.
  Future<bool> _requestPermission(PermissionGroup permission) async {
    Map<PermissionGroup, PermissionStatus> result = await _permissionHandler.requestPermissions([permission]);
    if (result[permission] == PermissionStatus.granted) {
      return true;
    }
    return false;
  }

  /// [Check] if App has any of the mentioned/specific users permission.
  Future<bool> hasPermission(PermissionGroup permission) async {
    PermissionStatus permissionStatus = await _permissionHandler.checkPermissionStatus(permission);
    return permissionStatus == PermissionStatus.granted;
  }

  /// [Check ServiceStatus] if App has any of the mentioned/specific users service.
  Future<bool> hasService(PermissionGroup permission) async {
    ServiceStatus serviceStatus = await _permissionHandler.checkServiceStatus(permission);
    return serviceStatus == ServiceStatus.enabled;
  }

  /// [Check AppSettings] if its opened.
  Future<bool> appSetting() async {
    bool isOpened = await _permissionHandler.openAppSettings();
    return isOpened;
  }

  /// [Check PermissionRationale] if its shawn.
  Future<bool> requestPermissionRationale() async {
    bool isShown = await PermissionHandler().shouldShowRequestPermissionRationale(PermissionGroup.contacts);
    return isShown;
  }

  /****************************** Requesting & Checking for Permissions *****************************/

  /// [Storage Permissions]

  /// [Requests] the users permission to read their contacts.
  Future<bool> requestStoragePermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.storage);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  /// [Check] if App has Contacts Permissions.
  Future<bool> hasStoragePermission() async {
    return hasPermission(PermissionGroup.storage);
  }

  /// [SMS Permissions]

  /// [Requests] the users permission to read their contacts.
  Future<bool> requestSmsPermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.sms);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  /// [Check] if App has Contacts Permissions.
  Future<bool> hasSmsPermission() async {
    return hasPermission(PermissionGroup.sms);
  }

  /// [PhoneNumbers Permissions]

  /// [Requests] the users permission to read their contacts.
  Future<bool> requestPhonePermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.phone);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  /// [Check] if App has Contacts Permissions.
  Future<bool> hasPhonePermission() async {
    return hasPermission(PermissionGroup.phone);
  }

  /// [Contacts Permissions]

  /// [Requests] the users permission to read their contacts.
  Future<bool> requestContactsPermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.contacts);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  /// [Check] if App has Contacts Permissions.
  Future<bool> hasContactsPermission() async {
    return hasPermission(PermissionGroup.contacts);
  }

  /// [Location Permissions]

  /// [Requests] the users permission to read their contacts.
  Future<bool> requestLocationPermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.locationWhenInUse);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  /// [Check] if App has Contacts Permissions.
  Future<bool> hasLocationPermission() async {
    return hasPermission(PermissionGroup.locationWhenInUse);
  }

  /// [Camera Permissions]

  /// [Requests] the users permission to read their contacts.
  Future<bool> requestCameraPermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.camera);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  /// [Check] if App has Contacts Permissions.
  Future<bool> hasCameraPermission() async {
    return hasPermission(PermissionGroup.camera);
  }

  /// [Photos Permissions]

  /// [Requests] the users permission to read their contacts.
  Future<bool> requestPhotosPermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.photos);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  /// [Check] if App has Contacts Permissions.
  Future<bool> hasPhotosPermission() async {
    return hasPermission(PermissionGroup.photos);
  }
}