import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:page_transition/page_transition.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:tanesco/custom_classes/alert.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/login.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/service_rating.dart';
import 'package:tanesco/models/service_report_implement_repository.dart';
import 'package:tanesco/models/service_status.dart';
import 'package:tanesco/service_locator.dart';
import 'package:tanesco/services/generic_service_entry_point.dart';

class ServicesSummary extends StatefulWidget {
  final String title;
  final Client client;

  ServicesSummary({Key key, this.title, this.client}) : super(key: key);
  @override
  _ServicesSummaryState createState() => _ServicesSummaryState();
}

class _ServicesSummaryState extends State<ServicesSummary> {
  final _formKey = GlobalKey<FormState>();
  final String baseUrl = Config.baseUrl;
  CustomAppBar customAppBar = CustomAppBar();

  final _serviceStatus = ServiceStatus();
  final _serviceRating = ServiceRating();

  double _rating;

  TextStyle cardLabel = TextStyle(
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w600,
    fontSize: 16.0,
    color: CustomColors.yellow,
  );

  List services = List();
  List statuses = ['received', 'in progress', 'resolved'];
  List reportSummaries = List();

  /// Fetch services from API
  ///
  /// @param
  /// @return String
  Future<String> getServices() async {
    var response = await http.get(Uri.encodeFull(baseUrl + '/api/service'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      services = responseBody;
    });
    return 'Success';
  }

  /// Fetch previous reported issues based on mobile number
  /// from API
  ///
  /// @param
  /// @return String
  Future<String> getReportedServices() async {
    var response = await http.get(
      Uri.encodeFull(baseUrl +
          '/api/service/report-summary/?mobile_no=${widget.client.phone}'),
      headers: {"Accept": "application/json"},
    );
    var responseBody = json.decode(response.body);
    setState(() {
      reportSummaries = responseBody;
    });
    return 'Succes';
  }

  ///
  /// Restricting Access to un-authorized Client
  ///
  _isClientAuthentic() {
    var storageService = sl.get("LocalStorageService");

    /// Has User Confirmed SignedUp/LoggedIn Verification Code.?
    bool hasLoggedIn = storageService.hasLoggedIn;
    if ([null, false].contains(hasLoggedIn))
      Navigator.push(
        context,
        PageTransition(
          type: PageTransitionType.leftToRightWithFade,
          child: LoginPage(),
        ),
      );
  }

  @override
  void initState() {
    super.initState();
    this.getServices();
    this._isClientAuthentic();
    this.getReportedServices();
    this._rating = 3;
  }

  /// Add Column spacing
  ///
  /// @return SizedBox
  Widget _separator({height = 0}) {
    return SizedBox(
      height: height == 0 ? 10.0 : height,
    );
  }

  /// RENDER feedback button for individual service
  /// if attended then show Confirm if not Get status
  ///
  /// @param String status, String referenceNumber
  /// @return MaterialButton
  Center _renderGetStatusButton({String status, String referenceNumber}) {
    return Center(
      child: Material(
        elevation: 16.0,
        color: CustomColors.green,
        borderRadius: BorderRadius.circular(50.0),
        child: MaterialButton(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            'Get Status'.toUpperCase(),
            textAlign: TextAlign.center,
            style: cardLabel.copyWith(
              color: Colors.white,
              letterSpacing: 1,
            ),
          ),
          onPressed: () => _getServiceStatus(referenceNumber),
        ),
      ),
    );
  }

  /// RENDER feedback button for individual service
  /// if attended then show Confirm if not Get status
  ///
  /// @param String status, String referenceNumber
  /// @return MaterialButton
  Material _renderSatisfactionButton(
      {@required String referenceNumber,
      @required String details,
      @required int serviceId,
      @required String isMeterRequired,
      @required bool isSatisfied}) {
    return Material(
      elevation: 16.0,
      color: isSatisfied ? CustomColors.yellow : Colors.red.shade600,
      borderRadius: BorderRadius.circular(50.0),
      child: Center(
        child: MaterialButton(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            isSatisfied
                ? 'Resolved'.toUpperCase()
                : 'Not Resolved'.toUpperCase(),
            textAlign: TextAlign.center,
            style: cardLabel.copyWith(
              color: Colors.white,
              letterSpacing: 1,
            ),
          ),
          onPressed: () => _renderFeedbackDialog(
            serviceNo: referenceNumber,
            details: details,
            serviceId: serviceId,
            isMeterRequired: isMeterRequired,
            isSatisfied: isSatisfied,
          ),
        ),
      ),
    );
  }

  /// Show both feedback buttons for individual service
  ///
  /// @param String referenceNumber, String details, String isMeterRequired
  /// @return Row
  Row _showSatisfactionButtons({
    @required String referenceNumber,
    @required String details,
    @required int serviceId,
    @required String isMeterRequired,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _renderSatisfactionButton(
          referenceNumber: referenceNumber,
          details: details,
          serviceId: serviceId,
          isMeterRequired: isMeterRequired,
          isSatisfied: true,
        ),
        _renderSatisfactionButton(
          referenceNumber: referenceNumber,
          details: details,
          serviceId: serviceId,
          isMeterRequired: isMeterRequired,
          isSatisfied: false,
        ),
      ],
    );
  }

  /// Render content text
  ///
  /// @param String title, String content, bool upperCaseContent
  /// @return Row
  Row _renderCardContent(
      {@required String title,
      @required String content,
      bool upperCaseContent = false}) {
    return Row(
      children: <Widget>[
        Text(
          title,
          style: cardLabel,
        ),
        Expanded(
          child: Text(
            upperCaseContent
                ? content.length != 0 ? content.toUpperCase() : content
                : content.length != 0 ? Config.capitalize(content) : content,
            style: TextStyle(
              fontFamily: 'Raleway',
              fontSize: 16.0,
              fontWeight: FontWeight.normal,
            ),
          ),
        ),
      ],
    );
  }

  /// Render repor summary based on user reported issues
  ///
  /// @return ListView
  ListView _reportSummary() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      physics: ScrollPhysics(),
      padding: const EdgeInsets.all(8),
      itemCount: reportSummaries.length,
      itemBuilder: (BuildContext context, int index) {
        var parsedDate = DateTime.parse(reportSummaries[index]['created_at']);
        String status =
            reportSummaries[index]['status'].toString().toLowerCase();
        return Card(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 16.0),
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                _renderCardContent(
                  title: 'Reference No: ',
                  content: reportSummaries[index]['service_no'].toString(),
                  upperCaseContent: true,
                ),
                _separator(),
                _renderCardContent(
                  title: 'Reported Issue: ',
                  content: reportSummaries[index]['sub_service_name'],
                  upperCaseContent: true,
                ),
                _separator(),
                _renderCardContent(
                  title: 'Report Details: ',
                  content: reportSummaries[index]['details'] != null
                      ? reportSummaries[index]['details'].toString()
                      : '',
                ),
                _separator(),
                _renderCardContent(
                  title: 'Status: ',
                  content: reportSummaries[index]['status'].toString(),
                ),
                _separator(),
                reportSummaries[index]['remarks'] != null
                    ? _renderCardContent(
                        title: 'Remarks: ',
                        content: reportSummaries[index]['remarks'])
                    : Container(),
                _separator(),
                reportSummaries[index]['feedback'] != null
                    ? _renderCardContent(
                        title: 'Feedback: ',
                        content: reportSummaries[index]['feedback'])
                    : Container(),
                _separator(),
                _renderCardContent(
                  title: 'Reported Date: ',
                  content: DateFormat('dd-MM-yyyy').format(parsedDate),
                ),
                _separator(),
                SizedBox(
                  height: 16.0,
                  child: Divider(
                    height: 16.0,
                    color: Colors.lightGreen.shade400,
                  ),
                ),
                status == 'closed'
                    ? Row(
                        children: <Widget>[
                          Text(
                            'Rate: ',
                            style: cardLabel.copyWith(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          _renderRatingAfterFeedback(
                            mode: 1,
                            rating: reportSummaries[index]['rating'] != null
                                ? double.parse(
                                    reportSummaries[index]['rating'],
                                  )
                                : 0.0, // if not rating/null show zero on client
                          )
                        ],
                      )
                    : status == 'resolved'
                        ? _showSatisfactionButtons(
                            referenceNumber:
                                reportSummaries[index]['service_no'].toString(),
                            details: reportSummaries[index]['details'] != null
                                ? reportSummaries[index]['details'].toString()
                                : '',
                            serviceId: int.parse(reportSummaries[index]
                                    ['service_id']
                                .toString()),
                            isMeterRequired: reportSummaries[index]
                                    ['is_meter_required']
                                .toString(),
                          )
                        : _renderGetStatusButton(
                            status: reportSummaries[index]['status'],
                            referenceNumber: reportSummaries[index]
                                ['service_no'],
                          ),
              ],
            ),
          ),
        );
      },
    );
  }

  /// Fetch current status from API
  ///
  /// @param String serviceNo i.e. Reference Number
  /// @return
  void _getServiceStatus(serviceNo) {
    _serviceStatus.deviceId = widget.client.deviceId;
    _serviceStatus.mobileNo = widget.client.phone;
    _serviceStatus.serviceNo = serviceNo;

    // DIALOG loder
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );

    pr.style(
      message: 'Please wait...',
      borderRadius: 5.0,
      backgroundColor: Colors.white,
      progressWidget: LinearProgressIndicator(
        backgroundColor: CustomColors.green,
        valueColor: AlwaysStoppedAnimation(CustomColors.yellow),
      ),
      elevation: 10.0,
      insetAnimCurve: Curves.fastLinearToSlowEaseIn,
      progressTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 13.0,
        fontWeight: FontWeight.w400,
      ),
      messageTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 19.0,
        fontWeight: FontWeight.w600,
      ),
    );

    pr.show();

    ServiceReportImplementRepository()
        .requestStatus(_serviceStatus)
        .then((result) {
      if (result.isEmpty) {
        pr.hide();
        Alert().showErrorAlert(
          context: context,
          message: 'Unable to process your request',
        );
      } else {
        Future.delayed(Duration(seconds: 1)).then((value) {
          pr.update(message: 'Fetching status ...');

          pr.hide();
          // show dialog with status
          String responseJson = json.decode(result)["data"];
          showDialog(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Report Service Status'),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      _renderCardContent(
                        title: 'Service No.',
                        content: serviceNo,
                        upperCaseContent: true,
                      ),
                      _separator(),
                      _renderStatusDialog(content: responseJson),
                    ],
                  ),
                ),
                actions: <Widget>[
                  Material(
                    elevation: 7.0,
                    borderRadius: BorderRadius.circular(50.0),
                    color: Colors.grey.shade400,
                    child: MaterialButton(
                      padding: const EdgeInsets.all(16.0),
                      onPressed: () {
                        Navigator.of(context).pop();
                        this.getReportedServices();
                      },
                      child: Text(
                        'ok'.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: cardLabel.copyWith(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          );
        });
      }
    });
  }

  /// Show status and remarks dialog based on content
  ///
  /// @param String content
  /// @return
  Row _renderStatusDialog({String content}) {
    List statement;
    if (content.contains('-')) {
      statement = content.split('-');
      return Row(
        children: <Widget>[
          _renderCardContent(title: 'Status: ', content: statement[0]),
          _renderCardContent(title: 'Remarks: ', content: statement[1]),
        ],
      );
    } else {
      return _renderCardContent(title: 'Status: ', content: content);
    }
  }

  /// Render progress bar
  ///
  ///
  progressIndicator() {
    CircularProgressIndicator();
    Future.delayed(Duration(seconds: 1)).then((value) {
      Container(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Text(
              'You have not reported any service yet!',
              style: TextStyle(
                fontFamily: 'Raleway',
                fontSize: 16.0,
              ),
            ),
          ),
        ),
      );
    });
  }

  /// render feedback dialog
  ///
  /// @param String serviceNo
  /// @return ShowDialog
  _renderFeedbackDialog({
    @required String serviceNo,
    @required String details,
    @required int serviceId,
    @required String isMeterRequired,
    @required bool isSatisfied,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Rate this service',
            style: cardLabel.copyWith(
              fontWeight: FontWeight.w600,
              color: Colors.grey.shade900,
            ),
            textAlign: TextAlign.center,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7.0),
          ),
          content: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: ListBody(
                children: <Widget>[
                  _renderCardContent(
                    title: 'Reference No: ',
                    content: serviceNo,
                  ),
                  _separator(height: 20.0),
                  _ratingBar(mode: 1),
                  _separator(height: 20.0),
                  _feedbackField(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      _renderSubmitFeedbackButton(
                        submit: true,
                        serviceNo: serviceNo,
                        details: details,
                        serviceId: serviceId,
                        isMeterRequired: isMeterRequired,
                        isSatisfied: isSatisfied,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      _renderSubmitFeedbackButton(
                        submit: false,
                        serviceId: serviceId,
                        isSatisfied: isSatisfied,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          // actions: <Widget>[

          // ],
        );
      },
    );
  }

  /// Render rating faces for all services with status of
  /// resolved
  ///
  /// @param mode, 1 - stars, 2-faces
  /// @return
  _renderRatingAfterFeedback({int mode, rating}) {
    switch (mode) {
      case 1:
        return RatingBarIndicator(
          rating: rating,
          direction: Axis.horizontal,
          unratedColor: Colors.grey[200],
          itemCount: 5,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: CustomColors.green,
          ),
        );
      case 2:
        return RatingBarIndicator(
          rating: rating,
          direction: Axis.horizontal,
          itemCount: 5,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, index) {
            switch (index) {
              case 0:
                return Icon(
                  Icons.sentiment_very_dissatisfied,
                  color: Colors.red,
                );
              case 1:
                return Icon(
                  Icons.sentiment_dissatisfied,
                  color: Colors.redAccent,
                );
              case 2:
                return Icon(
                  Icons.sentiment_neutral,
                  color: Colors.amber,
                );
              case 3:
                return Icon(
                  Icons.sentiment_satisfied,
                  color: Colors.lightGreen,
                );
              case 4:
                return Icon(
                  Icons.sentiment_very_satisfied,
                  color: CustomColors.green,
                );
              default:
                return Container();
            }
          },
        );
      default:
        return Container();
    }
  }

  /// Render rating faces for all services with status of
  /// resolved
  ///
  /// @param mode, 1 - stars, 2-faces
  /// @return
  Widget _ratingBar({int mode}) {
    switch (mode) {
      case 1:
        return RatingBar(
          initialRating: _rating,
          direction: Axis.horizontal,
          allowHalfRating: true,
          unratedColor: Colors.grey[200],
          itemCount: 5,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: CustomColors.green,
          ),
          onRatingUpdate: (rating) {
            setState(() {
              _rating = rating;
            });
          },
        );
      case 2:
        return RatingBar(
          initialRating: 3,
          direction: Axis.horizontal,
          itemCount: 5,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, index) {
            switch (index) {
              case 0:
                return Icon(
                  Icons.sentiment_very_dissatisfied,
                  color: Colors.red,
                );
              case 1:
                return Icon(
                  Icons.sentiment_dissatisfied,
                  color: Colors.redAccent,
                );
              case 2:
                return Icon(
                  Icons.sentiment_neutral,
                  color: Colors.amber,
                );
              case 3:
                return Icon(
                  Icons.sentiment_satisfied,
                  color: Colors.lightGreen,
                );
              case 4:
                return Icon(
                  Icons.sentiment_very_satisfied,
                  color: CustomColors.green,
                );
              default:
                return Container();
            }
          },
          onRatingUpdate: (rating) {
            setState(() {
              _rating = rating;
              _serviceRating.rating = _rating;
            });
          },
        );
      default:
        return Container();
    }
  }

  ///
  /// First name form field
  ///
  TextFormField _feedbackField() {
    return TextFormField(
      autofocus: true,
      autovalidate: true,
      obscureText: false,
      style: TextStyle(
        fontFamily: 'Raleway',
        fontSize: 16.0,
      ),
      keyboardType: TextInputType.text,
      maxLength: 120,
      decoration: InputDecoration(
        hintText: 'Describe your experience',
        hintStyle: cardLabel.copyWith(color: Colors.grey.shade500),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0.0),
          borderSide: BorderSide(color: Colors.grey.shade400, width: 1.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0.0),
          borderSide: BorderSide(color: Colors.grey.shade300, width: 1.0),
        ),
      ),
      validator: (String value) {
        return null;
      },
      onSaved: (String value) {
        value.isEmpty
            ? _serviceRating.feedback = null
            : _serviceRating.feedback = value;
      },
    );
  }

  /// Submit feedback from client
  ///
  /// @param String serviceNo i.e. Reference Number
  /// @return
  void _submitFeedback({
    @required String serviceNo,
    @required String details,
    @required String isMeterRequired,
    @required int serviceId,
    @required bool isSatisfied,
  }) {
    Navigator.of(context).pop();

    /// DIALOG loader
    ProgressDialog pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );
    if (_formKey.currentState.validate()) {
      _serviceRating.rating = _rating;
      _serviceRating.mobileNo = widget.client.phone;
      _serviceRating.deviceId = widget.client.deviceId;
      _serviceRating.serviceNo = serviceNo;
      _formKey.currentState.save();

      pr.style(
        message: 'Submitting your feedback...',
        borderRadius: 5.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.fastLinearToSlowEaseIn,
        progressTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 13.0,
          fontWeight: FontWeight.w400,
        ),
        messageTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 19.0,
          fontWeight: FontWeight.w600,
        ),
      );

      pr.show();

      Future.delayed((Duration(seconds: 1))).then(
        (value) {
          ServiceReportImplementRepository().addFeedback(_serviceRating).then(
            (jsonData) {
              if (jsonData.isNotEmpty) {
                var responseStatus = json.decode(jsonData)["status"];
                if (responseStatus == 'success') {
                  if (isSatisfied) {
                    this.getReportedServices();
                    Navigator.of(context).pop();
                  } else {
                    Route complaintRoute = MaterialPageRoute(
                      builder: (context) => GenericServiceEntryPoint(
                        title: 'complaint'.toUpperCase(),
                        client: widget.client,
                        reportedForComplaint:
                            serviceNo + '@' + details + '&' + isMeterRequired,
                        serviceId: serviceId,
                      ),
                    );
                    Navigator.of(context).pop();
                    Navigator.pushReplacement(context, complaintRoute);
                  }
                }
              }
            },
          );
        },
      );
    }
  }

  ///
  /// render feedback button feedback
  ///
  _renderSubmitFeedbackButton({
    @required bool submit,
    String serviceNo,
    String details,
    @required int serviceId,
    String isMeterRequired,
    @required bool isSatisfied,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(50.0),
      color: submit ? CustomColors.green : Colors.grey.shade400,
      child: MaterialButton(
        padding: const EdgeInsets.all(16.0),
        onPressed: () => submit
            ? _submitFeedback(
                serviceNo: serviceNo,
                details: details,
                serviceId: serviceId,
                isMeterRequired: isMeterRequired,
                isSatisfied: isSatisfied,
              )
            : Navigator.of(context).pop(),
        child: Text(
          submit
              ? isSatisfied ? 'rate'.toUpperCase() : 'complain'.toUpperCase()
              : 'cancel'.toUpperCase(),
          textAlign: TextAlign.center,
          style: cardLabel.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  ///
  /// Render body
  ///
  Container renderBody() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Card(
            color: Colors.lightGreen.shade100,
            margin: EdgeInsets.symmetric(
              horizontal: 10.0,
              vertical: 10.0,
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                child: Text(
                  'previous reported services'.toUpperCase(),
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 20.0,
                    color: CustomColors.green,
                  ),
                ),
              ),
            ),
          ),
          // list view for all reported/requested services
          reportSummaries.isEmpty
              ? Container(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: Text('No Reported Services Found'.toUpperCase()),
                    ),
                  ),
                )
              : _reportSummary(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          customAppBar.getAppBar(title: widget.title),
          SliverToBoxAdapter(
            child: renderBody(),
          )
        ],
      ),
      bottomNavigationBar: customAppBar.getBottomAppBar(
        copyRightLabel: "\u00a9 ${DateTime.now().year} TANESCO",
      ),
    );
  }
}
