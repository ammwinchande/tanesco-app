import 'dart:convert';

import 'package:bot_toast/bot_toast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:strings/strings.dart';
import 'package:tanesco/custom_classes/alert.dart';
import 'package:tanesco/custom_classes/config.dart';
import 'package:tanesco/custom_classes/custom_appbar.dart';
import 'package:tanesco/dashboard.dart';
import 'package:tanesco/login.dart';
import 'package:tanesco/models/client.dart';
import 'package:tanesco/models/client_database_repository.dart';
import 'package:tanesco/models/service_report.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:tanesco/custom_classes/custom_colors.dart';
import 'package:tanesco/models/service_report_implement_repository.dart';
import 'package:tanesco/service_locator.dart';

class GenericServiceForm extends StatefulWidget {
  final Client client;
  final int serviceId;
  final String title;
  final String reportedForComplaint;
  GenericServiceForm(
      {this.client,
      this.serviceId,
      this.title,
      this.reportedForComplaint,
      Key key})
      : super(key: key);
  @override
  _GenericServiceFormState createState() => new _GenericServiceFormState();
}

class _GenericServiceFormState extends State<GenericServiceForm> {
  final _formKey = GlobalKey<FormState>();
  final _serviceReport = ServiceReport();
  final String baseUrl = Config.baseUrl;
  List _reportedServices = List();

  CustomAppBar _customAppBar = CustomAppBar(); // for appBar
  ClientDatabaseRepository _clientDatabaseRepository =
      ClientDatabaseRepository(); // client database repository

  TextEditingController _meternoTextEditingController = TextEditingController();

  bool _renderMeterNoField = false; // for drawing meter number field
  bool _renderLocationBuilds = true; // for location details
  bool _makeMeterNoPrimary = false;

  /// IMPLEMENT local notifications
  // FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;

  int currStep = 0;
  TextStyle style = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 16.0,
  );
  TextStyle _screenHeaderStyle = TextStyle(
    fontSize: 16.0,
    fontFamily: "Raleway",
    fontWeight: FontWeight.w500,
  );
  TextStyle _stepperTitleStyle = TextStyle(
    fontSize: 16.0,
    fontFamily: "Raleway",
    fontWeight: FontWeight.w600,
  );

  String _selectedRegion;
  String _selectedDistrict;
  String _selectedArea;
  String _selectedStreet;
  String _selectedSubService;
  String _selectedComplaint;

  List regions = List();
  List districts = List();
  List areas = List();
  List streets = List();
  List categories = List();
  List subServices = List();

  List meterVerificationMessage = List(); // store verification message from API

  setPreviousMeterNumber({meterNumber}) {
    setState(() {});
  }

  /// Fetch regions from API
  ///
  /// @param
  /// @return String message
  ///
  Future<String> getRegions() async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/region'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      regions = responseBody;
    });
    return 'Success';
  }

  /// Fetch districts from API based on
  ///
  /// @param int regionId
  /// @return String message
  Future<String> getDistricts({regionId}) async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/district/?region_id=$regionId'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      districts = responseBody;
    });
    return 'Success';
  }

  /// Fetch areas from API based on district_id
  ///
  /// @param int districtId
  /// @return String message
  Future<String> getAreas({districtId}) async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/area/?district_id=$districtId'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      areas = responseBody;
    });
    return 'Success';
  }

  /// Fetch streets from API based on area_id
  ///
  /// @param int areaId
  /// @return String message
  Future<String> getStreets({areaId}) async {
    var response = await http.get(
        Uri.encodeFull(Config.baseUrl + '/api/street/?area_id=$areaId'),
        headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);

    setState(() {
      streets = responseBody;
    });
    return 'Success';
  }

  /// Fetch Sub Categories from API
  ///
  /// @param
  /// @return String message
  Future<String> getSubServices() async {
    var response = await http.get(
      Uri.encodeFull(
        baseUrl + '/api/sub-service/?service_id=${widget.serviceId}',
      ),
      headers: {"Accept": "application/json"},
    );
    var responseBody = json.decode(response.body);

    setState(() {
      subServices = responseBody;
    });
    return 'Success';
  }

  /// Verify meter number from API
  ///
  /// @param String meterno
  /// @return String message
  Future<String> verifyMeterNumber(String meterno) async {
    var response = await http.get(
      Uri.encodeFull(
        baseUrl + '/api/client/verify-meter/?meter_number=$meterno',
      ),
      headers: {"Accept": "application/json"},
    );
    var responseBody = json.decode(response.body);

    setState(() {
      meterVerificationMessage = responseBody;
    });
    return 'Success';
  }

  /// Fetch all reported services per customer as complaints
  /// days for reported services currently within 14days
  ///
  /// @param int days
  /// @return String success
  Future<String> getReportedServices({int days}) async {
    var response = await http.get(
      Uri.encodeFull(baseUrl +
          '/api/service/report-summary/?mobile_no=${widget.client.phone}&days=$days'),
      headers: {'Accept': 'application/json'},
    );
    var responseBody = json.decode(response.body);
    setState(() {
      _reportedServices = responseBody;
    });

    return 'Succes';
  }

  @override
  void dispose() {
    super.dispose();
    _meternoTextEditingController.dispose();
  }

  ///
  /// Restricting Access to un-authorized Client
  ///
  _isClientAuthentic() {
    var storageService = sl.get("LocalStorageService");

    /// Has User Confirmed SignedUp/LoggedIn Verification Code.?
    bool hasLoggedIn = storageService.hasLoggedIn;
    if ([null, false].contains(hasLoggedIn))
      Navigator.push(
        context,
        PageTransition(
          type: PageTransitionType.leftToRightWithFade,
          child: LoginPage(),
        ),
      );
  }

  @override
  initState() {
    super.initState();
    this._isClientAuthentic();
    this.getSubServices();
    this.getRegions();

    widget.title.toLowerCase() == 'complaint'
        ? widget.reportedForComplaint != null
            ? _selectedComplaint = widget.reportedForComplaint
            : _selectedComplaint = null
        : _selectedComplaint = null;

    this.getReportedServices(days: 7); // demo of within 7 days of reporting

    widget.title.toLowerCase() != 'complaint' ?? widget.client.meterno != null
        ? _meternoTextEditingController.text = widget.client.meterno
        : _meternoTextEditingController.text = '';
    _meternoTextEditingController.addListener(meternoTextEditingListener);
  }

  /// Return Stepper state
  ///
  /// @param int step
  /// @return StepState
  StepState _getState(int i) {
    if (currStep >= i)
      return StepState.complete;
    else
      return StepState.indexed;
  }

  FormField _buildRegionDropdown() {
    return FormField<String>(
      validator: (value) {
        if (value == null) {
          return "Select Region";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _serviceReport.region = value;
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InputDecorator(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'Region *',
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  isExpanded: true,
                  hint: Text("Select Region"),
                  value: _selectedRegion,
                  onChanged: (selectedRegion) {
                    state.didChange(selectedRegion);
                    setState(() {
                      _selectedRegion = selectedRegion;
                      _selectedDistrict = null;
                      this.getDistricts(regionId: _selectedRegion);
                    });
                  },
                  items: regions.map((region) {
                    return DropdownMenuItem(
                      child: Text(
                        region['name'].toString().toUpperCase(),
                      ),
                      value: region['id'].toString(),
                    );
                  }).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
      },
    );
  }

  FormField _buildDistrictDropdown() {
    return FormField<String>(
      validator: (value) {
        if (value == null) {
          return "Select District";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _serviceReport.district = value;
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InputDecorator(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'District *',
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  isExpanded: true,
                  hint: Text("Select District"),
                  value: _selectedDistrict,
                  onChanged: (selectedDistrict) {
                    state.didChange(selectedDistrict);
                    setState(() {
                      _selectedDistrict = selectedDistrict;
                      _selectedArea = null;
                      this.getAreas(districtId: _selectedDistrict);
                    });
                  },
                  items: districts.map((district) {
                    return DropdownMenuItem(
                      child: Text(
                        district['name'].toString().toUpperCase(),
                      ),
                      value: district['id'].toString(),
                    );
                  }).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
      },
    );
  }

  FormField _buildAreaDropdown() {
    return FormField<String>(
      validator: (value) {
        if (value == null) {
          return "Select Area";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _serviceReport.area = value;
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InputDecorator(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'Area *',
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  isExpanded: true,
                  hint: Text("Select Area"),
                  value: _selectedArea,
                  onChanged: (selectedArea) {
                    state.didChange(selectedArea);
                    setState(() {
                      _selectedArea = selectedArea;
                      _selectedStreet = null;
                      this.getStreets(areaId: _selectedArea);
                    });
                  },
                  items: areas.map((area) {
                    return DropdownMenuItem(
                      child: Text(
                        area['name'].toString().toUpperCase(),
                      ),
                      value: area['id'].toString(),
                    );
                  }).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
      },
    );
  }

  FormField _buildStreetDropdown() {
    return FormField<String>(
      validator: (value) {
        if (value == null) {
          return "Select Street";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _serviceReport.street = value;
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InputDecorator(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'Street *',
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: Text("Select Street"),
                  value: _selectedStreet,
                  onChanged: (selectedStreet) {
                    state.didChange(selectedStreet);
                    setState(() {
                      _selectedStreet = selectedStreet;
                    });
                  },
                  items: streets.map((street) {
                    return DropdownMenuItem(
                      child: Text(
                        street['name'].toString().toUpperCase(),
                      ),
                      value: street['id'].toString(),
                    );
                  }).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
      },
    );
  }

  TextFormField _buildSpecialMarkFormField() {
    return TextFormField(
      maxLength: 50,
      style: style,
      decoration: InputDecoration(
        icon: Icon(Icons.location_on),
        hintText: 'Your location famous name?',
        labelText: 'Special Mark',
      ),
      onSaved: (value) {
        _serviceReport.specialMark = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          return null;
        } else if (value.length != 0) {
          if (value.length < 3) {
            return 'Please enter special mark';
          } else {
            return null;
          }
        }
        return null;
      },
    );
  }

  ///
  /// Realtime typed meter number checking
  /// Test meter: 07041236790
  ///
  meternoTextEditingListener() {
    if (_meternoTextEditingController.text.length >= 5) {
      Future.delayed((Duration(seconds: 1))).then((message) {
        this.verifyMeterNumber(_meternoTextEditingController.text);
      });
    }
  }

  ///
  /// Changing primary meter number
  /// to update client local and API
  ///
  /// @return Container
  Container _makeMeterPrimary() {
    return Container(
      padding: EdgeInsets.all(0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(
              '- Make this meter number primary',
              style: style.copyWith(
                color: CustomColors.green,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Switch(
            value: _makeMeterNoPrimary,
            onChanged: (value) {
              setState(() {
                _makeMeterNoPrimary = value;
              });
            },
            activeTrackColor: Colors.grey.shade300,
            activeColor: CustomColors.green,
          ),
        ],
      ),
    );
  }

  ///
  /// Meter number form field
  ///
  TextFormField _buildMeterNumberFormField({String reportedMeter}) {
    return TextFormField(
      obscureText: false,
      style: style,
      maxLength: 11,
      autofocus: true,
      controller: _meternoTextEditingController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        icon: Icon(FontAwesomeIcons.tachometerAlt),
        hintText: "Enter Your Meter Number",
        labelText: "Enter Your Meter Number",
      ),
      validator: (String value) {
        String patttern = r'(^[0-9]*$)';
        String limitPattern =
            r'^(\d{5}|\d{7}|\d{9}|\d{11})$'; // enbale this to have starting from 5
        RegExp regExp = new RegExp(patttern);
        RegExp regExpLimit = new RegExp(limitPattern);
        if (value.trim().isEmpty) {
          return 'Meter Number is required';
        }
        if (value.length == 0) {
          return 'Meter Number is Required';
        } else if (!regExp.hasMatch(value)) {
          return 'Enter Correct Meter Number';
        } else if (!regExpLimit.hasMatch(value)) {
          return 'Meter Number is not valid';
        } else if (meterVerificationMessage.isEmpty ||
            meterVerificationMessage[0]['Meter'] == null) {
          return 'Meter Number is not valid';
        }
        return null;
      },
      onSaved: (String value) {
        _serviceReport.meterNumber = value;
      },
      onChanged: (value) {
        value.isEmpty
            ? value = '0'
            : Future.delayed(
                Duration(seconds: 1),
                () {
                  verifyMeterNumber(value);
                },
              );
      },
    );
  }

  ///
  /// Meter number form field
  ///
  TextFormField _buildReportedMeterNumberFormField(
      {@required String reportedMeter}) {
    return TextFormField(
      obscureText: false,
      style: style,
      maxLength: 11,
      autofocus: true,
      controller: _meternoTextEditingController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        icon: Icon(FontAwesomeIcons.tachometerAlt),
        hintText: "Enter Your Meter Number",
        labelText: "Enter Your Meter Number",
      ),
      validator: (String value) {
        String patttern = r'(^[0-9]*$)';
        String limitPattern =
            r'^(\d{5}|\d{7}|\d{9}|\d{11})$'; // enbale this to have starting from 5
        RegExp regExp = new RegExp(patttern);
        RegExp regExpLimit = new RegExp(limitPattern);
        if (value.trim().isEmpty) {
          return 'Meter Number is required';
        }
        if (value.length == 0) {
          return 'Meter Number is Required';
        } else if (!regExp.hasMatch(value)) {
          return 'Enter Correct Meter Number';
        } else if (!regExpLimit.hasMatch(value)) {
          return 'Meter Number is not valid';
        } else if (meterVerificationMessage.isEmpty ||
            meterVerificationMessage[0]['Meter'] == null) {
          return 'Meter Number is not valid';
        }
        return null;
      },
      onSaved: (String value) {
        _serviceReport.meterNumber = value;
      },
      onChanged: (value) {
        value.isEmpty
            ? value = '0'
            : Future.delayed(
                Duration(seconds: 1),
                () {
                  verifyMeterNumber(value);
                },
              );
      },
      initialValue: reportedMeter,
    );
  }

  /// Realtime meter number validation check
  /// when meet criteria e.g. 07041236790
  ///
  /// @param
  /// @return Text
  Widget _realTimeMeterValidation() {
    if (_meternoTextEditingController.text.length < 5) {
      return Text('');
    } else if (meterVerificationMessage == null ||
        meterVerificationMessage.isEmpty) {
      return Text(
        '- Not Found',
        textAlign: TextAlign.left,
        style: style.copyWith(
          fontStyle: FontStyle.italic,
          color: Colors.red.shade600,
        ),
      );
    } else {
      return Text(
        '- ${camelize(meterVerificationMessage[0]['Meter'].toString())}',
        textAlign: TextAlign.start,
        style: style.copyWith(
          color: CustomColors.green,
          fontStyle: FontStyle.italic,
        ),
      );
    }
  }

  /// Build meter number form fields
  ///
  /// @return Container
  Container _renderMeterNumberFormFields({String meterNumber}) {
    return Container(
      child: Column(
        children: <Widget>[
          widget.title.toLowerCase() == 'complaint'
              ? _buildReportedMeterNumberFormField(reportedMeter: meterNumber)
              : _buildMeterNumberFormField(),
          _realTimeMeterValidation(),
          widget.title.toLowerCase() == 'complaint'
              ? _skipRenderMeterNumberFormFields()
              : Padding(
                  padding: EdgeInsets.fromLTRB(0, 8.0, 0, 0),
                  child: _makeMeterPrimary(),
                ),
        ],
      ),
    );
  }

  ///
  /// Provide inputDecoration based on service
  ///
  InputDecoration _inputDecorationLabel() {
    String label = widget.title.toLowerCase() == 'emergency'
        ? 'Problem'
        : camelize(widget.title);
    return InputDecoration(
      contentPadding: EdgeInsets.all(0.0),
      labelText: '$label *',
    );
  }

  ///
  /// Provide Hint inputDecoration based on service
  ///
  InputDecoration _inputDecorationHint() {
    String hint = widget.title.toLowerCase() == 'emergency'
        ? 'Enter problem details'
        : 'Enter ' + camelize(widget.title) + ' details';
    return InputDecoration(
      icon: Icon(Icons.short_text),
      hintText: hint,
      labelText: 'Description *',
    );
  }

  _renderMeterFields() {
    _renderMeterNoField = true;
    _makeMeterNoPrimary = true;
  }

  FormField _buildSubServicesDropdown() {
    return FormField<String>(
      validator: (value) {
        if (value == null) {
          String placeholder = widget.title.toLowerCase() == 'emergency'
              ? 'Select Problem'
              : 'Select ' + camelize(widget.title);
          return placeholder;
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _serviceReport.subServiceId =
            value.toString().split('-')[0]; // split to get subServiceId
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InputDecorator(
              decoration: _inputDecorationLabel(),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  isExpanded: true,
                  hint: widget.title.toLowerCase() == 'emergency'
                      ? Text("Select Problem")
                      : Text("Select " + camelize(widget.title)),
                  value: _selectedSubService,
                  onChanged: (selectedSubService) {
                    state.didChange(selectedSubService);
                    setState(
                      () {
                        // check if meter field should be rendered
                        selectedSubService.toString().split('-')[1] == '1'
                            ? _renderMeterFields()
                            : _renderMeterNoField = false;
                        _selectedSubService = selectedSubService;
                      },
                    );
                  },
                  items: subServices.map(
                    (subService) {
                      return DropdownMenuItem(
                        child: Text(
                          subService['name'].toString().toUpperCase(),
                          style: style,
                        ),
                        value: subService['id'].toString() +
                            '-' +
                            subService['is_meter_required'].toString(),
                      );
                    },
                  ).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style: TextStyle(
                color: Colors.redAccent.shade700,
                fontSize: 12.0,
              ),
            ),
          ],
        );
      },
    );
  }

  TextFormField _buildDescriptionFormField() {
    return TextFormField(
      showCursor: true,
      style: style,
      decoration: _inputDecorationHint(),
      onSaved: (value) {
        _serviceReport.details = value;
      },
      validator: (value) {
        if (value != "") {
          return null;
        } else if (value.length != 0) {
          if (value.length < 7) {
            return "Details must be longer than 7 characters";
          }
        }
        // if (value.isEmpty) {
        //   String statement = widget.title.toLowerCase() == 'emergency'
        //       ? 'problem'
        //       : widget.title.toLowerCase();
        //   return 'Please enter $statement details';
        // } else if (value.length < 7) {
        //   return "Details must be longer than 7 characters";
        // }
        return null;
      },
      keyboardType: TextInputType.multiline,
      maxLines: 4,
      maxLength: 200,
    );
  }

  ///
  /// Show or Hide location form fields
  ///
  /// @return Container
  Container _buildLocationSwitch() {
    return Container(
      padding: EdgeInsets.all(0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(
              'Your Registered Location'.toUpperCase(),
              style: style.copyWith(fontWeight: FontWeight.bold),
            ),
          ),
          Switch(
            value: _renderLocationBuilds,
            onChanged: (value) {
              setState(() {
                _renderLocationBuilds = value;
              });
            },
            activeTrackColor: Colors.grey.shade300,
            activeColor: CustomColors.green,
          ),
        ],
      ),
    );
  }

  /// Build location form fields
  ///
  /// @return Container
  Container _renderLocationFormFields() {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Divider(color: CustomColors.green),
          ),
          _buildRegionDropdown(),
          _buildDistrictDropdown(),
          _buildAreaDropdown(),
          _buildStreetDropdown(),
          _buildSpecialMarkFormField()
        ],
      ),
    );
  }

  /// Skip meter number form fields rendering,
  /// _makeMeterNoPrimary to false
  ///
  /// @return Container
  Container _skipRenderMeterNumberFormFields() {
    _makeMeterNoPrimary = false;
    return Container();
  }

  /// Build form steps
  ///
  /// @param
  /// @return List<Step> steps
  List<Step> _formSteps() {
    List<Step> steps = [
      Step(
        title: Text(
          camelize(widget.title) + ' Details',
          style: _stepperTitleStyle,
        ),
        isActive: true,
        state: _getState(1),
        content: Column(
          children: <Widget>[
            _buildSubServicesDropdown(),
            _buildDescriptionFormField(),
            _renderMeterNoField ? _renderMeterNumberFormFields() : Container(),
          ],
        ),
      ),
      Step(
        title: Text(
          "Location Details",
          style: _stepperTitleStyle,
        ),
        isActive: true,
        state: _getState(2),
        content: Column(
          children: <Widget>[
            _buildLocationSwitch(),
            _renderLocationBuilds ? Container() : _renderLocationFormFields(),
          ],
        ),
      ),
    ];
    return steps;
  }

  Container _formTitle() {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          Text(
            "Report " + camelize(widget.title),
            textDirection: TextDirection.ltr,
            textAlign: TextAlign.left,
            style: _screenHeaderStyle,
          ),
        ],
      ),
    );
  }

  Row _formGuide() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.fromLTRB(16.0, 0, 8, 8),
            child: Text(
              "Fill the form below to report your ${widget.title.toLowerCase()}",
              textDirection: TextDirection.ltr,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontFamily: "Raleway",
                fontSize: 16.0,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
        ),
      ],
    );
  }

  /// Render stepper form fields for all services
  ///
  /// @param
  /// @return Stepper
  Stepper _renderStepperForm() {
    return Stepper(
      physics: ClampingScrollPhysics(), // enable scrolling for stepper
      steps: _formSteps(),
      type: StepperType.vertical,
      currentStep: this.currStep,
      onStepContinue: () {
        setState(
          () {
            if (currStep < _formSteps().length - 1) {
              currStep = currStep + 1;
            } else {
              currStep = 0;
            }
          },
        );
      },
      onStepCancel: () {
        setState(
          () {
            if (currStep > 0) {
              currStep = currStep - 1;
            } else {
              currStep = 0;
            }
          },
        );
      },
      onStepTapped: (step) {
        setState(
          () {
            currStep = step;
          },
        );
      },
      controlsBuilder: (BuildContext context,
          {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
        if (currStep == _formSteps().length - 1) {
          return Row(
            children: <Widget>[
              // submit button
              FlatButton(
                color: CustomColors.green,
                child: Text(
                  'PREVIOUS',
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
                onPressed: onStepCancel,
              ),
              // cancel button
            ],
          );
        } else {
          return Row(
            children: <Widget>[
              FlatButton(
                color: CustomColors.green,
                child: Text(
                  'NEXT',
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
                onPressed: onStepContinue,
              ),
            ],
          );
        }
      },
    );
  }

  /// Render reference number and subservice name dropdown
  ///
  /// @return FormField
  FormField _buildReferenceNumberDropdown() {
    return FormField<String>(
      initialValue: _selectedComplaint,
      validator: (value) {
        if (value == null) {
          String placeholder = widget.title.toLowerCase() == 'emergency'
              ? 'Select Problem'
              : 'Select ' + camelize(widget.title);
          return placeholder;
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _serviceReport.serviceNumber = value
            .toString()
            .split('&')[0]
            .split('@')[0]; // split to get serviceNumber
      },
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InputDecorator(
              decoration: _inputDecorationLabel(),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  isExpanded: true,
                  autofocus: true,
                  hint: widget.title.toLowerCase() == 'emergency'
                      ? Text("Select Problem")
                      : Text(
                          "Select " + camelize(widget.title),
                        ),
                  value: _selectedComplaint,
                  onChanged: (selectedSubService) {
                    var selectedService =
                        selectedSubService.toString().split('@');
                    state.didChange(selectedSubService);
                    setState(
                      () {
                        if (selectedService[1].split('&')[1] == '1') {
                          _renderMeterNoField = true;
                          _makeMeterNoPrimary = true;
                        } else {
                          _renderMeterNoField = false;
                        }
                        _selectedComplaint = selectedSubService;
                      },
                    );
                  },
                  items: _reportedServices.map(
                    (reportedService) {
                      return DropdownMenuItem(
                        child: Text(
                          '(${reportedService['service_no']})' +
                              ' - ' +
                              reportedService['sub_service_name']
                                  .toString()
                                  .toUpperCase(),
                          style: style,
                        ),
                        value: reportedService['service_no'].toString() +
                            '@' +
                            reportedService['details'].toString() +
                            '&' +
                            reportedService['is_meter_required'].toString(),
                      );
                    },
                  ).toList(),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style: TextStyle(
                color: Colors.redAccent.shade700,
                fontSize: 12.0,
              ),
            ),
          ],
        );
      },
    );
  }

  /// Render complaint form
  /// with auto population on various fields
  ///
  /// @param
  /// @return Container
  Container _renderComplaintForm() {
    return Container(
      padding: const EdgeInsets.all(24.0),
      child: Column(
        children: <Widget>[
          _buildReferenceNumberDropdown(),
          _buildDescriptionFormField(),
        ],
      ),
    );
  }

  /// Render correct form per service
  ///
  /// @return Form
  MediaQuery _renderForm() {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      removeBottom: true,
      child: widget.title.toLowerCase() == 'complaint'
          ? _renderComplaintForm()
          : _renderStepperForm(),
    );
  }

  /// RENDER notification message
  ///
  /// @param String start, String complementary, String message
  /// @return RichText _notificationMessage
  RichText _renderNotificationMessage(
      {@required String start,
      @required String complementary,
      @required String message}) {
    return RichText(
      text: TextSpan(
        style: style.copyWith(color: CustomColors.black),
        children: <TextSpan>[
          TextSpan(text: start),
          TextSpan(
            text: camelize(widget.title),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          TextSpan(text: complementary),
          TextSpan(
            text: message,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  /// SHOW within app notification
  ///
  /// @param String status, String response
  /// @return BotToast _displayNotification
  _displayNotification({@required RichText notificationMessage}) {
    return BotToast.showCustomText(
      duration: Duration(seconds: Config.notificationSeconds),
      animationDuration: Duration(milliseconds: 300),
      animationReverseDuration: Duration(milliseconds: 300),
      onlyOne: true,
      clickClose: true,
      crossPage: true,
      ignoreContentClick: true,
      backgroundColor: Colors.transparent,
      toastBuilder: (_) => Align(
        alignment: Alignment(-1.0, -0.92),
        child: Card(
          elevation: 16.0,
          margin: EdgeInsets.fromLTRB(48.0, 4.0, 16.0, 4.0),
          color: Colors.green.shade100,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
            side: BorderSide(color: CustomColors.green, width: 2.0),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: notificationMessage,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///
  /// Perform report submission
  ///
  void _submittingReport() {
    final FormState formState = _formKey.currentState;
    if (formState.validate()) {
      _serviceReport.serviceId = widget.serviceId;
      _serviceReport.deviceId = widget.client.deviceId;
      _serviceReport.mobileNo = widget.client.phone;
      if (widget.title.toLowerCase() == 'complaint') {
        _serviceReport.isPrimary = false;
        _makeMeterNoPrimary = false;
      }

      // check reported locations based on whether equals to customer location
      if (_renderLocationBuilds) {
        _serviceReport.region = widget.client.regionId.toString();
        _serviceReport.district = widget.client.districtId.toString();
        _serviceReport.area = widget.client.areaId.toString();
        _serviceReport.street = widget.client.streetId.toString();
        _serviceReport.specialMark = widget.client.specialMark;
      }

      formState.save();

      // DIALOG loder
      ProgressDialog pr = new ProgressDialog(
        context,
        type: ProgressDialogType.Normal,
      );

      pr.style(
        message: 'Submitting your ${widget.title.toLowerCase()} report...',
        borderRadius: 5.0,
        backgroundColor: Colors.white,
        progressWidget: LinearProgressIndicator(
          backgroundColor: CustomColors.green,
          valueColor: AlwaysStoppedAnimation(CustomColors.yellow),
        ),
        elevation: 16.0,
        insetAnimCurve: Curves.fastLinearToSlowEaseIn,
        progressTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 13.0,
          fontWeight: FontWeight.w400,
        ),
        messageTextStyle: style.copyWith(
          color: Colors.grey.shade700,
          letterSpacing: 1,
        ),
      );

      pr.show();

      /// Update Local storage client info
      Client client = Client();
      if (_makeMeterNoPrimary) {
        Future.delayed((Duration(seconds: 2))).then((value) {
          _clientDatabaseRepository
              .getClient(widget.client.id)
              .then((currClient) {
            client = currClient;

            client.meterno = _serviceReport.meterNumber;

            _clientDatabaseRepository.update(client).then((updatedClient) {
              if (updatedClient != null) {
                pr.update(message: "setting primary meter number...");
              }
            });
          });
        });
      }

      ServiceReportImplementRepository().postData(_serviceReport).then(
        (jsonData) {
          if (jsonData.isEmpty) {
            pr.hide();
            Alert().showErrorAlert(
              context: context,
              message:
                  'Unable submit your ${widget.title.toLowerCase()} report, Please try again!',
            );
            // dialog to show failure of getting TB number
          } else {
            Future.delayed(Duration(seconds: 1)).then(
              (value) {
                pr.update(message: "Almost done...");

                Future.delayed(Duration(seconds: 1)).then(
                  (value) {
                    pr.update(
                      message: "Successfully reported...",
                      progressWidget: Icon(
                        Icons.check,
                        color: CustomColors.green,
                        size: 35.0,
                      ),
                    );

                    Future.delayed(Duration(seconds: 1)).then(
                      (value) {
                        // send to dashboard
                        var responseJson = json.decode(jsonData)["data"];
                        String referenceNo =
                            responseJson.toString().split(':')[0];
                        String status = responseJson.toString().split(':')[1];

                        pr.hide().whenComplete(
                          () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => Dashboard(
                                  client: _makeMeterNoPrimary
                                      ? client
                                      : widget.client,
                                  title: 'TANESCO',
                                ),
                              ),
                            );

                            status.isNotEmpty
                                ? _displayNotification(
                                    notificationMessage:
                                        _renderNotificationMessage(
                                      start: 'This ',
                                      complementary:
                                          ' has already been reported with Reference No. ',
                                      message: referenceNo +
                                          ' & Status is ' +
                                          camelize(status),
                                    ),
                                  )
                                : _displayNotification(
                                    notificationMessage:
                                        _renderNotificationMessage(
                                      start: 'Your ',
                                      complementary:
                                          ' has been received successfully, with Reference No. ',
                                      message: referenceNo,
                                    ),
                                  );
                          },
                        );
                      },
                    );
                  },
                );
              },
            );
          }
        },
      );
    }
  }

  /// Report submission button
  ///
  /// @return Material
  Material _reportButton() {
    return Material(
      elevation: 16.0,
      borderRadius: BorderRadius.circular(50.0),
      color: CustomColors.green,
      child: MaterialButton(
        padding: const EdgeInsets.all(16.0),
        onPressed: () => _submittingReport(),
        child: Text(
          widget.title.toLowerCase() == 'complaint'
              ? 'submit complaint'.toUpperCase()
              : 'submit ${widget.title}'.toUpperCase(),
          textAlign: TextAlign.center,
          style: style.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _customAppBar.getAppBar(title: widget.title),
          SliverToBoxAdapter(
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  _formTitle(),
                  _formGuide(),
                  _renderForm(),
                  SizedBox(
                    height: 10.0,
                  ),
                  _reportButton(),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
