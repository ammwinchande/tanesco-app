import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {
  static LocalStorageService _instance;
  static SharedPreferences _preferences;
  static const String AppTourKey = 'completedTour';
  static const String LastNameKey = 'lastName';
  static const String MobileNoKey = 'mobileNo';
  static const String FirstNameKey = 'firstName';
  static const String AppLangCodeKey = 'langCode';
  static const String AppDarkModeKey = 'darkMode';
  static const String SignedUpKey = 'signedUp';
  static const String LoggedInKey = 'loggedIn';
  bool status = false;

  static Future<LocalStorageService> getInstance() async {
    if (_instance == null) {
      _instance = LocalStorageService();
    }
    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }
    return _instance;
  }

  String get langCode => _getFromDisk(AppLangCodeKey) ?? null;
  set langCode(String appLangCode) => _saveToDisk(AppLangCodeKey, appLangCode);

  String get mobileNo => _getFromDisk(MobileNoKey) ?? null;
  set mobileNo(String appMobileNo) => _saveToDisk(MobileNoKey, appMobileNo);

  String get firstName => _getFromDisk(FirstNameKey) ?? null;
  set firstName(String appFirstName) => _saveToDisk(FirstNameKey, appFirstName);

  String get lastName => _getFromDisk(LastNameKey) ?? null;
  set lastName(String appLastName) => _saveToDisk(LastNameKey, appLastName);

  bool get darkMode => _getFromDisk(AppDarkModeKey) ?? false;
  set darkMode(bool appTheme) => _saveToDisk(AppDarkModeKey, appTheme);

  bool get hasSignedUp => _getFromDisk(SignedUpKey) ?? false;
  set hasSignedUp(bool value) => _saveToDisk(SignedUpKey, value);

  bool get hasLoggedIn => _getFromDisk(LoggedInKey) ?? false;
  set hasLoggedIn(bool value) => _saveToDisk(LoggedInKey, value);

  bool get hasCompletedTour => _getFromDisk(AppTourKey) ?? false;
  set hasCompletedTour(bool appTour) => _saveToDisk(AppTourKey, appTour);

  /// Retrieving from Disk
  dynamic _getFromDisk(String key) {
    var value = _preferences.get(key);
    return value;
  }

  /// Saving to Disk
  void _saveToDisk<T>(String key, T content) {
    if (content is String) {
      _preferences.setString(key, content);
    }
    if (content is bool) {
      _preferences.setBool(key, content);
    }
    if (content is int) {
      _preferences.setInt(key, content);
    }
    if (content is double) {
      _preferences.setDouble(key, content);
    }
    if (content is List<String>) {
      _preferences.setStringList(key, content);
    }
  }

  /// Retrieving from Disk
  bool resetAllPrefs() {
    _preferences.clear().then((value) => status = value);
    return status;
  }

  /// Retrieving from Disk
  bool resetPrefsByKey(String key) {
    if (![null, ""].contains(key)) {
      _preferences.remove(key).then((value) => status = value);
    }
    return status;
  }
}
